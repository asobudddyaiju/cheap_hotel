import 'dart:convert';
import 'dart:core';

import 'package:shared_preferences/shared_preferences.dart';

/// shared preference storage
class Prefs {
  JsonCodec codec = new JsonCodec();
  SharedPreferences _sharedPreferences;



  static const String _IS_CONNECTED = "is_connected";
  static const String _IS_USER_LOADED = "is_user_loaded";

  static const String _AUTH_TOKEN = "auth_token";
  static const String _FCM_TOKEN = "fcm_token";

  static const String _IS_LOGGED_IN = "is_logged_in";

  static const String _USER_ID = "user_id";

  static const String _LAT = "lat";

  static const String _LNG = "lng";

  static const String _PHONE = "phone";

  static const String _PHONE_MODEL = "phone_moel";

  static const String _OTP = "otp";

  static const String _PROFILE_PHOTO = "profile _photo";
  static const String _DISPLAY_NAME = "display_name";

  static const String _NO_OF_ROOMS = "no_of_rooms";
  static const String _NO_OF_ADULTS = "no_of_adults";
  static const String _NO_OF_CHILDREN = "no_of_children";
  static const String _AGE_OF_CHILDREN="age_of_children";
  static const String _LOCAL_LANG = "local_lang";
  static const String _IN_DATE = "in_date";
  static const String _OUT_DATE = "out_date";
  static const String _DESTINATION = "destination";



  Prefs();


  // set sharedPreferences(SharedPreferences value) {
  //   _sharedPreferences = value;
  // }
  void getSharedPref() async {

    _sharedPreferences = await SharedPreferences.getInstance();
  }

  ///saving  the no of rooms as a String
  void setRooms({int val}) async {
    SharedPreferences _sharedPreferences =
        await SharedPreferences.getInstance();
    await _sharedPreferences.setString(_NO_OF_ROOMS, val.toString());
  }

  String getRooms(_sharedPreferences) =>
      _sharedPreferences.getString(_NO_OF_ROOMS);

  ///saving  the destination as a String
  void setDestination({String val}) async {
    SharedPreferences _sharedPreferences =
        await SharedPreferences.getInstance();
    await _sharedPreferences.setString(_DESTINATION, val.toString());
  }

  // String getDestination(_sharedPreferences) =>
  //     _sharedPreferences.getString(_DESTINATION);

  Future<String> getDestination() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String value = "no value";
    bool checkValue = prefs.containsKey(_DESTINATION);
    if (checkValue == true) value = prefs.getString(_DESTINATION);
    return value;
  }

  ///saving  the no of rooms as a String
  void setCheckInDate({String val}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(_IN_DATE, val);
  }

  ///saving  the no of rooms as a String
  void setCheckOutDate({String val}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString(_OUT_DATE, val);
  }

  String getCheckInDate(preference) {
    return preference.getString(_IN_DATE);
  }

  String getCheckOutDate(preference) {
    return preference.getString(_OUT_DATE);
  }

  ///saving  the language code  as a String
  void setLanguage({String val}) async {
    SharedPreferences _sharedPreferences =
        await SharedPreferences.getInstance();
    await _sharedPreferences.setString(_LOCAL_LANG, val.toString());
  }

  Future<String> getLanguage() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String value = "no value";
    bool checkValue = prefs.containsKey(_LOCAL_LANG);
    if (checkValue == true) value = prefs.getString(_LOCAL_LANG);

    return value;
  }

  ///saving  the no of adults as a String
  void setAdults({int val}) {
    _sharedPreferences.setString(_NO_OF_ADULTS, val.toString());
  }

  String getAdults() => _sharedPreferences.getString(_NO_OF_ADULTS);

  ///saving  the no of children as a String
  void setChildren({int val}) {
    _sharedPreferences.setString(_NO_OF_CHILDREN, val.toString());
  }

  String getChildren() => _sharedPreferences.getString(_NO_OF_CHILDREN);

  void setChildrenAge(List<String> list){
    getSharedPref();
    _sharedPreferences.setStringList(_AGE_OF_CHILDREN, list);
  }
  List<String> getChildrenAge(){
    getSharedPref();
    List<String> ageList=_sharedPreferences.getStringList(_AGE_OF_CHILDREN);
    return ageList;
  }
  ///saving  the auth token as a String
  void setAuthToken({String token}) {
    _sharedPreferences.setString(_AUTH_TOKEN, "Token " + token);
  }

  ///saving  the profile photo as a String
  void setProfilePhoto({String photo}) {
    getSharedPref();
    _sharedPreferences.setString(_PROFILE_PHOTO, photo);
  }

  ///saving  the profile photo as a String
  void setDisplayName({String name}) {
    getSharedPref();
    _sharedPreferences.setString(_DISPLAY_NAME, name);
  }

  ///saving  the profile phone no. as a String
  void setPhone({String phone}) {
    _sharedPreferences.setString(_PHONE, phone);
  }

  ///get method  for auth token
  String getPhone() => _sharedPreferences.getString(_PHONE);

  ///get method  for auth token
  String getAuthToken() => _sharedPreferences.getString(_AUTH_TOKEN);

  ///get method  for profile photo
  String getProfilePhoto() {
    getSharedPref();
    String value = "no value";
    bool checkValue = _sharedPreferences.containsKey(_PROFILE_PHOTO);
    if (checkValue == true)
      value = _sharedPreferences.getString(_PROFILE_PHOTO);
    return value;
  }

  ///get method  for profile photo
  String getDisplayName() {
    getSharedPref();
    String value = "no value";
    bool checkValue = _sharedPreferences.containsKey(_DISPLAY_NAME);
    if (checkValue == true) value = _sharedPreferences.getString(_DISPLAY_NAME);
    return value;
  }

  ///saving  the auth token as a String
  void setFcmToken({String token}) async {
    await _sharedPreferences.setString(_FCM_TOKEN, token);
  }

  ///get method  for auth token
  String getFcmToken() => _sharedPreferences.getString(_FCM_TOKEN);

  ///after login set isLoggedIn true
  ///before logout set isLoggedIn false
  void setIsLoggedIn(bool status) {
    _sharedPreferences.setBool(_IS_LOGGED_IN, status);
  }

  ///checking that is logged in or not

  bool isLoggedIn() => _sharedPreferences.getBool(_IS_LOGGED_IN) != null &&
          _sharedPreferences.getBool(_IS_LOGGED_IN)
      ? true
      : false;

  /// for clearing the data in preference
  void clearPrefs() async {
    final pref = await SharedPreferences.getInstance();
    await pref.clear();
  }

  ///setting  connection status
  void setIsConnected(bool status) {
    _sharedPreferences.setBool(_IS_CONNECTED, status);
  }

  /// checking network is available or not
  bool isConnected() => _sharedPreferences.getBool(_IS_CONNECTED) != null &&
          _sharedPreferences.getBool(_IS_CONNECTED)
      ? true
      : false;

  void setIsUserLoaded(bool status) {
    _sharedPreferences.setBool(_IS_USER_LOADED, status);
  }

  /// checking network is available or not
  bool isUserLoaded() => _sharedPreferences.getBool(_IS_USER_LOADED) != null &&
          _sharedPreferences.getBool(_IS_USER_LOADED)
      ? true
      : false;

  ///saving  the user id
  void setUserId({String userId}) {
    _sharedPreferences.setString(_USER_ID, userId);
  }

  ///get method  for user id
  String getUserID() => _sharedPreferences.getString(_USER_ID);

  ///saving  the latitude
  void setLat({String lat}) {
    _sharedPreferences.setString(_LAT, lat);
  }

  ///get method  for latitude
  String getLat() => _sharedPreferences.getString(_LAT);

  ///saving  the longitude
  void setLng({String lng}) {
    _sharedPreferences.setString(_LNG, lng);
  }

  ///get method  for latitude
  String getLng() => _sharedPreferences.getString(_LNG);

  ///saving  the flat id
  void setPhoneModel({String phoneModel}) {
    _sharedPreferences.setString(_PHONE_MODEL, phoneModel);
  }

  ///get method  for user id
  String getPhoneModel() => _sharedPreferences.getString(_PHONE_MODEL);
}
