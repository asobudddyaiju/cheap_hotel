// import 'package:hive/hive.dart';
//
// class GetCurrency {
//   List<String> currencyList = [
//     "AED",
//     "BGN",
//     "BRL",
//     "CNY",
//     "CZK",
//     "HRK",
//     "DKK",
//     "EUR",
//     "HUF",
//     "HKD",
//     "ISK",
//     "INR",
//     "IDR",
//     "ILS",
//     "JPY",
//     "KRW",
//     "LTL",
//     "MYR",
//     "NOK",
//     "PHP",
//     "PLN",
//     "RON",
//     "RUB",
//     "RSD",
//     "SEK",
//     "THB",
//     "TRY",
//     "UAH",
//     "USD",
//     "VND",
//   ];
//   List<String> countries = [
//     "العربية",
//     "Български",
//     "Català",
//     "简体中文",
//     "Čeština",
//     "Hrvatski",
//     "Dansk",
//     "Eesti",
//     "Suomi",
//     "Français",
//     "Deutsch",
//     "Ελληνικά",
//     "Magyar",
//     "Íslenska",
//     "Bahasa Indonesia",
//     "עברית",
//     "Italiano",
//     "日本語",
//     "한국어",
//     "Latviski",
//     "Lietuvių",
//     "Bahasa Malaysia",
//     "Nederlands",
//     "Norsk",
//     "Filipino",
//     "Polski",
//     "Português",
//     "Română",
//     "Русский",
//     "Srpski",
//     "Slovenčina",
//     "Slovenščina",
//     "Español",
//     "Svenska",
//     "ภาษาไทย",
//     "Türkçe",
//     "Українська",
//     "English",
//     "Tiếng Việt",
//   ];
//
//   List<String> locales = [
//     "ar",
//     "bg",
//     "zh",
//     "cs",
//     "hr",
//     "da",
//     "es",
//     "et",
//     "fi",
//     "fr",
//     "de",
//     "el",
//     "hu",
//     "is",
//     "id",
//     "he",
//     "it",
//     "ja",
//     "ko",
//     "lv",
//     "lt",
//     "ms",
//     "nl",
//     "no",
//     "tl",
//     "pl",
//     "pt",
//     "ro",
//     "ru",
//     "sr",
//     "sk",
//     "sl",
//     "sv",
//     "th",
//     "tr",
//     "uk",
//     "en",
//     "vi"
//   ];
//
//   List<String> getCountryName(String country) {
//     String currency;
//     String language;
//     String locale;
//     switch (country) {
//       case "United Arab Emirates":
//         {
//           currency = currencyList[0];
//           language = countries[0];
//           locale = locales[0];
//           break;
//         }
//       case "Bulgaria":
//         {
//           currency = currencyList[1];
//           language = countries[1];
//           locale = locales[1];
//
//
//           break;
//         }
//     // case "Brazil":
//     //   {
//     //     currency = currencyList[2];
//     //
//     //     break;
//     //   }
//       case "China":
//         {
//           currency = currencyList[3];
//           language = countries[3];
//           locale = locales[2];
//
//           break;
//         }
//       case "Czechia":
//         {
//           currency = currencyList[4];
//           language = countries[4];
//           locale = locales[3];
//
//           break;
//         }
//       case "Croatia":
//         {
//           currency = currencyList[5];
//           language = countries[5];
//           locale = locales[4];
//
//           break;
//         }
//       case "Denmark":
//         {
//           currency = currencyList[6];
//           language = countries[6];
//           locale = locales[5];
//
//
//           break;
//         }
//       case "Spain":
//         {
//           currency = currencyList[7];
//           language = countries[32];
//           locale = locales[6];
//           break;
//         }
//       case "Estonia":
//         {
//           currency = currencyList[7];
//           language = countries[7];
//           locale = locales[7];
//           break;
//         }
//       case "Finland":
//         {
//           currency = currencyList[7];
//           language = countries[8];
//           locale = locales[8];
//
//           break;
//         }
//       case "France":
//         {
//           currency = currencyList[7];
//           language = countries[9];
//           locale = locales[9];
//
//           break;
//         }
//       case "Germany":
//         {
//           currency = currencyList[7];
//           language = countries[10];
//           locale = locales[10];
//
//           break;
//         }
//       case "Greece":
//         {
//           currency = currencyList[7];
//           language = countries[11];
//           locale = locales[11];
//
//           break;
//         }
//       case "Hungary":
//         {
//           currency = currencyList[8];
//           language = countries[12];
//           locale = locales[12];
//
//           break;
//         }
//       case "Iceland":
//         {
//           currency = currencyList[10];
//           language = countries[13];
//           locale = locales[13];
//
//           break;
//         }
//     case "India":
//       {
//         currency = currencyList[11];
//
//         break;
//       }
//       case "Indonesia":
//         {
//           currency = currencyList[12];
//           language = countries[14];
//           locale = locales[14];
//
//           break;
//         }
//       case "Israel":
//         {
//           currency = currencyList[13];
//           language = countries[15];
//           locale = locales[15];
//
//           break;
//         }
//       case "Italy":
//         {
//           currency = currencyList[7];
//           language = countries[16];
//           locale = locales[16];
//
//           break;
//         }
//       case "Japan":
//         {
//           currency = currencyList[14];
//           language = countries[17];
//           locale = locales[17];
//
//           break;
//         }
//       case "South Korea":
//         {
//           currency = currencyList[15];
//           language = countries[18];
//           locale = locales[18];
//
//           break;
//         }
//       case "Latvia":
//         {
//           currency = currencyList[7];
//           language = countries[19];
//           locale = locales[19];
//
//           break;
//         }
//       case "Lithuania":
//         {
//           currency = currencyList[16];
//           language = countries[20];
//           locale = locales[20];
//
//           break;
//         }
//       case "Malaysia":
//         {
//           currency = currencyList[17];
//           language = countries[21];
//           locale = locales[21];
//
//           break;
//         }
//       case "Netherlands":
//         {
//           currency = currencyList[7];
//           language = countries[22];
//           locale = locales[22];
//
//           break;
//         }
//       case "Norway":
//         {
//           currency = currencyList[18];
//           language = countries[23];
//           locale = locales[23];
//
//           break;
//         }
//       case "Philippines":
//         {
//           currency = currencyList[19];
//           language = countries[24];
//           locale = locales[24];
//
//           break;
//         }
//       case "Poland":
//         {
//           currency = currencyList[20];
//           language = countries[25];
//           locale = locales[25];
//
//           break;
//         }
//       case "Portugal":
//         {
//           currency = currencyList[7];
//           language = countries[26];
//           locale = locales[26];
//
//           break;
//         }
//       case "Romania":
//         {
//           currency = currencyList[21];
//           language = countries[27];
//           locale = locales[27];
//
//           break;
//         }
//       case "Russia":
//         {
//           currency = currencyList[22];
//           language = countries[28];
//           locale = locales[28];
//
//           break;
//         }
//       case "Serbia":
//         {
//           currency = currencyList[23];
//           language = countries[29];
//           locale = locales[29];
//
//           break;
//         }
//       case "Slovakia":
//         {
//           currency = currencyList[7];
//           language = countries[30];
//           locale = locales[30];
//
//           break;
//         }
//       case "Slovenia":
//         {
//           currency = currencyList[7];
//           language = countries[31];
//           locale = locales[31];
//
//           break;
//         }
//       case "Sweden":
//         {
//           currency = currencyList[24];
//           language = countries[33];
//           locale = locales[32];
//
//           break;
//         }
//       case "Thailand":
//         {
//           currency = currencyList[25];
//           language = countries[34];
//           locale = locales[33];
//
//           break;
//         }
//       case "Turkey":
//         {
//           currency = currencyList[26];
//           language = countries[35];
//           locale = locales[34];
//
//           break;
//         }
//       case "Ukraine":
//         {
//           currency = currencyList[27];
//           language = countries[36];
//           locale = locales[35];
//
//           break;
//         }
//       case "United States":
//         {
//           currency = currencyList[28];
//           language = countries[37];
//           locale = locales[36];
//
//           break;
//         }
//       case "Vietnam":
//         {
//           currency = currencyList[29];
//           language = countries[38];
//           locale = locales[37];
//
//           break;
//         }
//       default :
//         currency = currencyList[28];
//         language = countries[37];
//         locale = locales[36];
//
//         break;
//     }
//     Hive.box('code').put(2, currency);
//     print(Hive.box('code').get(2));
//     return [currency,language,locale];
//   }
//
// }
