import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/localization/demo_local.dart';
import 'package:hotel_booking_1/src/ui/screens/splash_screen.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';
import 'package:android_play_install_referrer/android_play_install_referrer.dart';


class MyApp extends StatefulWidget {
  // This widget is the root of your application.
  static void setLocale(BuildContext context, Locale locale) {
    _MyAppState state = context.findAncestorStateOfType<_MyAppState>();
    state.setLocale(locale);
    // MyApp.setLocale(context, locality);
  }

  @override
  _MyAppState createState() => _MyAppState();
}

String lc;
String cc;
Locale _locale;
Locale locality;

class _MyAppState extends State<MyApp> {
  String _referrerDetails='';
  String utmMedium,utmMediumFinal;
  void setLocale(Locale locale) {
    setState(() {
      _locale = locale;
    });
  }

  void initState() {
    initReferrerDetails();
    getPublicIP();
    setState(() {
      lc = Hive.box('locale').get(2);
      cc = Hive.box('locale').get(1);
    });

    super.initState();
  }

  @override
  void didChangeDependencies() {
    setState(() {
      // _locale = Locale(lc, cc) ?? Locale('en', 'US');
      if ((lc != null) && (cc != null)) {
        _locale = Locale(lc, cc);
      }
    });
    super.didChangeDependencies();
  }

  // @override
  // void didChangeDependencies() {
  //   setState(() {
  //     _locale = Hive.box('locale').get(1);
  //   });

  //   super.didChangeDependencies();
  // }


  //Platform messages are asynchronous, so we initialize in an async method.
  Future<void> initReferrerDetails() async {
    String referrerDetailsString;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      ReferrerDetails referrerDetails = await AndroidPlayInstallReferrer.installReferrer;

      referrerDetailsString = referrerDetails.installReferrer.split('&').last.toString();
      referrerDetailsString=referrerDetailsString.split("=").last;
      utmMediumFinal=referrerDetailsString.length<=9?referrerDetailsString:referrerDetailsString.substring((referrerDetailsString.length-9),referrerDetailsString.length);
      Hive.box('referrerAPI').put(1, utmMediumFinal);
      print('utm medium from hive is '+Hive.box('ReferrerAPI').get(1).toString());
      print('utm medium after length rounded to 9 from hive is '+utmMediumFinal);

    } catch (e) {
      referrerDetailsString = 'Failed to get referrer details: $e';
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) return;

    setState(() {
      _referrerDetails = referrerDetailsString;
      Hive.box('referrerAPI').put(1, _referrerDetails.toString().trim());
      print("Google Analytics referrer API details for unmounted is "+Hive.box('referrerAPI').get(1).toString().trim());
    });
  }


  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'Cheap Hotels',
        theme: ThemeData(
          // This is the theme of your application.
          //
          // Try running your application with "flutter run". You'll see the
          // application has a blue toolbar. Then, without quitting the app, try
          // changing the primarySwatch below to Colors.green and then invoke
          // "hot reload" (press "r" in the console where you ran "flutter run",
          // or simply save your changes to "hot reload" in a Flutter IDE).
          // Notice that the counter didn't reset back to zero; the application
          // is not restarted.
          primarySwatch: Colors.blue,
          textTheme: Theme.of(context).textTheme.apply(
              fontFamily: 'Poppins',
              bodyColor: Constants.kitGradients[0],
              displayColor: Constants.kitGradients[0]),
          // This makes the visual density adapt to the platform that you run
          // the app on. For desktop platforms, the controls will be smaller and
          // closer together (more dense) than on mobile platforms.
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        locale: _locale,
        localizationsDelegates: [
          DemoLocalizations
              .delegate, // ... app-specific localization delegate[s]
          GlobalMaterialLocalizations.delegate,
          GlobalWidgetsLocalizations.delegate,
          GlobalCupertinoLocalizations.delegate,
        ],
        localeResolutionCallback: (deviceLocale, supportedLocales) {
          for (var locale in supportedLocales) {
            if (locale.languageCode == deviceLocale.languageCode &&
                locale.countryCode == deviceLocale.countryCode) {
              return deviceLocale;
            }
          }
          return supportedLocales.first;
        },
        supportedLocales: [
          Locale('en', 'US'), // English
          Locale('ar', 'AE'), //Arabic
          Locale('bg', 'BG'), //Bulgarian
          // Locale('pt', 'PT'), //Brazil
          Locale('ca', 'CA'), //Catalan
          Locale('zh', 'CN'), //Chinese
          Locale('cs', 'CZ'), //Czech
          Locale('da', 'DK'), //Danish
          Locale('de', 'DE'), //German
          Locale('el', 'GR'), //Greek
          Locale('es', 'ES'), //Spanish
          Locale('et', 'EE'), //Estonian
          Locale('fi', 'FI'), //Finland
          Locale('fr', 'FR'), //France
          // Locale('zh', 'CN'), //Hongkong
          Locale('hr', 'HR'), //Croatian
          Locale('hu', 'HU'), //Hungarian
          Locale('id', 'ID'), //Indonesian
          Locale('he', 'IL'), //Israel
          Locale('is', 'IS'), //Iceland
          Locale('it', 'IT'), //Italy
          Locale('ja', 'JP'), //Japanese
          Locale('ko', 'KO'), //Korea
          Locale('lt', 'LT'), //Lithuanian
          Locale('lv', 'LV'), //Latvian
          Locale('ms', 'MY'), //Malaysia
          Locale('nl', 'NL'), //Netherlands
          Locale('no', 'NO'), //Norwegian
          Locale('pl', 'PL'), //Poland
          Locale('pt', 'PT'), //Portugal
          Locale('ro', 'RO'), //Romanian
          Locale('ru', 'RU'), //Russian
          Locale('sk', 'SK'), //Slovak
          Locale('sl', 'SL'), //Slovenian
          Locale('sr', 'SP'), //Serbian
          Locale('sv', 'SE'), //Sweden
          Locale('th', 'TH'), //Thai
          Locale('tl', 'PH'), //Philippines
          Locale('tr', 'TR'), //Turkish
          // Locale('zh', 'CN'), //Chinese(Traditional)
          Locale('uk', 'UA'), //Ukrainian
          Locale('vi', 'VN'), //Vietnamese
        ],
        home: FutureBuilder(
            future: Hive.openBox('lang'),
            builder: (BuildContext context, AsyncSnapshot snapshot) {
              if (snapshot.connectionState == ConnectionState.done) {
                if (snapshot.hasError)
                  return Text(snapshot.error.toString());
                else
                  return SplashScreen();
              } else
                return Scaffold();
            }));
  }


  @override
  void dispose() {
    Hive.box('adult').clear();
    Hive.close();
    super.dispose();
  }
}
