import 'package:flutter/material.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';
class MapShrimmer extends StatefulWidget {
  String title;
  MapShrimmer({this.title});
  @override
  _MapShrimmerState createState() => _MapShrimmerState();
}

class _MapShrimmerState extends State<MapShrimmer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context,dividedBy: 25),
      width: screenWidth(context,dividedBy: 2.2),
       decoration: BoxDecoration(
         color: Constants.kitGradients[3],
         boxShadow: [
           BoxShadow(
             offset: Offset(-0.4,-0.4),
             spreadRadius: 0,
             blurRadius: 1,
             color: Constants.kitGradients[12],
           ),

           BoxShadow(
             offset: Offset(0.4,0.4),
             spreadRadius: 0,
             blurRadius: 1,
             color: Constants.kitGradients[12],
           )
         ]
       ),
      child: Align(
            alignment: Alignment.center,
          child: Text(widget.title,style: TextStyle(fontSize: 16,color: Constants.kitGradients[12],fontFamily: 'Gilroy',fontWeight: FontWeight.w300),)),

    );
  }
}
