import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hotel_booking_1/src/ui/screens/destination_page.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';

// ignore: must_be_immutable
class HomeFieldDestination extends StatefulWidget {
  String svgPath;
  Function onTap;
  String label;
  String hintText;
  HomeFieldDestination({this.svgPath, this.hintText, this.onTap, this.label});
  @override
  _HomeFieldDestinationState createState() => _HomeFieldDestinationState();
}

class _HomeFieldDestinationState extends State<HomeFieldDestination> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:
          EdgeInsets.symmetric(horizontal: screenWidth(context, dividedBy: 15)),
      child: GestureDetector(
        onTap: widget.onTap,
        child: Container(
          height: screenHeight(context, dividedBy: 7),
          width: screenWidth(context, dividedBy: 1),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Container(
                height: 60,
                width: 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Color(0xFFE3E4E8),
                ),
                child: Padding(
                  padding: const EdgeInsets.all(15),
                  child: SvgPicture.asset(
                    widget.svgPath,
                    color: Colors.black,
                  ),
                ),
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 25),
              ),
              Container(
                height: screenHeight(context, dividedBy: 11),
                width: screenWidth(context, dividedBy: 1.6),
                child: Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 100)),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        height: screenHeight(context, dividedBy: 40),
                        width: screenWidth(context, dividedBy: 1),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Colors.transparent,
                        ),
                        child: Text(
                          widget.label,
                          style: TextStyle(
                              color: Color(0xFFABABAB),
                              fontSize: 14,
                              fontFamily: 'poppins'),
                        ),
                      ),
                      Container(
                        height: screenHeight(context, dividedBy: 25),
                        width: screenWidth(context, dividedBy: 1),
                        child: Text(
                          widget.hintText,
                          overflow: TextOverflow.ellipsis,
                          style: TextStyle(
                              color: Constants.kitGradients[2],
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                              fontFamily: 'poppins'),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
