import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/localization/locale.dart';
import 'package:hotel_booking_1/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';


class BuildMaterialButton extends StatefulWidget {
  int roomCount;
  List<int> adultCount;
  int counterValueAdults1;
  int counterValueAdults2;
  int counterValueAdults3;
  int counterValueAdults4;
  int counterValueAdults5;
  int counterValueAdults6;
  int counterValueAdults7;
  int counterValueAdults8;
  int counterValueAdults9;
  int counterValueChildren1;
  int counterValueChildren2;
  int counterValueChildren3;
  int counterValueChildren4;
  int counterValueChildren5;
  int counterValueChildren6;
  int counterValueChildren7;
  int counterValueChildren8;
  int counterValueChildren9;
  List<int> childrenCount;
  List<List<String>> ChildrenAgeList = [];
  List<String> agelist1 = [];
  List<String> agelist2 = [];
  List<String> agelist3 = [];
  List<String> agelist4 = [];
  List<String> agelist5 = [];
  List<String> agelist6 = [];
  List<String> agelist7 = [];
  List<String> agelist8 = [];
  List<String> agelist9 = [];

  BuildMaterialButton({this.roomCount,this.adultCount,this.counterValueAdults1,this.counterValueAdults2,this.counterValueAdults3,this.counterValueAdults4,this.counterValueAdults5,this.counterValueAdults6,this.counterValueAdults7,this.counterValueAdults8,this.counterValueAdults9,this.counterValueChildren1,this.counterValueChildren2,this.counterValueChildren3,this.counterValueChildren4,this.counterValueChildren5,this.counterValueChildren6,this.counterValueChildren7,this.counterValueChildren8,this.counterValueChildren9,this.childrenCount,this.ChildrenAgeList,this.agelist1,this.agelist2,this.agelist3,this.agelist4,this.agelist5,this.agelist6,this.agelist7,this.agelist8,this.agelist9});


  @override
  _BuildMaterialButtonState createState() => _BuildMaterialButtonState();
}

class _BuildMaterialButtonState extends State<BuildMaterialButton> {
  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      color: Constants.kitGradients[0],
      minWidth: screenWidth(context, dividedBy: 1.2),
      height: 50,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(25)),
      child: Text(
        getTranslated(context, 'Apply'),
        style: TextStyle(color: Colors.white, fontSize: 22),
      ),
      onPressed: () {
        FirebaseAnalytics().logEvent(name: 'selected_guests',parameters: {
          'room_count':widget.roomCount,
          'adult_count':widget.adultCount,
          'children_count':widget.childrenCount
        });
        Hive.box('adult').put(1, widget.roomCount);
        Hive.box('adult').put(2, widget.adultCount);
        Hive.box('adult').put(3, widget.childrenCount);
        Hive.box('adult').put(4, widget.ChildrenAgeList);
        Hive.box('adult').put(11, widget.agelist1);
        Hive.box('adult').put(12, widget.agelist2);
        Hive.box('adult').put(13, widget.agelist3);
        Hive.box('adult').put(14, widget.agelist4);
        Hive.box('adult').put(15, widget.agelist5);
        Hive.box('adult').put(16, widget.agelist6);
        Hive.box('adult').put(17, widget.agelist7);
        Hive.box('adult').put(18, widget.agelist8);
        Hive.box('adult').put(19, widget.agelist9);
        Hive.box('adult').put(21, widget.counterValueAdults1);
        Hive.box('adult').put(22, widget.counterValueAdults2);
        Hive.box('adult').put(23, widget.counterValueAdults3);
        Hive.box('adult').put(24, widget.counterValueAdults4);
        Hive.box('adult').put(25, widget.counterValueAdults5);
        Hive.box('adult').put(26, widget.counterValueAdults6);
        Hive.box('adult').put(27, widget.counterValueAdults7);
        Hive.box('adult').put(28, widget.counterValueAdults8);
        Hive.box('adult').put(29, widget.counterValueAdults9);
        Hive.box('adult').put(31, widget.counterValueChildren1);
        Hive.box('adult').put(32, widget.counterValueChildren2);
        Hive.box('adult').put(33, widget.counterValueChildren3);
        Hive.box('adult').put(34, widget.counterValueChildren4);
        Hive.box('adult').put(35, widget.counterValueChildren5);
        Hive.box('adult').put(36, widget.counterValueChildren6);
        Hive.box('adult').put(37, widget.counterValueChildren7);
        Hive.box('adult').put(38, widget.counterValueChildren8);
        Hive.box('adult').put(39, widget.counterValueChildren9);

        print("no: of rooms "+widget.roomCount.toString());
        print("no: of adults "+widget.adultCount.toString());
        print("no: of children "+widget.childrenCount.toString());
        print("children age list "+widget.ChildrenAgeList.toString());
        print('children agelist from hive: '+Hive.box('adult').get(4).toString());
        print( Hive.box('adult').get(11).toString());
        print( Hive.box('adult').get(12).toString());
        print( Hive.box('adult').get(13).toString());
        print( Hive.box('adult').get(14).toString());
        print( Hive.box('adult').get(15).toString());
        print( Hive.box('adult').get(16).toString());
        print( Hive.box('adult').get(17).toString());
        print( Hive.box('adult').get(18).toString());
        print( Hive.box('adult').get(19).toString());
        print( "no: of adults in room 1 is "+Hive.box('adult').get(21).toString());
        print( "no: of adults in room 2 is "+Hive.box('adult').get(22).toString());
        print( "no: of adults in room 3 is "+Hive.box('adult').get(23).toString());
        print( "no: of adults in room 4 is "+Hive.box('adult').get(24).toString());
        print( "no: of adults in room 5 is "+Hive.box('adult').get(25).toString());
        print( "no: of adults in room 6 is "+Hive.box('adult').get(26).toString());
        print( "no: of adults in room 7 is "+Hive.box('adult').get(27).toString());
        print( "no: of adults in room 8 is "+Hive.box('adult').get(28).toString());
        print( "no: of adults in room 9 is "+Hive.box('adult').get(29).toString());
        print( "no: of children in room 1 is "+Hive.box('adult').get(31).toString());
        print( "no: of children in room 2 is "+Hive.box('adult').get(32).toString());
        print( "no: of children in room 3 is "+Hive.box('adult').get(33).toString());
        print( "no: of children in room 4 is "+Hive.box('adult').get(34).toString());
        print( "no: of children in room 5 is "+Hive.box('adult').get(35).toString());
        print( "no: of children in room 6 is "+Hive.box('adult').get(36).toString());
        print( "no: of children in room 7 is "+Hive.box('adult').get(37).toString());
        print( "no: of children in room 8 is "+Hive.box('adult').get(38).toString());
        print( "no: of children in room 9 is "+Hive.box('adult').get(39).toString());
        Navigator.push(context, MaterialPageRoute(builder: (context)=>HomePage()));
      },
    );
  }
}
