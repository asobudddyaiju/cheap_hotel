import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dropdown/flutter_dropdown.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/localization/locale.dart';
import 'package:hotel_booking_1/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_1/src/ui/widgets/build_material_button.dart';
import 'package:hotel_booking_1/src/ui/widgets/childrenCountUI.dart';
import 'package:hotel_booking_1/src/ui/widgets/room_count.dart';
import 'package:hotel_booking_1/src/ui/widgets/room_person_count.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';

class ModalBottomSheet extends StatefulWidget {
  _ModalBottomSheetState createState() => _ModalBottomSheetState();
}

class _ModalBottomSheetState extends State<ModalBottomSheet>
    with SingleTickerProviderStateMixin {
  var heightOfModalBottomSheet = 100.0;
  int counterValueRoom = 1;
  List<int> counterValueAdults=[2,0,0,0,0,0,0,0,0];
  List<int> counterValueChildren=[0,0,0,0,0,0,0,0,0];
  int counterValueAdults1=2;
  int counterValueAdults2=0;
  int counterValueAdults3=0;
  int counterValueAdults4=0;
  int counterValueAdults5=0;
  int counterValueAdults6=0;
  int counterValueAdults7=0;
  int counterValueAdults8=0;
  int counterValueAdults9=0;
  int counterValueChildren1=0;
  int counterValueChildren2=0;
  int counterValueChildren3=0;
  int counterValueChildren4=0;
  int counterValueChildren5=0;
  int counterValueChildren6=0;
  int counterValueChildren7=0;
  int counterValueChildren8=0;
  int counterValueChildren9=0;
  List<int> counterValueAdultsTotal=[];
  List<int> counterValueChildrenTotal=[];
  bool showLimit = false;
  bool showLimitPerRoom = false;
  List<String> agelist1 = [];
  List<String> agelist2 = [];
  List<String> agelist3 = [];
  List<String> agelist4 = [];
  List<String> agelist5 = [];
  List<String> agelist6 = [];
  List<String> agelist7 = [];
  List<String> agelist8 = [];
  List<String> agelist9 = [];
  List<List<String>> agelistoflist=[];
  String ageValue;
  String hint1, hint2, hint3;
  ScrollController _controller = new ScrollController();


 @override
  void initState() {
   FirebaseAnalytics().logEvent(name: 'guest_page',parameters: null);
    if (Hive.box('adult').get(1) != null) {
      setState(() {
        counterValueRoom = (Hive.box('adult').get(1));
      });
    } else {
      setState(() {
        counterValueRoom = 1;
      });
    }
    counterValueAdults1=Hive.box('adult').get(21) != null?Hive.box('adult').get(21):counterValueAdults1;
    counterValueAdults2=Hive.box('adult').get(22) != null?Hive.box('adult').get(22):counterValueAdults2;
    counterValueAdults3=Hive.box('adult').get(23) != null?Hive.box('adult').get(23):counterValueAdults3;
    counterValueAdults4=Hive.box('adult').get(24) != null?Hive.box('adult').get(24):counterValueAdults4;
    counterValueAdults5=Hive.box('adult').get(25) != null?Hive.box('adult').get(25):counterValueAdults5;
    counterValueAdults6=Hive.box('adult').get(26) != null?Hive.box('adult').get(26):counterValueAdults6;
    counterValueAdults7=Hive.box('adult').get(27) != null?Hive.box('adult').get(27):counterValueAdults7;
    counterValueAdults8=Hive.box('adult').get(28) != null?Hive.box('adult').get(28):counterValueAdults8;
    counterValueAdults9=Hive.box('adult').get(29) != null?Hive.box('adult').get(29):counterValueAdults9;

    counterValueChildren1=Hive.box('adult').get(31) != null?Hive.box('adult').get(31):counterValueChildren1;
    counterValueChildren2=Hive.box('adult').get(32) != null?Hive.box('adult').get(32):counterValueChildren2;
    counterValueChildren3=Hive.box('adult').get(33) != null?Hive.box('adult').get(33):counterValueChildren3;
    counterValueChildren4=Hive.box('adult').get(34) != null?Hive.box('adult').get(34):counterValueChildren4;
    counterValueChildren5=Hive.box('adult').get(35) != null?Hive.box('adult').get(35):counterValueChildren5;
    counterValueChildren6=Hive.box('adult').get(36) != null?Hive.box('adult').get(36):counterValueChildren6;
    counterValueChildren7=Hive.box('adult').get(37) != null?Hive.box('adult').get(37):counterValueChildren7;
    counterValueChildren8=Hive.box('adult').get(38) != null?Hive.box('adult').get(38):counterValueChildren8;
    counterValueChildren9=Hive.box('adult').get(39) != null?Hive.box('adult').get(39):counterValueChildren9;

    if (Hive.box('adult').get(2) != null) {
      setState(() {
        counterValueAdults = (Hive.box('adult').get(2));
      });
    } else {
      setState(() {
        counterValueAdults=[2,0,0,0,0,0,0,0,0];
      });
    }
    if (Hive.box('adult').get(3) != null) {
      setState(() {
        counterValueChildren = (Hive.box('adult').get(3));
      });
    } else {
      setState(() {
        counterValueChildren=[0,0,0,0,0,0,0,0,0];
      });
    }
    if (Hive.box('adult').get(11)!=null||Hive.box('adult').get(12)!=null||Hive.box('adult').get(13)!=null||Hive.box('adult').get(14)!=null||Hive.box('adult').get(15)!=null||Hive.box('adult').get(16)!=null||Hive.box('adult').get(17)!=null||Hive.box('adult').get(18)!=null||Hive.box('adult').get(19)!=null) {
      agelist1=Hive.box('adult').get(11);
      agelist2=Hive.box('adult').get(12);
      agelist3=Hive.box('adult').get(13);
      agelist4=Hive.box('adult').get(14);
      agelist5=Hive.box('adult').get(15);
      agelist6=Hive.box('adult').get(16);
      agelist7=Hive.box('adult').get(17);
      agelist8=Hive.box('adult').get(18);
      agelist9=Hive.box('adult').get(19);

      for(int i=0;i<agelist1.length;i++){
        if (agelist1[i].length == 1) {
          setState(() {
            hint1 = agelist1[0];
          });
        }
        if (agelist1.length == 2) {
          setState(() {
            hint1 = agelist1[0];
            hint2 = agelist1[1];
          });
        }
        if (agelist1.length == 3) {
          setState(() {
            hint1 = agelist1[0];
            hint2 = agelist1[1];
            hint3 = agelist1[2];
          });
        }
      }
    } else {
      setState(() {
       agelist1=[];
       agelist2=[];
       agelist3=[];
       agelist4=[];
       agelist5=[];
       agelist6=[];
       agelist7=[];
       agelist8=[];
       agelist9=[];
      });
    }

    agelistoflist=[agelist1,agelist2,agelist3,agelist4,agelist5,agelist6,agelist7,agelist8,agelist9];

    counterValueAdultsTotal=[counterValueAdults1,counterValueAdults2,counterValueAdults3,counterValueAdults4,counterValueAdults5,counterValueAdults6,counterValueAdults7,counterValueAdults8,counterValueAdults9];
    counterValueChildrenTotal=[counterValueChildren1,counterValueChildren2,counterValueChildren3,counterValueChildren4,counterValueChildren5,counterValueChildren6,counterValueChildren7,counterValueChildren8,counterValueChildren9];
    super.initState();
  }

  List<String> years = [
    "1 Years",
    "2 Years",
    "3 Years",
    "4 Years",
    "5 Years",
    "6 Years",
    "7 Years",
    "8 Years",
    "9 Years",
    "10 Years",
    "11 Years",
    "12 Years",
    "13 Years",
    "14 Years",
    "15 Years",
    "16 Years",
    "17 Years",
  ];
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 15)),
          child: AppBar(
            title: Text(
              getTranslated(context, 'Rooms_and_Guests'),
              style: TextStyle(color: Constants.kitGradients[17],
              fontSize: 16,
              fontWeight: FontWeight.w300),
            ),
            leading: new IconButton(
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                        (route) => false);
              },
              icon: new Icon(
                Icons.close,
                color: Constants.kitGradients[15],
              ),
            ),
            backgroundColor: Colors.white,
            elevation: 0.5,
          )),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            // Column(
            //   children: [
            //     SizedBox(
            //       height: screenHeight(context, dividedBy: 14.0),
            //     ),
            //     Row(
            //       children: [
            //         FlatButton(
            //           child: Icon(Icons.close),
            //           onPressed: () {
            //             Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>HomePage()), (route) => false);
            //           },
            //         ),
            //         Padding(
            //           padding: const EdgeInsets.symmetric(
            //               vertical: 10, horizontal: 0),
            //           child: Text(
            //             "Rooms and Guests",
            //             style: TextStyle(
            //                 color: Constants.kitGradients[6],
            //                 fontFamily: 'Poppins',
            //                 fontSize: 16,
            //                 fontWeight: FontWeight.w300),
            //           ),
            //         ),
            //       ],
            //     ),
            //   ],
            // ),

            showLimit == true
                ? Padding(
                  padding: const EdgeInsets.all(20.0),
                  child: Center(
              child: Container(
                  height: screenHeight(context, dividedBy: 30),
                  width: screenWidth(context, dividedBy: 1.2),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: [
                        Image(
                          image: AssetImage(Constants.CAUTION_SIGN),
                        ),

                        showLimitPerRoom == false?Text(
                          getTranslated(context,Constants.GUEST_LIMIT_ERROR ),
                          style: TextStyle(
                              color: Constants.kitGradients[5],
                              fontSize: 9.0,
                              fontWeight: FontWeight.w400,
                              fontFamily: 'Poppins',
                              fontStyle: FontStyle.normal),
                        ):Text(
                          getTranslated(context, Constants.GUEST_LIMIT_ERROR),
                          style: TextStyle(
                              color: Constants.kitGradients[5],
                              fontSize: 9.0,
                              fontWeight: FontWeight.w400,
                              fontFamily: 'Poppins',
                              fontStyle: FontStyle.normal),
                        ),
                      ],
                    ),
                  ),
              ),
            ),
                )
                : Container(
              height: screenHeight(context, dividedBy: 40),
              width: screenWidth(context, dividedBy: 1.2),
            ),
            Container(
              width: screenWidth(context, dividedBy: 1.1),
              decoration: BoxDecoration(color: Constants.kitGradients[0],
                  borderRadius: BorderRadius.circular(10),
                  border: Border.all(
                      color: Constants.kitGradients[0],
                      width: 0.8,
                      style: BorderStyle.solid)),
              child: RoomCount(
                  counterLabel: getTranslated(context, 'Room'),
                  counterValue: counterValueRoom,
                  decFunction: () {
                    if (counterValueRoom > 1) {
                      setState(() {
                        counterValueRoom--;
                        counterValueAdults.removeAt(counterValueRoom);
                        counterValueChildren.removeAt(counterValueRoom);
                        counterValueAdults.insert(counterValueRoom, 0);
                        counterValueChildren.insert(counterValueRoom, 0);
                        counterValueAdultsTotal.removeAt(counterValueRoom);
                        counterValueAdultsTotal.insert(counterValueRoom, 0);
                        counterValueChildrenTotal.removeAt(counterValueRoom);
                        counterValueChildrenTotal.insert(counterValueRoom, 0);
                      });
                    }
                    if (counterValueRoom <= 9) {
                      setState(() {
                        showLimit = false;
                      });
                    }
                  },
                  incFunction: () {

                    int sumAdults =0;
                    for(int q=0;q<counterValueAdultsTotal.length;q++)
                    {
                      sumAdults=sumAdults+counterValueAdultsTotal[q];
                      print("total adult is "+sumAdults.toString());
                    }
                    int sumChildren=0;
                    for(int y=0;y<counterValueChildrenTotal.length;y++){
                      sumChildren=sumChildren+counterValueChildrenTotal[y];
                      print("total children is "+sumChildren.toString());
                    }
                    int sumTotal=sumAdults+sumChildren;
                    print("total guests"+sumTotal.toString());

                    if (counterValueRoom < 9 && sumTotal<15) {
                      setState(() => counterValueRoom++);
                    } else {
                      setState(() {
                        showLimit = true;
                      });
                    }
                  }),
            ),

            ListView.builder(
              controller: _controller,
              shrinkWrap: true,
              padding: EdgeInsets.only(
                  left: screenWidth(context, dividedBy: 22),
                  right: screenWidth(context, dividedBy: 22)),
              scrollDirection: Axis.vertical,
              itemCount: counterValueRoom,
              itemBuilder: (BuildContext cntxt, int index) {
                return new Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 15)),
                  child: Container(
                    width: screenWidth(context, dividedBy: 1.1),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        border: Border.all(
                            color: Constants.kitGradients[0],
                            width: 2.5,
                            style: BorderStyle.solid)),
                    child: Column(
                      children: [
                        Align(
                          alignment: Alignment.topLeft,
                          child: Container(
                            decoration: BoxDecoration(
                                borderRadius:
                            BorderRadius.only(topLeft: Radius.circular(5),topRight: Radius.circular(5)),
                                color: Constants.kitGradients[0],),
                            child: Padding(
                              padding: const EdgeInsets.symmetric(vertical: 15),
                              child: Row(children: [
                                SizedBox(
                                  width: screenWidth(context, dividedBy: 25),
                                ),
                                Text(
                                  getTranslated(context, 'Room')+ (index+1).toString(),
                                  style: TextStyle(
                                      color: Constants.kitGradients[1],
                                      fontFamily: 'Poppins',
                                      fontSize: 16,
                                      fontWeight: FontWeight.w300),
                                ),
                              ]),
                            ),
                          ),
                        ),
                        RoomPersonCount(
                          counterLabel: getTranslated(context, 'Adult'),
                          counterValue: counterValueAdultsTotal[index],
                          decFunction: () {

                            int sumAdults =0;
                            for(int q=0;q<counterValueAdultsTotal.length;q++)
                            {
                              sumAdults=sumAdults+counterValueAdultsTotal[q];
                              print("total adult is "+sumAdults.toString());
                            }
                            int sumChildren=0;
                            for(int y=0;y<counterValueChildrenTotal.length;y++){
                              sumChildren=sumChildren+counterValueChildrenTotal[y];
                              print("total children is "+sumChildren.toString());
                            }
                            int sumTotal=sumAdults+sumChildren;
                            print("total guests"+sumTotal.toString());
                            int sumAdultPerRoom = counterValueAdultsTotal[index];
                            int sumChildrenPerRoom = counterValueChildrenTotal[index];
                            int SumPerRoomTotal = sumAdultPerRoom+sumChildrenPerRoom;

                            if(sumTotal>16){
                              setState(() {
                                showLimit=true;
                              });
                            }else if(sumTotal<=16){
                              showLimit=false;
                            }
                            if(SumPerRoomTotal>4){
                              setState(() {
                                showLimit=true;
                                showLimitPerRoom=true;
                              });
                            }else if(SumPerRoomTotal<=4){
                              showLimit=false;
                              showLimitPerRoom=false;
                            }

                            if(counterValueRoom==1 && counterValueAdultsTotal[index]>1){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x--;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==2 && counterValueAdultsTotal[index]>1){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x--;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==3 && counterValueAdultsTotal[index]>1){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x--;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==4 && counterValueAdultsTotal[index]>1){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x--;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==5 && counterValueAdultsTotal[index]>1){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x--;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==6 && counterValueAdultsTotal[index]>1){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x--;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==7 && counterValueAdultsTotal[index]>1){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x--;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==8 && counterValueAdultsTotal[index]>1){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x--;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==9 && counterValueAdultsTotal[index]>1){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x--;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }
                            //////////////////////////////////////////////////////////
                            if (counterValueAdults[index] > 1) {
                              setState(() => counterValueAdults[index]--);
                              print("no: of rooms "+Hive.box('adult').get(1).toString());
                              print("no: of adults "+Hive.box('adult').get(2).toString());
                              print("no: of children "+Hive.box('adult').get(3).toString());
                            }
                            /////////////////////////////////////////////////////////////

                          },
                          incFunction: () {
                            int sumAdults =0;
                            for(int q=0;q<counterValueAdultsTotal.length;q++)
                              {
                                sumAdults=sumAdults+counterValueAdultsTotal[q];
                                print("total adult is "+sumAdults.toString());
                              }
                            int sumChildren=0;
                            for(int y=0;y<counterValueChildrenTotal.length;y++){
                              sumChildren=sumChildren+counterValueChildrenTotal[y];
                              print("total children is "+sumChildren.toString());
                            }
                            int sumTotal=sumAdults+sumChildren;
                            print("total guests"+sumTotal.toString());
                            int sumAdultPerRoom = counterValueAdultsTotal[index];
                            int sumChildrenPerRoom = counterValueChildrenTotal[index];
                            int SumPerRoomTotal = sumAdultPerRoom+sumChildrenPerRoom;

                            if(sumTotal>16){
                              setState(() {
                                showLimit=true;
                              });
                            }else if(sumTotal<=16){
                              showLimit=false;
                            }
                            if(SumPerRoomTotal>4){
                              setState(() {
                                showLimit=true;
                                showLimitPerRoom=true;
                              });
                            }else if(SumPerRoomTotal<=4){
                              showLimit=false;
                              showLimitPerRoom=false;
                            }

                            if(counterValueRoom==1 && counterValueAdultsTotal[index]<4 && sumTotal<=16 && SumPerRoomTotal<4){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x++;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==2 && counterValueAdultsTotal[index]<4 && sumTotal<=16 && SumPerRoomTotal<4){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                               x++;
                               counterValueAdultsTotal.removeAt(index);
                               counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==3 && counterValueAdultsTotal[index]<4 && sumTotal<=16 && SumPerRoomTotal<4){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x++;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==4 && counterValueAdultsTotal[index]<4 && sumTotal<=16 && SumPerRoomTotal<4){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x++;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==5 && counterValueAdultsTotal[index]<4 && sumTotal<=16 && SumPerRoomTotal<4){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x++;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==6 && counterValueAdultsTotal[index]<4 && sumTotal<=16 && SumPerRoomTotal<4){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x++;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==7 && counterValueAdultsTotal[index]<4 && sumTotal<=16 && SumPerRoomTotal<4){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x++;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==8 && counterValueAdultsTotal[index]<4 && sumTotal<=16 && SumPerRoomTotal<4){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x++;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==9 && counterValueAdultsTotal[index]<4 && sumTotal<=16 && SumPerRoomTotal<4){
                              int x =  counterValueAdultsTotal[index];
                              setState(() {
                                x++;
                                counterValueAdultsTotal.removeAt(index);
                                counterValueAdultsTotal.insert(index, x);
                              });
                            }
             /////////////////////////////////////////////////////////////////////////////////////////
                            int sum1=0;
                            int sum2=0;
                            int sum=0;
                            for(int p=0;p<counterValueAdults.length;p++) {
                              sum1 = sum1 + counterValueAdults[p];
                            }
                            for(int q=0;q<counterValueChildren.length;q++) {
                              sum2 = sum2 + counterValueChildren[q];
                            }
                            sum=sum1 + sum2;

                     ///////////////////////////////////////////////////////////////////////////////
                            if ((counterValueAdults[index] + counterValueChildren[index]<=3)&&(sum <=16)) {
                              setState(() => counterValueAdults[index]++);
                              print("no: of rooms "+Hive.box('adult').get(1).toString());
                              print("no: of adults "+Hive.box('adult').get(2).toString());
                              print("no: of children "+Hive.box('adult').get(3).toString());
                            } else{
                              setState(() {
                                //showLimit=true;
                               // showLimitPerRoom=true;
                              });
                            }
                            if(sum>=16){
                              setState(() {
                               // showLimit=true;
                               // showLimitPerRoom=false;
                              });
                            }
                            ///////////////////////////////////////////////////////////////////////////////////////

                          },
                        ),
                        Center(
                          child: Container(
                            height: 1,
                            width: screenWidth(context, dividedBy: 1.2),
                            color: Constants.kitGradients[2].withOpacity(.06),
                          ),
                        ),
                        RoomPersonCount(
                          counterLabel: getTranslated(context, 'Children'),
                          counterValue: counterValueChildrenTotal[index],
                          incFunction: () {

                            int sumAdults =0;
                            for(int q=0;q<counterValueAdultsTotal.length;q++)
                            {
                              sumAdults=sumAdults+counterValueAdultsTotal[q];
                              print("total adult is "+sumAdults.toString());
                            }
                            int sumChildren=0;
                            for(int y=0;y<counterValueChildrenTotal.length;y++){
                              sumChildren=sumChildren+counterValueChildrenTotal[y];
                              print("total children is "+sumChildren.toString());
                            }
                            int sumTotal=sumAdults+sumChildren;
                            print("total guests"+sumTotal.toString());
                            int sumAdultPerRoom = counterValueAdultsTotal[index];
                            int sumChildrenPerRoom = counterValueChildrenTotal[index];
                            int SumPerRoomTotal = sumAdultPerRoom+sumChildrenPerRoom;

                            if(sumTotal>16){
                              setState(() {
                                showLimit=true;
                              });
                            }else if(sumTotal<=16){
                              showLimit=false;
                            }
                            if(SumPerRoomTotal>4){
                              setState(() {
                                showLimit=true;
                                showLimitPerRoom=true;
                              });
                            }else if(SumPerRoomTotal<=4){
                              showLimit=false;
                              showLimitPerRoom=false;
                            }

                            if((counterValueAdultsTotal[index] + counterValueChildrenTotal[index]<=3)&&(counterValueAdultsTotal[index]>=1)&& sumTotal<=16){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x++;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if((counterValueAdultsTotal[index] + counterValueChildrenTotal[index]<=3)&&(counterValueAdultsTotal[index]>=1) && sumTotal<=16){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x++;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if((counterValueAdultsTotal[index] + counterValueChildrenTotal[index]<=3)&&(counterValueAdultsTotal[index]>=1)&& sumTotal<=16){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x++;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if((counterValueAdultsTotal[index] + counterValueChildrenTotal[index]<=3)&&(counterValueAdultsTotal[index]>=1)&& sumTotal<=16){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x++;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if((counterValueAdultsTotal[index] + counterValueChildrenTotal[index]<=3)&&(counterValueAdultsTotal[index]>=1)&& sumTotal<=16){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x++;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if((counterValueAdultsTotal[index] + counterValueChildrenTotal[index]<=3)&&(counterValueAdultsTotal[index]>=1)&& sumTotal<=16){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x++;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if((counterValueAdultsTotal[index] + counterValueChildrenTotal[index]<=3)&&(counterValueAdultsTotal[index]>=1)&& sumTotal<=16){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x++;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }
                            if((counterValueAdultsTotal[index] + counterValueChildrenTotal[index]<=3)&&(counterValueAdultsTotal[index]>=1)&& sumTotal<=16){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x++;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if((counterValueAdultsTotal[index] + counterValueChildrenTotal[index]<=3)&&(counterValueAdultsTotal[index]>=1)&& sumTotal<=16){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x++;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                  ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
                            if ((counterValueAdults[index] + counterValueChildren[index]<=3)&&(counterValueAdults[index]>=1)) {
                              setState(() => counterValueChildren[index]++);
                            }else{
                              setState(() {
                               // showLimit=true;
                               // showLimitPerRoom=true;
                              });
                            }
                //////////////////////////////////////////////////////////////////////////////////////////////////////
                          },
                          decFunction: () {


                            int sumAdults =0;
                            for(int q=0;q<counterValueAdultsTotal.length;q++)
                            {
                              sumAdults=sumAdults+counterValueAdultsTotal[q];
                              print("total adult is "+sumAdults.toString());
                            }
                            int sumChildren=0;
                            for(int y=0;y<counterValueChildrenTotal.length;y++){
                              sumChildren=sumChildren+counterValueChildrenTotal[y];
                              print("total children is "+sumChildren.toString());
                            }
                            int sumTotal=sumAdults+sumChildren;
                            print("total guests"+sumTotal.toString());
                            int sumAdultPerRoom = counterValueAdultsTotal[index];
                            int sumChildrenPerRoom = counterValueChildrenTotal[index];
                            int SumPerRoomTotal = sumAdultPerRoom+sumChildrenPerRoom;

                            if(sumTotal>16){
                              setState(() {
                                showLimit=true;
                              });
                            }else if(sumTotal<=16){
                              showLimit=false;
                            }
                            if(SumPerRoomTotal>4){
                              setState(() {
                                showLimit=true;
                                showLimitPerRoom=true;
                              });
                            }else if(SumPerRoomTotal<=4){
                              showLimit=false;
                              showLimitPerRoom=false;
                            }

                            if(counterValueRoom==1 && counterValueChildrenTotal[index]>=1){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x--;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==2 && counterValueChildrenTotal[index]>=1){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x--;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==3 && counterValueChildrenTotal[index]>=1){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x--;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==4 && counterValueChildrenTotal[index]>=1){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x--;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==5 && counterValueChildrenTotal[index]>=1){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x--;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==6 && counterValueChildrenTotal[index]>=1){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x--;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==7 && counterValueChildrenTotal[index]>=1){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x--;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==8 && counterValueChildrenTotal[index]>=1){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x--;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            if(counterValueRoom==9 && counterValueChildrenTotal[index]>=1){
                              int x =  counterValueChildrenTotal[index];
                              setState(() {
                                x--;
                                counterValueChildrenTotal.removeAt(index);
                                counterValueChildrenTotal.insert(index, x);
                              });
                            }

                            /////////////////////////////////////////////////////////////////////////////////////
                            if (counterValueChildren[index] > 0) {
                              setState(() {
                                counterValueChildren[index]--;
                               // showLimit=false;
                               // showLimitPerRoom=false;
                              });
                              if (counterValueChildren[index] == 0) {
                                setState(() {
                                  agelistoflist[index].removeAt(0);
                                });
                              }
                              if (counterValueChildren[index] == 1) {
                                setState(() {
                                  agelistoflist[index].removeAt(1);
                                  agelistoflist[index].removeAt(2);
                                });
                              }
                            }
                            if (counterValueChildren[index] == 2) {
                              setState(() {
                                agelistoflist[index].removeAt(2);
                              });
                            }
                    //////////////////////////////////////////////////////////////////////////////////////////
                          },
                        ),
                        counterValueChildrenTotal[index] > 0
                            ? Container(
                          height: screenHeight(context, dividedBy: 30),
                          width: screenWidth(context, dividedBy: 1.2),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                          ),
                          child: Row(
                            children: [
                              SizedBox(
                                width:
                                screenWidth(context, dividedBy: 19.5),
                              ),
                              Text(
                                getTranslated(context, Constants.AGE_OF_CHILD),
                                style: TextStyle(
                                    color: Constants.kitGradients[2],
                                    fontSize: 12.0,
                                    fontWeight: FontWeight.w500,
                                    fontFamily: 'Poppins',
                                    fontStyle: FontStyle.normal),
                              ),
                            ],
                          ),
                        )
                            : Container(
                          height: screenHeight(context, dividedBy: 40),
                          width: screenWidth(context, dividedBy: 1.2),
                        ),
                        counterValueChildrenTotal[index] > 0
                            ? ChildrenCountUI(
                          hintText: hint1,
                          childrenCount: 1,
                          getage: (val) {
                            ageValue = val;
                            if (agelistoflist[index].isNotEmpty) {
                              setState(() {
                                agelistoflist[index][0] = ageValue;
                              });

                              // Hive.box('adult').put(4, agelistoflist);
                            } else {
                              agelistoflist[index].add(ageValue);
                              // Hive.box('adult').put(4, agelistoflist);
                            }
                          },
                        )
                            : Container(
                          height: 0,
                          width: 0,
                        ),
                        counterValueChildrenTotal[index] > 1
                            ? ChildrenCountUI(
                          hintText: hint2,
                          childrenCount: 2,
                          getage: (val2) {
                            ageValue = val2;
                            if (agelistoflist[index].length == 1) {
                              agelistoflist[index].add(ageValue);
                              // Hive.box('adult').put(4, agelistoflist);
                            } else {
                              setState(() {
                                agelistoflist[index][1] = ageValue;
                              });
                              // Hive.box('adult').put(4, agelistoflist);
                            }
                          },
                        )
                            : Container(
                          height: 0,
                          width: 0,
                        ),
                        counterValueChildrenTotal[index] > 2
                            ? ChildrenCountUI(
                          hintText: hint3,
                          childrenCount: 3,
                          getage: (val2) {
                            ageValue = val2;
                            if (agelistoflist[index].length == 2) {
                              agelistoflist[index].add(ageValue);
                              // Hive.box('adult').put(4, agelistoflist);
                            } else {
                              setState(() {
                                agelistoflist[index][2] = ageValue;
                              });
                              // Hive.box('adult').put(4, agelistoflist);
                            }
                          },
                        )
                            : Container(
                          height: 0,
                          width: 0,
                        ),
                      ],
                    ),
                  ),
                );
              },
            ),

            SizedBox(
              height: screenHeight(context, dividedBy: 5),
            ),
            Center(
              child: new BuildMaterialButton(
                  roomCount: counterValueRoom,
                  adultCount: counterValueAdults,
                  childrenCount: counterValueChildren,
                  ChildrenAgeList: agelistoflist,
                agelist1: agelistoflist[0],
                agelist2: agelistoflist[1],
                agelist3: agelistoflist[2],
                agelist4: agelistoflist[3],
                agelist5: agelistoflist[4],
                agelist6: agelistoflist[5],
                agelist7: agelistoflist[6],
                agelist8: agelistoflist[7],
                agelist9: agelistoflist[8],
                counterValueAdults1: counterValueAdultsTotal[0],
                counterValueAdults2: counterValueAdultsTotal[1],
                counterValueAdults3: counterValueAdultsTotal[2],
                counterValueAdults4: counterValueAdultsTotal[3],
                counterValueAdults5: counterValueAdultsTotal[4],
                counterValueAdults6: counterValueAdultsTotal[5],
                counterValueAdults7: counterValueAdultsTotal[6],
                counterValueAdults8: counterValueAdultsTotal[7],
                counterValueAdults9: counterValueAdultsTotal[8],
                counterValueChildren1: counterValueChildrenTotal[0],
                counterValueChildren2: counterValueChildrenTotal[1],
                counterValueChildren3: counterValueChildrenTotal[2],
                counterValueChildren4: counterValueChildrenTotal[3],
                counterValueChildren5: counterValueChildrenTotal[4],
                counterValueChildren6: counterValueChildrenTotal[5],
                counterValueChildren7: counterValueChildrenTotal[6],
                counterValueChildren8: counterValueChildrenTotal[7],
                counterValueChildren9: counterValueChildrenTotal[8],
              ),
            ),
            SizedBox(
              height: screenHeight(context, dividedBy: 20),
            ),
          ],
        ),
      ),
    );
  }
}
