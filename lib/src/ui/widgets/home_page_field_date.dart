import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/src/ui/widgets/date_range_picker.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';

// ignore: must_be_immutable
class HomePageDate extends StatefulWidget {
  String label;
  String hintText;
  Widget icon;
  Function onTap;
  HomePageDate({
    this.label,
    this.onTap,
    this.hintText,
    this.icon
  });
  @override
  _HomePageDateState createState() => _HomePageDateState();
}

class _HomePageDateState extends State<HomePageDate> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 15)),
      child: Container(
        height: screenHeight(context,dividedBy: 9),
        width: screenWidth(context,dividedBy: 1),
        child: Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              height: 60,
              width: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Color(0xFFE3E4E8),
              ),
              child: Padding(
                  padding: const EdgeInsets.all(15),
                  child:widget.icon
              ),
            ),
            SizedBox(
              width: screenWidth(context,dividedBy: 25),
            ),
            Container(
              height: screenHeight(context, dividedBy: 11),
              width: screenWidth(context, dividedBy: 1.6),
              // color: Colors.transparent,
              child: Padding(
                padding:  EdgeInsets.only(top: screenHeight(context,dividedBy: 100)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Padding(
                      padding:  EdgeInsets.only(top: screenWidth(context,dividedBy: 100)),
                      child: Container(
                        height: screenHeight(context, dividedBy: 40),
                        width: screenWidth(context, dividedBy: 1),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(25),
                          color: Colors.transparent,
                        ),
                        child: Text(
                          widget.label,
                          style: TextStyle(
                              color: Color(0xFFABABAB),
                              fontSize: 14,
                              fontFamily: 'poppins'),
                        ),
                      ),
                    ),
                    Container(
                      height: screenHeight(context,dividedBy: 25),
                      width: screenWidth(context,dividedBy: 1),
                      child: DateRangeField(
                        fieldVal: widget.hintText,
                        context: context,
                      )
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
