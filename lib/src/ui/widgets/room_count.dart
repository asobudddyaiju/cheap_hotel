import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';


class RoomCount extends StatefulWidget {
  final String counterLabel;
  final int counterValue;
  final Function incFunction;
  final Function decFunction;

  RoomCount(
      {this.counterLabel,
        this.decFunction,
        this.incFunction,
        this.counterValue});

  @override
  _RoomCountState createState() => _RoomCountState();
}

class _RoomCountState extends State<RoomCount> {
  // int c = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ListTile(
          title: Row(
            children: <Widget>[
              Container(
                width: screenWidth(context, dividedBy: 5),
                child: new Text(
                  widget.counterLabel,
                  style: TextStyle(
                      fontSize: 16,
                      color: Constants.kitGradients[1],
                      fontWeight: FontWeight.w300,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'Poppins'),
                ),
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 3),
              ),
              GestureDetector(
                child: SvgPicture.asset("assets/icons/dec_counter1.svg"),
                onTap: () {
                  widget.decFunction();
                  // setState(() {
                  //   c = widget.counterValue;
                  // });
                },
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                width: screenWidth(context, dividedBy: 12),
                child: Center(
                  child: Text(widget.counterValue.toString(),
                      style: TextStyle(
                          color: Constants.kitGradients[1],
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontFamily: 'Poppins')),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              GestureDetector(
                  child: SvgPicture.asset("assets/icons/inc_counter1.svg"),
                  onTap: () {
                    widget.incFunction();
                    // setState(() {
                    //   c = widget.counterValue;
                    // });
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
