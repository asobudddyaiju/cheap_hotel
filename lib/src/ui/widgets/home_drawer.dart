import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/localization/locale.dart';
import 'package:hotel_booking_1/src/ui/screens/currency.dart';
import 'package:hotel_booking_1/src/ui/screens/destination_page.dart';
import 'package:hotel_booking_1/src/ui/screens/profile_page.dart';
import 'package:hotel_booking_1/src/auth_service/firebase_auth.dart';
import 'package:hotel_booking_1/src/ui/screens/login.dart';
import 'package:hotel_booking_1/src/ui/screens/region.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/object_factory.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';

class HomeDrawer extends StatefulWidget {
  final FirebaseAuth auth, authfb;

  const HomeDrawer({this.auth, this.authfb});

  @override
  // _HomeDrawerState createState() => _HomeDrawerState();
  State<StatefulWidget> createState() {
    return _HomeDrawerState();
  }
}

class _HomeDrawerState extends State<HomeDrawer> {
  String buttonName = "Sign_in";
  String photpurl;
  String dummyphotourl =
      'https://upload.wikimedia.org/wikipedia/commons/thumb/d/d8/Antu_system-switch-user.svg/768px-Antu_system-switch-user.svg.png';
  String displayName;
  String dummydisplayName = 'username';
  bool show_profile;
  String defaultLang = Platform.localeName;

  @override
  void initState() {
    if ((widget.auth.currentUser != null) ||
        (widget.authfb.currentUser != null)) {
      setState(() {
        show_profile = true;
        buttonName = "Sign Out";
        photpurl = Hive.box('lang').get(12);
        displayName = Hive.box('lang').get(13);
      });
    } else {
      setState(() {
        show_profile = false;
      });
    }
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new Drawer(
      child: ListView(
        children: <Widget>[
          SizedBox(
            height: screenHeight(context, dividedBy: 35),
          ),
          show_profile == true
              ? Padding(
                  padding: EdgeInsets.only(left: 15.0,top: 20,bottom: 20),
                  child: Row(
                    children: [
                      Container(
                        width: 38.0,
                        height: 40.24,
                        decoration: BoxDecoration(
                          color: Colors.black.withOpacity(.05),
                          shape: BoxShape.circle,
                          image: DecorationImage(
                              image: NetworkImage(photpurl), fit: BoxFit.fill),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(left: 15.0),
                        child: Text(displayName,
                            style: TextStyle(
                                color: Color(0xFF272727),
                                fontWeight: FontWeight.w300,
                                fontSize: 18.0,
                                fontStyle: FontStyle.normal,
                                fontFamily: 'NexaLight')),
                      ),
                    ],
                  ),
                )
              :Padding(
                  padding:
                      const EdgeInsets.symmetric(vertical: 20, horizontal: 40),
                  child: MaterialButton(
                    color: Constants.kitGradients[0],
                    onPressed: () {
                      if ((widget.auth.currentUser == null) &&
                          (widget.authfb.currentUser == null)) {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => Login(
                                    auth: widget.auth, authFb: widget.authfb)));
                      } else {
                        void signout() async {
                          await Auth(auth: widget.auth, authfb: widget.authfb)
                              .Signout();
                        }

                        signout();

                        setState(() {
                          buttonName = "Sign in";
                          photpurl = dummyphotourl;
                          displayName = getTranslated(context, displayName);
                        });
                      }
                    },
                    minWidth: 10,
                    height: 40,
                    shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(30)),
                    child: Text(
                      getTranslated(context, buttonName),
                      style: TextStyle(fontSize: 18, fontWeight: FontWeight.w700, fontFamily: 'Poppins',color: Colors.white),
                    ),
                  ),
                ),

          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          SizedBox(
            height: screenHeight(context, dividedBy: 95),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18),
            child: GestureDetector(
              child: Container(
                color: Colors.transparent,
                height: screenHeight(context,dividedBy: 15),
                child: Row(
                 children: [
                   SvgPicture.asset("assets/images/search_icon.svg"),
                   SizedBox(
                     width: screenWidth(context,dividedBy: 25),
                   ),
                   Text(
                     getTranslated(context, 'Search'),
                     style: TextStyle(
                         color: Colors.black.withOpacity(.50),
                         fontWeight: FontWeight.w200),
                   ),
                 ],
                ),
              ),
              onTap: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => DestinationPage()));
              },
            ),
          ),
           Padding(
            padding: const EdgeInsets.symmetric(horizontal: 18),
            child: GestureDetector(
              child: Container(
                color: Colors.transparent,
                height: screenHeight(context,dividedBy: 15),
                child: Row(
                 children: [
                   Icon(
                     Icons.settings,
                     color: Colors.black.withOpacity(.60),
                   ),
                   SizedBox(
                     width: screenWidth(context,dividedBy: 25),
                   ),
                   Text(
                     getTranslated(context, 'Profile'),
                     style: TextStyle(
                         color: Colors.black.withOpacity(.50),
                         fontWeight: FontWeight.w200),
                   ),
                 ],
                ),
              ),
              onTap: (){
                Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ProfilePage(
                          auth: widget.auth,
                          authfb: widget.authfb,
                        )));
              },
            ),
          ),


          SizedBox(
            height: screenHeight(context, dividedBy: 2.5),
          ),
          Container(
            width: screenWidth(context, dividedBy: 1),
            height: 1,
            color: Colors.grey.withOpacity(.10),
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              SizedBox(
                height: screenHeight(context,dividedBy: 35),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: screenHeight(context, dividedBy: 75),
                    left: screenWidth(context, dividedBy: 20),
                    right: screenWidth(context, dividedBy: 20,)),
                child: Text(
                  getTranslated(context, 'language'),
                  style: TextStyle(fontSize: 14, color: Colors.black),
                ),
              ),
              Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 100),
                      left: screenWidth(context, dividedBy: 20),
                      right: screenWidth(context, dividedBy: 20,)),
                  child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => RegionPage()));
                      },
                      child: Text(
                        Hive.box('lang').get(2) != null
                            ? Hive.box('lang').get(2)
                            :ObjectFactory().getLanguage.getCountryLanguage(defaultLang)[1]
                        ,
                        // details.name,
                        style: TextStyle(fontSize: 12),
                      )))
            ],
          ),
          Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.only(
                    top: screenHeight(context, dividedBy: 35),
                    left: screenWidth(context, dividedBy: 20,),right: screenWidth(context, dividedBy: 20,)),
                child: Text(
                  getTranslated(context, 'Currency'),
                  style: TextStyle(fontSize: 14, color: Colors.black),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(
                    top: screenHeight(context, dividedBy: 100),
                    left: screenWidth(context, dividedBy: 20),
                    right: screenWidth(context, dividedBy: 20,)),
                child: GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => CurrencyPage()));
                    },
                    child: Text(
                        Hive.box('code').get(2) != null
                            ? Hive.box('code').get(2).toString().split(" ").first
                            : Hive.box('room').get('country')!=null?ObjectFactory().getCurrency.getCurrency(Hive.box('room').get('currency')):"USD",
                        style: TextStyle(fontSize: 12))),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
