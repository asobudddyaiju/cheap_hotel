import 'dart:io';

import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/localization/locale.dart';
import 'package:hotel_booking_1/src/ui/screens/home_screen.dart';

import 'package:hotel_booking_1/src/ui/widgets/multi_date_range_picker.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/object_factory.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';
import 'package:intl/date_symbol_data_local.dart';
import 'package:intl/intl.dart';

class DatePicker extends StatefulWidget {
  const DatePicker({Key key}) : super(key: key);

  @override
  _DatePickerState createState() => _DatePickerState();
}

class _DatePickerState extends State<DatePicker> {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  // List<List<DateTime>> initialInterval = [
  //   [
  //     DateTime.now().add(Duration(days: Hive.box('adult').get('checkIn') !=null ?Hive.box('adult').get('checkIn'):0 )),
  //     DateTime.now().add(Duration(days: Hive.box('adult').get('checkOut') !=null ?Hive.box('adult').get('checkOut'):0))
  //
  //   ]
  // ];
  List<List<DateTime>> intervals = [
    [
      DateTime.now().add(Duration(days: Hive.box('adult').get('checkIn') !=null ?Hive.box('adult').get('checkIn'):0)),
      DateTime.now().add(Duration(days: Hive.box('adult').get('checkOut') !=null ?Hive.box('adult').get('checkOut'):1))
    ]
  ];
   String defaultLocale = Platform.localeName;
String day2;
  @override
  void initState() {
    print(defaultLocale);
    initializeDateFormatting();
    Intl.defaultLocale = Hive.box('code').get(10)!=null ? Hive.box('code').get(10) : ObjectFactory().getLanguage.getCountryLanguage(defaultLocale)[3];

  }

  Widget build(BuildContext context) {
    return Scaffold(
      key: _globalKey,
      backgroundColor: Colors.white,
      floatingActionButton: MaterialButton(
          disabledColor: Constants.kitGradients[0],
          height: 50,
          minWidth: screenWidth(context, dividedBy: 1.2),
          shape: RoundedRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(25.0))),
          child:Text(getTranslated(context, 'Done'),
            style: TextStyle(color: Colors.white,fontSize: 18),
          ),
          color: Constants.kitGradients[0],
          onPressed: () {
            Navigator.pushAndRemoveUntil(
                context,
                MaterialPageRoute(builder: (context) => HomePage()),
                (route) => false);
          }),
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 15)),
          child: AppBar(
            title: Text(
              getTranslated(context, 'Select_dates'),
              style: TextStyle(color: Colors.black),
            ),
            leading: new IconButton(
              onPressed: () {
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                        (route) => false);
              },
              icon: new Icon(
                Icons.close,
                color: Colors.black,
              ),
            ),
            backgroundColor: Colors.white,
            elevation: 0.5,
          )),
      body: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            // SizedBox(
            //   height: screenHeight(context, dividedBy: 100),
            // ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[

                Card(
                  elevation: 0.2,
                  child: MultiDateRangePicker(
                    initialValue: intervals,
                    //  Hive.box('date').get(1) != null ? Hive.box('date').get(1):intervals,
                    onChanged: (List<List<DateTime>> intervals) async {
                      print("CheckIn CheckOut Interval Days" +
                          intervals.toString());
                      // setState(() {
                      //   initialInterval = intervals;
                      // });
                      // initialInterval = intervals;
                      Hive.box('date').put(1, intervals);
                      checkinDateCheck(intervals);
                      print(Hive.box('date').get(1));
                    },
                    selectedDateTextColor: Colors.white,
                    onlyOne: true,
                    dateTextColor: Constants.kitGradients[0],
                    buttonTextColor: Colors.white,
                    primaryTextColor: Constants.kitGradients[0],
                    selectionColor: Constants.kitGradients[0].withOpacity(.80),
                    buttonColor: Constants.kitGradients[0],
                  ),
                ),
                SizedBox(
                  height: 4,
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        children: [
                          Card(
                            elevation: 0.2,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 8.0, vertical: 16.0),
                              child: Container(
                                width: screenWidth(context, dividedBy: 1),
                                child: Column(
                                  children: buildColumn1(),
                                ),
                              ),
                            ),
                          ),
                          Card(
                            elevation: 0.2,
                            child: Padding(
                              padding: const EdgeInsets.symmetric(
                                  horizontal: 8.0, vertical: 16.0),
                              child: Container(
                                width: screenWidth(context, dividedBy: 1),
                                child: Column(
                                  children: buildColumn2(),
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  List<Widget> buildColumn1() {
    final List<Widget> list = [];

    for (final interval in intervals) {
      Hive.box('date').put('weekday1', interval[0].weekday.toString());
      Hive.box('lang').put('checkInDay', interval[0].day.toString());
      Hive.box('lang').put('checkInMonth', interval[0].month.toString());
      Hive.box('lang').put('checkInYear', interval[0].year.toString());
      list.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            getTranslated(context, 'Check-in_Date'),
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Constants.kitGradients[7],
                fontSize: 16,
                fontWeight: FontWeight.w300,
                fontFamily: 'NexaLight'),
          ),
          Text(Hive.box('lang').get('checkInDay') == null ?
            interval[0].day.toString() +
                " " +
                getTranslated(context, ObjectFactory()
                    .getMonthAlpha
                    .getCheckInMonth(interval[0].month.toString())) +
                " " +
                interval[0].year.toString() :
          Hive.box('lang').get('checkInDay') +
              " " +
              getTranslated(context, ObjectFactory()
                  .getMonthAlpha
                  .getCheckInMonth(Hive.box('lang').get('checkInMonth'))) +
              " " +
              Hive.box('lang').get('checkInYear'),
            style: TextStyle(
                color: Constants.kitGradients[9],
                fontWeight: FontWeight.w700,
                fontSize: 14.34,
                fontStyle: FontStyle.normal,
                fontFamily: 'NexaBold'),
          )
        ],
      ));

      if (interval != intervals.last)
        list.add(Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              getTranslated(context, 'Check-in_Date'),
              textAlign: TextAlign.left,
              style: TextStyle(
                  color: Constants.kitGradients[7],
                  fontSize: 16,
                  fontWeight: FontWeight.w300,
                  fontFamily: 'NexaLight'),
            ),
            Text(
              "checkin",
              style: TextStyle(
                  color: Constants.kitGradients[9],
                  fontWeight: FontWeight.w700,
                  fontSize: 14.34,
                  fontStyle: FontStyle.normal,
                  fontFamily: 'NexaBold'),
            )
          ],
        ));
    }

    return list;
  }

  List<Widget> buildColumn2() {
    final List<Widget> list = [];

    for (final interval in intervals) {
      Hive.box('date').put('weekday2', interval[1].weekday.toString());
      list.add(Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            getTranslated(context, 'Check-out_Date'),
            textAlign: TextAlign.left,
            style: TextStyle(
                color: Constants.kitGradients[7],
                fontSize: 16,
                fontWeight: FontWeight.w300,
                fontFamily: 'NexaLight'),
          ),
          Text(
            Hive.box('lang').get('checkOutDay') == null ?
            interval[1].day.toString() +
                " " +
                getTranslated(context, ObjectFactory()
                    .getMonthAlpha
                    .getCheckInMonth(interval[1].month.toString())) +
                " " +
                interval[1].year.toString() :
            (interval[1].day).toString()+
                " " +
                getTranslated(context, ObjectFactory()
                    .getMonthAlpha
                    .getCheckInMonth(interval[1].month.toString())) +
                " " +
                interval[1].year.toString(),
            // day2 :
            // Hive.box('lang').get('checkOutDay') +
            //     " " +
            //     ObjectFactory()
            //         .getMonthAlpha
            //         .getCheckInMonth(Hive.box('lang').get('checkOutMonth')) +
            //     " " +
            //     Hive.box('lang').get('checkOutYear'),
            style: TextStyle(
                color: Constants.kitGradients[9],
                fontWeight: FontWeight.w700,
                fontSize: 14.34,
                fontStyle: FontStyle.normal,
                fontFamily: 'NexaBold'),
          )
        ],
      )
      );
      Hive.box('lang').put('checkOutDay', (interval[1].day).toString());
      Hive.box('lang').put('checkOutMonth', interval[1].month.toString());
      Hive.box('lang').put('checkOutYear', interval[1].year.toString());
      if (interval != intervals.last)
        list.add(Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Text(
              getTranslated(context, 'Check-out_Date'),
              textAlign: TextAlign.left,
              style: TextStyle(
                  color: Constants.kitGradients[7],
                  fontSize: 16,
                  fontWeight: FontWeight.w300,
                  fontFamily: 'NexaLight'),
            ),
            Text(
              "checkout",
              style: TextStyle(
                  color: Constants.kitGradients[9],
                  fontWeight: FontWeight.w700,
                  fontSize: 14.34,
                  fontStyle: FontStyle.normal,
                  fontFamily: 'NexaBold'),
            )
          ],
        ));
    }

    return list;
  }

  void showSnackMethod(String snackmsg) {
    final snackbar = SnackBar(
      content: Text(
        snackmsg,
        style: TextStyle(
            fontStyle: FontStyle.normal, fontFamily: 'Poppins', fontSize: 14.0),
      ),
      duration: Duration(seconds: 3),
      action: SnackBarAction(
        label: "OK",
        textColor: Colors.yellow,
        onPressed: () {
          Navigator.push(
              context, MaterialPageRoute(builder: (context) => DatePicker()));
        },
      ),
      backgroundColor: Constants.kitGradients[0],
    );
    _globalKey.currentState.showSnackBar(snackbar);
  }

  void checkinDateCheck(List<List<DateTime>> intervals) {
    if (intervals[0][0].year > DateTime.now().year) {
      setState(() {
        this.intervals = intervals;
      });
      // Hive.box('lang').put(
      //     'date',
      //     []);
      Hive.box('lang').put(
          5,
          intervals[0][0].year.toString() +
              '-' +
              intervals[0][0].month.toString() +
              '-' +
              intervals[0][0].day.toString());

      Hive.box('lang').put(
          6,
          intervals[0][1].year.toString() +
              '-' +
              intervals[0][1].month.toString() +
              '-' +
              intervals[0][1].day.toString());

      Hive.box('code').put(3, intervals[0][0].day.toString());
      Hive.box('code').put(5, intervals[0][0].month.toString());
      Hive.box('code').put(22, intervals[0][0].year.toString());
      Hive.box('code').put(6, intervals[0][1].month.toString());
      Hive.box('code').put(4, intervals[0][1].day.toString());
      Hive.box('code').put(23, intervals[0][1].year.toString());

      // initialCheckIn(intervals);
      // initialCheckOut(intervals);
      numberOfDays(intervals);
    }
    if (intervals[0][0].year < DateTime.now().year) {
      showSnackMethod(getTranslated(context, 'Please_choose_a_valid_Check-in_date'));
    }
    if (intervals[0][0].year == DateTime.now().year) {
      //for check month
      if (intervals[0][0].month < DateTime.now().month) {
        showSnackMethod(getTranslated(context, 'Please_choose_a_valid_Check-in_date'));
      }
      if (intervals[0][0].month == DateTime.now().month) {
        if (intervals[0][0].day >= DateTime.now().day) {
          setState(() {
            this.intervals = intervals;
          });
          // Hive.box('lang').put(
          //     'date',
          //     [
          //       intervals[0][0].year,intervals[0][0].month,intervals[0][0].day
          //     ]);
          Hive.box('lang').put(
              5,
              intervals[0][0].year.toString() +
                  '-' +
                  intervals[0][0].month.toString() +
                  '-' +
                  intervals[0][0].day.toString());

          Hive.box('lang').put(
              6,
              intervals[0][1].year.toString() +
                  '-' +
                  intervals[0][1].month.toString() +
                  '-' +
                  intervals[0][1].day.toString());

          Hive.box('code').put(3, intervals[0][0].day.toString());
          Hive.box('code').put(5, intervals[0][0].month.toString());
          Hive.box('code').put(22, intervals[0][0].year.toString());
          Hive.box('code').put(6, intervals[0][1].month.toString());
          Hive.box('code').put(4, intervals[0][1].day.toString());
          Hive.box('code').put(23, intervals[0][1].year.toString());

          // initialCheckIn(intervals);
          // initialCheckOut(intervals);
          numberOfDays(intervals);
          print('year,month and day are ok');
        } else {
          print('day is gone');
          showSnackMethod(getTranslated(context, 'Please_choose_a_valid_Check-in_date'));
        }
        print('month is this month');
      }
      //for upcoming month
      else if (intervals[0][0].month >= DateTime.now().month) {
        setState(() {
          this.intervals = intervals;
        });
        // Hive.box('lang').put(
        //     'date',
        //     [
        //       intervals[0][0].year,intervals[0][0].month,intervals[0][0].day
        //     ]);
        Hive.box('lang').put(
            5,
            intervals[0][0].year.toString() +
                '-' +
                intervals[0][0].month.toString() +
                '-' +
                intervals[0][0].day.toString());

        Hive.box('lang').put(
            6,
            intervals[0][1].year.toString() +
                '-' +
                intervals[0][1].month.toString() +
                '-' +
                intervals[0][1].day.toString());

        Hive.box('code').put(3, intervals[0][0].day.toString());
        Hive.box('code').put(5, intervals[0][0].month.toString());
        Hive.box('code').put(22, intervals[0][0].year.toString());
        Hive.box('code').put(6, intervals[0][1].month.toString());
        Hive.box('code').put(4, intervals[0][1].day.toString());
        Hive.box('code').put(23, intervals[0][1].year.toString());

        numberOfDays(intervals);
        // initialCheckIn(intervals);
        // initialCheckOut(intervals);
        print('month is upcoming month');
      }
      print("year is ok");
    }
  }
}
