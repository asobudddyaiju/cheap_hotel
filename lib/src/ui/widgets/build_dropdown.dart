// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:hive/hive.dart';
// import 'package:hotel_booking_1/src/app/app.dart';
//
// class Country extends StatefulWidget {
//   @override
//   _CountryState createState() => _CountryState();
// }
//
// String local = Hive.box('code').get(10);
// String lang = Hive.box('code').get(1);
// String country = Hive.box('locale').get(1);
// String language1 = Hive.box('locale').get(2);
//
// class _CountryState extends State<Country> {
//   String dropdownvalue = Hive.box('lang').get(2);
//
//   List<String> countries = [
//     "Arabic",
//     "Bulgary",
//     // "Brazil",
//     "Catalan",
//     "China",
//     // "China(T)",
//     "Czech",
//     "Croatia",
//     "Denmark",
//     "Estonia",
//     "Finland",
//     "France",
//     "Germany",
//     "Greek",
//     "Hungary",
//     // "Hongkong",
//     "Iceland",
//     "Indonesia",
//     "Israel",
//     "Italy",
//     "Japan",
//     "Korea",
//     "Latvia",
//     "Lithuania",
//     "Malaysia",
//     "Netherlands",
//     "Norway",
//     "Philippines",
//     "Poland",
//     "Portugal",
//     "Romania",
//     "Russia",
//     "Serbia",
//     "Slovakia",
//     "Slovenia",
//     "Spain",
//     "Sweden",
//     "Thailand",
//     "Turkey",
//     "Ukrania",
//     "USA",
//     "Vietnam",
//   ];
//
//   void _changeLanguage(language) async {
//     Locale _temp;
//     switch (language) {
//       case "Arabic":
//         _temp = Locale('ar', 'AE');
//         local = 'ar';
//         lang = 'AR';
//         country = 'AE';
//         language1 = 'ar';
//         break;
//       case "Bulgary":
//         _temp = Locale('bg', 'BG');
//         local = 'bg';
//         lang = 'BG';
//         country = 'BG';
//         language1 = 'bg';
//         break;
//       case "Catalan":
//         _temp = Locale('ca', 'CA');
//         local = 'ca';
//         lang = 'CA';
//         country = 'CA';
//         language1 = 'ca';
//         break;
//       case "Czech":
//         _temp = Locale('cs', 'CZ');
//         local = 'cs';
//         lang = 'CZ';
//         country = 'CZ';
//         language1 = 'cs';
//         break;
//       case "Denmark":
//         _temp = Locale('da', 'DK');
//         local = 'da';
//         lang = 'DA';
//         country = 'DK';
//         language1 = 'da';
//         break;
//       case "Germany":
//         _temp = Locale('de', 'DE');
//         local = 'de';
//         lang = 'DE';
//         country = 'DE';
//         language1 = 'de';
//         break;
//       case "Greek":
//         _temp = Locale('el', 'GR');
//         local = 'el';
//         lang = 'EL';
//         country = 'GR';
//         language1 = 'el';
//         break;
//       case "USA":
//         _temp = Locale('en', 'US');
//         local = 'en_US';
//         lang = 'EN';
//         country = 'US';
//         language1 = 'en';
//         break;
//       case "Spain":
//         _temp = Locale('es', 'ES');
//         local = 'es';
//         lang = 'ES';
//         country = 'ES';
//         language1 = 'es';
//         break;
//       case "Estonia":
//         _temp = Locale('et', 'EE');
//         local = 'et';
//         lang = 'ET';
//         country = 'EE';
//         language1 = 'et';
//         break;
//       // case "Brazil":
//       //   _temp = Locale('fa', 'BR');
//       //   local = 'pt_BR';
//       //   lang = 'PB';
//       //   country = 'PT';
//       //   language1 = 'pt';
//       //   break;
//       case "Finland":
//         _temp = Locale('fi', 'FI');
//         local = 'fi';
//         lang = 'FI';
//         country = 'FI';
//         language1 = 'fi';
//         break;
//       case "France":
//         _temp = Locale('fr', 'FR');
//         local = 'fr';
//         lang = 'FR';
//         country = 'FR';
//         language1 = 'fr';
//         break;
//       case "Croatia":
//         _temp = Locale('hr', 'HR');
//         local = 'hr';
//         lang = 'HR';
//         country = 'HR';
//         language1 = 'hr';
//         break;
//       case "Hungary":
//         _temp = Locale('hu', 'HU');
//         local = 'hu';
//         lang = 'HU';
//         country = 'HU';
//         language1 = 'hu';
//         break;
//       case "Indonesia":
//         _temp = Locale('id', 'ID');
//         local = 'id';
//         lang = 'ID';
//         country = 'ID';
//         language1 = 'id';
//         break;
//       case "Israel":
//         _temp = Locale('he', 'IL');
//         local = 'he';
//         lang = 'HE';
//         country = 'IL';
//         language1 = 'he';
//         break;
//       case "Iceland":
//         _temp = Locale('is', 'IS');
//         local = 'is';
//         lang = 'IS';
//         country = 'IS';
//         language1 = 'is';
//         break;
//       case "Italy":
//         _temp = Locale('it', 'IT');
//         local = 'it';
//         lang = 'IT';
//         country = 'IT';
//         language1 = 'it';
//         break;
//       case "Japan":
//         _temp = Locale('ja', 'JP');
//         local = 'ja';
//         lang = 'JA';
//         country = 'JP';
//         language1 = 'ja';
//         break;
//       case "Korea":
//         _temp = Locale('ko', 'KO');
//         local = 'ko';
//         lang = 'KO';
//         country = 'KO';
//         language1 = 'ko';
//         break;
//       case "Lithuania":
//         _temp = Locale('lt', 'LT');
//         local = 'lt';
//         lang = 'LT';
//         country = 'LT';
//         language1 = 'lt';
//         break;
//       case "Latvia":
//         _temp = Locale('lv', 'LV');
//         local = 'lv';
//         lang = 'LV';
//         country = 'LV';
//         language1 = 'lv';
//         break;
//       case "Malaysia":
//         _temp = Locale('ms', 'MY');
//         local = 'ms';
//         lang = 'MS';
//         country = 'MY';
//         language1 = 'ms';
//         break;
//       case "Netherlands":
//         _temp = Locale('nl', 'NL');
//         local = 'nl';
//         lang = 'NL';
//         country = 'NL';
//         language1 = 'nl';
//         break;
//       case "Norway":
//         _temp = Locale('no', 'NO');
//         local = 'no';
//         lang = 'NO';
//         country = 'NO';
//         language1 = 'no';
//         break;
//       case "Poland":
//         _temp = Locale('pl', 'PL');
//         local = 'pl';
//         lang = 'PL';
//         country = 'PL';
//         language1 = 'pl';
//         break;
//       case "Portugal":
//         _temp = Locale('pt', 'PT');
//         local = 'pt';
//         lang = 'PT';
//         country = 'PT';
//         language1 = 'pt';
//         break;
//       case "Romania":
//         _temp = Locale('ro', 'RO');
//         local = 'ro';
//         lang = 'RO';
//         country = 'RO';
//         language1 = 'ro';
//         break;
//       case "Russia":
//         _temp = Locale('ru', 'RU');
//         local = 'ru';
//         lang = 'RU';
//         country = 'RU';
//         language1 = 'ru';
//         break;
//       case "Slovakia":
//         _temp = Locale('sk', 'SK');
//         local = 'sk';
//         lang = 'SK';
//         country = 'SK';
//         language1 = 'sk';
//         break;
//       case "Slovenia":
//         _temp = Locale('sl', 'SL');
//         local = 'sl';
//         lang = 'SL';
//         country = 'SL';
//         language1 = 'sl';
//         break;
//       case "Serbia":
//         _temp = Locale('sr', 'SP');
//         local = 'sr';
//         lang = 'SR';
//         country = 'SP';
//         language1 = 'sr';
//         break;
//       case "Sweden":
//         _temp = Locale('sv', 'SE');
//         local = 'sv';
//         lang = 'SV';
//         country = 'SE';
//         language1 = 'sv';
//         break;
//       // case "China(T)":
//       //   _temp = Locale('te', 'TW');
//       //   local = 'zh_TW';
//       //   lang = 'CN';
//       //   country = 'CN';
//       //   language1 = 'zh';
//       //   break;
//       case "Thailand":
//         _temp = Locale('th', 'TH');
//         local = 'th';
//         lang = 'TH';
//         country = 'TH';
//         language1 = 'th';
//         break;
//       case "Philippines":
//         _temp = Locale('tl', 'PH');
//         local = 'tl';
//         lang = 'TL';
//         country = 'PH';
//         language1 = 'tl';
//         break;
//       case "Turkey":
//         _temp = Locale('tr', 'TR');
//         local = 'tr';
//         lang = 'TR';
//         country = 'TR';
//         language1 = 'tr';
//         break;
//       case "Ukrania":
//         _temp = Locale('uk', 'UA');
//         local = 'uk';
//         lang = 'UK';
//         country = 'UA';
//         language1 = 'uk';
//         break;
//       // case "Hongkong":
//       //   _temp = Locale('ur', 'HK');
//       //   local = 'zh_HK';
//       //   lang = 'CS';
//       //   country = 'CN';
//       //   language1 = 'zh';
//       //   break;
//       case "Vietnam":
//         _temp = Locale('vi', 'VN');
//         local = 'vi';
//         lang = 'VI';
//         country = 'VN';
//         language1 = 'vi';
//         break;
//       case "China":
//         _temp = Locale('zh', 'CN');
//         local = 'zh_CN';
//         lang = 'CS';
//         country = 'Cn';
//         language1 = 'zh';
//         break;
//       default:
//         _temp = Locale('en', 'US');
//         local = 'en_US';
//         lang = 'EN';
//         country = 'US';
//         language1 = 'en';
//     }
//     Hive.box('locale').put(1, country);
//     Hive.box('code').put(10, local);
//     Hive.box('code').put(1, lang);
//     Hive.box('locale').put(2, language1);
//     MyApp.setLocale(context, _temp);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return DropdownButtonHideUnderline(
//       child: Container(
//         height: 10,
//         child: DropdownButton<String>(
//           value: dropdownvalue,
//           iconSize: 0.0,
//           style: TextStyle(fontSize: 9.0, color: Colors.blue),
//           items: countries
//               .map<DropdownMenuItem<String>>((String value) => DropdownMenuItem(
//                     value: value,
//                     child: Text(value),
//                   ))
//               .toList(),
//           onChanged: (language) {
//             _changeLanguage(language);
//             Hive.box('lang').put(2, language);
//             dropdownvalue = language;
//           },
//           hint: Text(
//             'USA',
//             style: TextStyle(fontSize: 9.0, color: Colors.blue),
//           ),
//         ),
//       ),
//     );
//   }
// }
