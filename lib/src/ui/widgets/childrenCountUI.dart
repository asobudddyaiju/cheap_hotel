import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_dropdown/flutter_dropdown.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/localization/locale.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/object_factory.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';


class ChildrenCountUI extends StatefulWidget {
  final int childrenCount;
  final ValueChanged<String> getage;
  String hintText;

  ChildrenCountUI({this.childrenCount, this.getage, this.hintText});

  @override
  _ChildrenCountUIState createState() => _ChildrenCountUIState();
}

class _ChildrenCountUIState extends State<ChildrenCountUI> {
  @override
  void initState() {
    if (widget.hintText == null) {
      widget.hintText = "age";
    }
    super.initState();
  }

  String dropdownvalue;
  List<String> childrenAgeList = [
    "1 Year old",
    "2 Years old",
    "3 Years old",
    "4 Years old",
    "5 Years old",
    "6 Years old",
    "7 Years old",
    "8 Years old",
    "9 Years old",
    "10 Years old",
    "11 Years old",
    "12 Years old",
    "13 Years old",
    "14 Years old",
    "15 Years old",
    "16 Years old",
    "17 Years old",
  ];

  @override
  Widget build(BuildContext context) {
    String hintFinal;
    if (widget.hintText != null) {
      hintFinal = widget.hintText;
    } else {
      hintFinal = "age";
    }

    return Container(
      child: Row(
        children: <Widget>[
          SizedBox(
            width: screenWidth(context, dividedBy: 20),
          ),
          Container(
            width: screenWidth(context, dividedBy: 5.5),
            child: new Text(
              getTranslated(context, 'child') +" "+
                  (widget.childrenCount > 0
                      ? widget.childrenCount.toString()
                      : ""),
              style: TextStyle(
                  fontSize: 16,
                  color: Constants.kitGradients[2],
                  fontWeight: FontWeight.w300,
                  fontStyle: FontStyle.normal,
                  fontFamily: 'Poppins'),
            ),
          ),
          SizedBox(
            width: screenWidth(context, dividedBy: 2.2),
          ),
          Container(
              // width: 100,
              height: 60,
              child: DropdownButtonHideUnderline(
                child: DropdownButton<String>(
                  items: childrenAgeList
                      .map<DropdownMenuItem<String>>((String value) =>
                          DropdownMenuItem<String>(
                              value: value, child: Text(getTranslated(context, value))))
                      .toList(),
                  hint: Text(
                    getTranslated(context, hintFinal.toString()),
                    style: TextStyle(
                        color: Constants.kitGradients[0],
                        fontSize: 12.0,
                        fontWeight: FontWeight.w500,
                        fontStyle: FontStyle.normal,
                        fontFamily: 'Poppins'),
                  ),
                  iconSize: 0.0,
                  isExpanded: false,
                  value: dropdownvalue,
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontSize: 12.0,
                      fontWeight: FontWeight.w500,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'Poppins'),
                  onChanged: (value) {
                    widget.getage(value);
                    setState(() {
                      dropdownvalue = value;
                    });
                  },
                ),
              )),
        ],
      ),
    );
  }
}
