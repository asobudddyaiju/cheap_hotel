// import 'package:find_dropdown/find_dropdown.dart';
// import 'package:flutter/material.dart';
// import 'package:hotel_booking/localization/Language.dart';
// import 'package:hotel_booking/src/app/app.dart';
//
// class DropDown extends StatefulWidget {
//   static const routeName = '/drop-down';
//
//   @override
//   _DropDownState createState() => _DropDownState();
// }
//
// class _DropDownState extends State<DropDown> {
//   List<String> countries = [
//     "Arabic",
//     "Bulgary",
//     "Brazil",
//     "Catalan",
//     "China",
//     "China(T)",
//     "Czech",
//     "Croatia",
//     "Denmark",
//     "Estonia",
//     "Finland",
//     "France",
//     "Germany",
//     "Greek",
//     "Hungary",
//     "Hongkong",
//     "Iceland",
//     "Indonesia",
//     "Israel",
//     "Italy",
//     "Japan",
//     "Korea",
//     "Latvia",
//     "Lithuania",
//     "Malaysia",
//     "Netherlands",
//     "Norway",
//     "Philippines",
//     "Poland",
//     "Portugal",
//     "Romania",
//     "Russia",
//     "Serbia",
//     "Slovakia",
//     "Slovenia",
//     "Spain",
//     "Sweden",
//     "Thailand",
//     "Turkey",
//     "Ukrania",
//     "USA",
//     "Vietnam",
//   ];
//   String lang = "";
//
//   void _changeLanguage(Language language) {
//     Locale _temp;
//
//     switch (language.languageCode) {
//       case "Arabic":
//         _temp = Locale(language.languageCode, 'AE');
//
//         break;
//       case "Bulgary":
//         _temp = Locale(language.languageCode, 'BG');
//
//         break;
//       case "Catalan":
//         _temp = Locale(language.languageCode, 'CA');
//         break;
//       // case "Czech":
//       //   _temp = Locale('cs', 'CZ');
//       //   break;
//       // case "Denmark":
//       //   _temp = Locale('da', 'DK');
//       //   break;
//       // case "Germany":
//       //   _temp = Locale('de', 'DE');
//       //   break;
//       // case "Greek":
//       //   _temp = Locale('el', 'GR');
//       //   break;
//       case "en":
//         _temp = Locale(language.languageCode, 'US');
//         break;
//       // case "Spain":
//       //   _temp = Locale('es', 'ES');
//       //   break;
//       // case "Estonia":
//       //   _temp = Locale('et', 'EE');
//       //   break;
//       // case "Brazil":
//       //   _temp = Locale('fa', 'BR');
//       //   break;
//       // case "Brazil":
//       //   _temp = Locale('fa', 'BR');
//       //   break;
//       // case "France":
//       //   _temp = Locale('fr', 'FR');
//       //   break;
//       // case "Croatia":
//       //   _temp = Locale('hr', 'HR');
//       //   break;
//       // case "Hungary":
//       //   _temp = Locale('hu', 'HU');
//       //   break;
//       // case "Indonesia":
//       //   _temp = Locale('id', 'ID');
//       //   break;
//       // case "Israel":
//       //   _temp = Locale('he', 'IL');
//       //   break;
//       // case "Iceland":
//       //   _temp = Locale('is', 'IS');
//       //   break;
//       // case "Italy":
//       //   _temp = Locale('it', 'IT');
//       //   break;
//       // case "Japan":
//       //   _temp = Locale('ja', 'JP');
//       //   break;
//       // case "Korea":
//       //   _temp = Locale('ko', 'KO');
//       //   break;
//       // case "Lithuania":
//       //   _temp = Locale('lt', 'LT');
//       //   break;
//       // case "Latvia":
//       //   _temp = Locale('lv', 'LV');
//       //   break;
//       // case "Malaysia":
//       //   _temp = Locale('ms', 'MY');
//       //   break;
//       // case "Netherlands":
//       //   _temp = Locale('nl', 'NL');
//       //   break;
//       // case "Norway":
//       //   _temp = Locale('no', 'NO');
//       //   break;
//       // case "Poland":
//       //   _temp = Locale('pl', 'PL');
//       //   break;
//       // case "Portugal":
//       //   _temp = Locale('pt', 'PT');
//       //   break;
//       // case "Romania":
//       //   _temp = Locale('ro', 'RO');
//       //   break;
//       // case "Russia":
//       //   _temp = Locale('ru', 'RU');
//       //   break;
//       // case "Slovakia":
//       //   _temp = Locale('sk', 'SK');
//       //   break;
//       // case "Slovenia":
//       //   _temp = Locale('sl', 'SL');
//       //   break;
//       // case "Serbia":
//       //   _temp = Locale('sr', 'SP');
//       //   break;
//       // case "Sweden":
//       //   _temp = Locale('sv', 'SE');
//       //   break;
//       // case "China(T)":
//       //   _temp = Locale('te', 'TW');
//       //   break;
//       // case "Thailand":
//       //   _temp = Locale('th', 'TH');
//       //   break;
//       // case "Philippines":
//       //   _temp = Locale('tl', 'PH');
//       //   break;
//       // case "Turkey":
//       //   _temp = Locale('tr', 'TR');
//       //   break;
//       // case "Ukrania":
//       //   _temp = Locale('uk', 'UA');
//       //   break;
//       // case "Hongkong":
//       //   _temp = Locale('ur', 'HK');
//       //   break;
//       // case "Vietnam":
//       //   _temp = Locale('vi', 'VN');
//       //   break;
//       // case "China":
//       //   _temp = Locale('zh', 'CN');
//       //   break;
//       default:
//         _temp = Locale(language.languageCode, 'US');
//     }
//     MyApp.setLocale(context, _temp);
//   }
//
//   @override
//   Widget build(BuildContext context) {
//     return DropdownButton(
//       onChanged: (language) {
//         _changeLanguage(language);
//         print(language);
//
//         lang = language;
//       },
//       items: countries,
//       selectedItem: lang,
//     );
//   }
// }
