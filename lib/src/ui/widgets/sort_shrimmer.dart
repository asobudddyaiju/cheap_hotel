import 'package:flutter/material.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';

class SortShrimmer extends StatefulWidget {
  String title;
  IconData SortIcon;
  SortShrimmer({this.title,this.SortIcon});
  @override
  _SortShrimmerState createState() => _SortShrimmerState();
}

class _SortShrimmerState extends State<SortShrimmer> {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: screenHeight(context,dividedBy: 25),
      width: screenWidth(context,dividedBy: 2.2),
      decoration: BoxDecoration(
          color: Constants.kitGradients[3],
          boxShadow: [
            BoxShadow(
              offset: Offset(-0.4,-0.4),
              spreadRadius: 0,
              blurRadius: 1,
              color: Constants.kitGradients[6],
            ),

            BoxShadow(
              offset: Offset(0.4,0.4),
              spreadRadius: 0,
              blurRadius: 1,
              color: Constants.kitGradients[6],
            )
          ]
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Align(
              alignment: Alignment.center,
              child: Text(widget.title,style: TextStyle(fontSize: 10,color: Constants.kitGradients[9],fontFamily: 'Gilroy',fontWeight: FontWeight.w300),)),

          Padding(
            padding:  EdgeInsets.only(left: screenWidth(context,dividedBy: 40)),
            child: Icon(widget.SortIcon,size:30,color: Constants.kitGradients[9],),
          )
        ],
      ),


    );
  }
}
