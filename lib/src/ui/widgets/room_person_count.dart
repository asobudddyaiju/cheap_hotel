import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';

class RoomPersonCount extends StatefulWidget {
  final String counterLabel;
  final int counterValue;
  final Function incFunction;
  final Function decFunction;

  RoomPersonCount(
      {this.counterLabel,
      this.decFunction,
      this.incFunction,
      this.counterValue});

  @override
  _RoomPersonCountState createState() => _RoomPersonCountState();
}

class _RoomPersonCountState extends State<RoomPersonCount> {
  // int c = 0;

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        ListTile(
          title: Row(
            children: <Widget>[
              Container(
                width: screenWidth(context, dividedBy: 5),
                child: new Text(
                  widget.counterLabel,
                  style: TextStyle(
                      fontSize: 16,
                      color: Constants.kitGradients[2],
                      fontWeight: FontWeight.w300,
                      fontStyle: FontStyle.normal,
                      fontFamily: 'Poppins'),
                ),
              ),
              SizedBox(
                width: screenWidth(context, dividedBy: 3),
              ),
              GestureDetector(
                child: SvgPicture.asset("assets/icons/dec_counter.svg"),
                onTap: () {
                  widget.decFunction();
                  // setState(() {
                  //   c = widget.counterValue;
                  // });
                },
              ),
              SizedBox(
                width: 10,
              ),
              Container(
                width: screenWidth(context, dividedBy: 12),
                child: Center(
                  child: Text(widget.counterValue.toString(),
                      style: TextStyle(
                          color: Constants.kitGradients[2],
                          fontSize: 18,
                          fontWeight: FontWeight.w400,
                          fontStyle: FontStyle.normal,
                          fontFamily: 'Poppins')),
                ),
              ),
              SizedBox(
                width: 10,
              ),
              GestureDetector(
                  child: SvgPicture.asset("assets/icons/inc_counter.svg"),
                  onTap: () {
                    widget.incFunction();
                    // setState(() {
                    //   c = widget.counterValue;
                    // });
                  }),
            ],
          ),
        ),
      ],
    );
  }
}
