import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hotel_booking_1/src/ui/widgets/guest_bottom_sheet.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';
// ignore: must_be_immutable
class HomePageField extends StatefulWidget {
  String label;
  String hintText;
  Widget icon;
  Function onTap;
  HomePageField({
    this.label,
    this.onTap,
    this.hintText,
    this.icon
});
  @override
  _HomePageFieldState createState() => _HomePageFieldState();
}

class _HomePageFieldState extends State<HomePageField> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 15)),
      child: Container(
        height: screenHeight(context,dividedBy: 9),
        width: screenWidth(context,dividedBy: 1),
        child: Row(
          children: [
            Padding(
              padding: EdgeInsets.only(
                  top: screenHeight(context, dividedBy: 50)),
              child: Container(
                height: 60,
                width: 60,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(30),
                  color: Color(0xFFE3E4E8),
                ),
                child: Padding(
                    padding: const EdgeInsets.all(10),
                    child:widget.icon),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(
                  top: screenHeight(context, dividedBy: 40), left: 10),
              child: GestureDetector(
                onTap: (){
                  FirebaseAnalytics().logEvent(name: 'clicked_select_guests_button',parameters: null);
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                        builder: (context) => ModalBottomSheet()),
                  );
                },
                child: Container(
                  height: screenHeight(context, dividedBy: 11),
                  width: screenWidth(context, dividedBy: 1.5),
                  // color: Colors.transparent,
                  child: Column(
                    children: [
                      Padding(
                        padding:  EdgeInsets.only(top: screenWidth(context,dividedBy: 70)),
                        child: Container(
                          height: screenHeight(context, dividedBy: 40),
                          width: screenWidth(context, dividedBy: 1),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            color: Colors.transparent,
                          ),
                          child: Text(
                            widget.label,
                            style: TextStyle(
                                color: Color(0xFFABABAB),
                                fontSize: 14,
                                fontFamily: 'poppins'),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            // vertical: screenHeight(context, dividedBy: 120),
                            // horizontal: screenWidth(context,dividedBy: 100)
                        ),
                        child: Container(
                          height: screenHeight(context,dividedBy: 25),
                          width: screenWidth(context,dividedBy: 1),
                          child:  Text(
                            widget.hintText,
                             overflow: TextOverflow.visible,
                              style: TextStyle(
                              color: Constants.kitGradients[2],
                              fontSize: 18,
                              fontWeight: FontWeight.w700,
                          ),
                        ),
                      ),
                      )
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
