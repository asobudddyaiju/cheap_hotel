import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/src/ui/widgets/date_range_picker.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';

class DateField extends StatefulWidget {
  String label;
  String hintText;
  Widget icon;
  Function onTap;
  DateField({
    this.label,
    this.onTap,
    this.hintText,
    this.icon
  });
  @override
  _DateFieldState createState() => _DateFieldState();
}

class _DateFieldState extends State<DateField> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 15)),
      child:Container(
        height: screenHeight(context,dividedBy: 10),
        width: screenWidth(context,dividedBy: 1),
        child: Row(
          children: [
            Container(
              height: 60,
              width: 60,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(30),
                color: Color(0xFFE3E4E8),
              ),
              child: Padding(
                  padding: const EdgeInsets.all(10),
                  child: widget.icon),
            ),
            Padding(
              padding: EdgeInsets.only(
                left: 10,
              ),
              child: Container(
                height: screenHeight(context, dividedBy: 11),
                width: screenWidth(context, dividedBy: 1.5),
                color: Colors.transparent,
                child: Padding(
                  padding:  EdgeInsets.only(top: screenHeight(context,dividedBy: 70)),
                  child: Column(
                    children: [
                      Padding(
                        padding:  EdgeInsets.symmetric(horizontal: 0),
                        child: Container(
                          height: screenHeight(context, dividedBy: 38),
                          width: screenWidth(context, dividedBy: 1),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(25),
                            color: Colors.transparent,
                          ),
                          child: Text(
                            widget.label,
                            style: TextStyle(
                                color: Color(0xFFABABAB),
                                fontSize: 14,
                                fontFamily: 'poppins'),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(
                            vertical: 0,
                            horizontal: 0),
                        child: Container(
                          height: screenHeight(context,dividedBy: 25),
                          width: screenWidth(context,dividedBy: 1),
                          child:DateRangeField(
                            fieldVal: widget.hintText,
                            context: context,
                            // initialValue: DateTimeRange(
                            //   start: DateTime.now().add(Duration(days:Hive.box('adult').get('checkIn')!=null?Hive.box('adult').get('checkIn'): 0)),
                            //   end: DateTime.now().add(Duration(days:Hive.box('adult').get('checkOut')!=null?Hive.box('adult').get('checkOut'): 1)),
                            // ),
                          )
                          // TextFormField(


                          //   readOnly: true,
                          //   onTap:widget.onTap,
                          //   decoration: InputDecoration(
                          //       hintText: widget.hintText,
                          //       hintStyle:
                          //       TextStyle(
                          //         fontSize: screenHeight(context,dividedBy: 50),
                          //         fontFamily: 'poppins',
                          //         color: Colors.black,
                          //         height: 0,
                          //       ),
                          //       border: InputBorder.none),
                          // ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      ),

    );
  }
}
