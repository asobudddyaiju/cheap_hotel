import 'package:flutter/material.dart';

class ProfileListTile extends StatefulWidget {
  final String listText, listText1;

  ProfileListTile({this.listText, this.listText1});

  @override
  _ProfileListTileState createState() => _ProfileListTileState();
}

class _ProfileListTileState extends State<ProfileListTile> {
  @override
  Widget build(BuildContext context) {
    return ListTile(
      trailing: Text(widget.listText1,
          style: TextStyle(
            color: Color(0xff1979a9),
          )),
      title: Padding(
        padding:  EdgeInsets.only(bottom: 12),
        child: Text(
          widget.listText,
          style: TextStyle(color: Colors.black),
        ),
      ),
    );
  }
}
