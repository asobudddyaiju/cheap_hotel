import 'dart:async';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hotel_booking_1/localization/locale.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';


class AboutPage extends StatefulWidget {
  @override
  _AboutPageState createState() => _AboutPageState();
}

class _AboutPageState extends State<AboutPage> {
  statusBarColor() async {
    await FlutterStatusbarManager.setColor(Color(0xFF15548F));
  }
@override
  void initState() {
  FirebaseAnalytics().logEvent(name: 'aboutus_page',parameters: null);
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Constants.kitGradients[3],
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0.0), // here the desired height
            child: AppBar(
              backgroundColor: Constants.kitGradients[0],
              elevation: 0.0,
            )),
        body: Builder(
            builder: (context) => SafeArea(
                top: true,
                left: true,
                bottom: true,
                child: Stack(children: [
                  Container(
                    height: screenHeight(context, dividedBy: 1.04),
                    width: screenWidth(context, dividedBy: 1),
                    // decoration: BoxDecoration(
                    //   gradient: LinearGradient(
                    //       begin: Alignment.topCenter,
                    //       end: Alignment.bottomCenter,
                    //       colors: [
                    //         Constants.kitGradients[0],
                    //         Constants.kitGradients[0],
                    //         Constants.kitGradients[0].withOpacity(.99),
                    //         Constants.kitGradients[0].withOpacity(.98),
                    //         Constants.kitGradients[0].withOpacity(.97),
                    //         Constants.kitGradients[0].withOpacity(.96),
                    //         Constants.kitGradients[0].withOpacity(.95),
                    //         Constants.kitGradients[0].withOpacity(.93),
                    //         Constants.kitGradients[0].withOpacity(.90),
                    //         Constants.kitGradients[0].withOpacity(.85),
                    //         Constants.kitGradients[0].withOpacity(.84),
                    //         Constants.kitGradients[0].withOpacity(.82),
                    //         Constants.kitGradients[0].withOpacity(.80),
                    //         Constants.kitGradients[0].withOpacity(.75),
                    //         Constants.kitGradients[0].withOpacity(.73),
                    //         Constants.kitGradients[0].withOpacity(.70),
                    //         Constants.kitGradients[0].withOpacity(.67),
                    //         Constants.kitGradients[0].withOpacity(.65),
                    //         Constants.kitGradients[0].withOpacity(.63),
                    //         Constants.kitGradients[0].withOpacity(.60),
                    //         Constants.kitGradients[0].withOpacity(.57),
                    //         Constants.kitGradients[0].withOpacity(.55),
                    //         Constants.kitGradients[0].withOpacity(.53),
                    //         Constants.kitGradients[0].withOpacity(.50),
                    //         Constants.kitGradients[0].withOpacity(.47),
                    //         Constants.kitGradients[0].withOpacity(.45),
                    //         Constants.kitGradients[0].withOpacity(.44),
                    //         Constants.kitGradients[0].withOpacity(.43),
                    //         Constants.kitGradients[0].withOpacity(.40)
                    //       ]),
                    // ),
                    child: Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 5),
                        ),
                        Padding(
                          padding: EdgeInsets.only(
                              bottom: screenHeight(context, dividedBy: 100)),
                          child: Container(
                            height: 70,
                            width: 110,
                            // child: Padding(
                            //   padding: EdgeInsets.symmetric(
                            //       vertical: screenHeight(context, dividedBy: 0),
                            //       horizontal: screenWidth(context, dividedBy: 0)),
                            child: SvgPicture.asset(
                              "assets/images/app_icon_final_1.svg",
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        // ),
                        Text(
                          getTranslated(context, 'Hotels'),
                          style: TextStyle(
                              color: Constants.kitGradients[4],
                              fontSize: screenWidth(context, dividedBy: 20)),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 95),
                        ),
                        Text(
                          getTranslated(context, 'Version_125.2'),
                          style: TextStyle(
                              color: Constants.kitGradients[4],
                              fontSize: screenWidth(context, dividedBy: 55)),
                        ),
                        Text(
                          '@ 2020 hotello.com.',
                          style: TextStyle(
                              color: Constants.kitGradients[4],
                              fontSize: screenWidth(context, dividedBy: 55)),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 10),
                        ),
                        Text(
                          getTranslated(context, 'The_best_hotel_deals'),
                          style: TextStyle(
                              color: Constants.kitGradients[6],
                              fontWeight: FontWeight.w600,
                              fontSize: screenWidth(context, dividedBy: 22)),
                        ),
                        Text(
                          getTranslated(context, 'Compare_prices_from_all_the_leading_travel'),
                          style: TextStyle(
                              color: Constants.kitGradients[4],
                              fontSize: screenWidth(context, dividedBy: 32)),
                        ),
                        Text(
                          getTranslated(context, 'organcies_and_hotel_websites._Booking.com'),
                          style: TextStyle(
                              color: Constants.kitGradients[4],
                              fontSize: screenWidth(context, dividedBy: 32)),
                        ),
                        Text(
                          getTranslated(context, 'Expedea,_Agada_and_many_more-in_one_app'),
                          style: TextStyle(
                              color: Constants.kitGradients[4],
                              fontSize: screenWidth(context, dividedBy: 32)),
                        )
                      ],
                    ),
                  ),
                ]))));
  }
}
