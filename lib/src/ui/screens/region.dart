import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/src/app/app.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';

import 'home_screen.dart';

class RegionPage extends StatefulWidget {
  @override
  _RegionPageState createState() => _RegionPageState();
}

TextEditingController countryController = new TextEditingController();

class _RegionPageState extends State<RegionPage> {
  List<String> region = [];
  // Future country() async {
  //   setState(() {
  //     region = countries;
  //   });
  //   return region;
  // }

  @override
  void initState() {
    FirebaseAnalytics().logEvent(name: 'region_page',parameters: null);
    setState(() {
      region = countries;
    });
    super.initState();
  }

  String local = Hive.box('code').get(10);
  String lang = Hive.box('code').get(1);
  String country = Hive.box('locale').get(1);
  String language1 = Hive.box('locale').get(2);
  String regionUrl = Hive.box('region').get(1);
  String currency = Hive.box('code').get(2);
  List<String> countries = [
    "العربية",
    "Български",
    // "Brazil",
    "Català",
    "简体中文",
    // "China(T)",
    "Čeština",
    "Hrvatski",
    "Dansk",
    "Eesti",
    "Suomi",
    "Français",
    "Deutsch",
    "Ελληνικά",
    "Magyar",
    // "Hongkong",
    "Íslenska",
    // "India",
    "Bahasa Indonesia",
    "עברית",
    "Italiano",
    "日本語",
    "한국어",
    "Latviski",
    "Lietuvių",
    "Bahasa Malaysia",
    "Nederlands",
    "Norsk",
    "Filipino",
    "Polski",
    "Português",
    "Română",
    "Русский",
    "Srpski",
    "Slovenčina",
    "Slovenščina",
    "Español",
    "Svenska",
    "ภาษาไทย",
    "Türkçe",
    "Українська",
    "English",
    "Tiếng Việt",
  ];
  void _changeLanguage(language) async {
    Locale _temp;
    switch (language) {
      case "العربية":
        _temp = Locale('ar', 'AE');
        local = 'ar';
        lang = 'AR';
        country = 'AE';
        language1 = 'ar';
        regionUrl = 'https://www.hotelscombined.ae/';
        currency = "AED";
        break;
      case "Български":
        _temp = Locale('bg', 'BG');
        local = 'bg';
        lang = 'BG';
        country = 'BG';
        language1 = 'bg';
        currency = "BGN";
        break;
      case "Català":
        _temp = Locale('ca', 'CA');
        local = 'ca';
        lang = 'CA';
        country = 'CA';
        language1 = 'ca';
        regionUrl = 'https://www.hotelscombined.cat/';
        currency = "EUR";
        break;
      case "Čeština":
        _temp = Locale('cs', 'CZ');
        local = 'cs';
        lang = 'CZ';
        country = 'CZ';
        language1 = 'cs';
        regionUrl = 'https://www.hotelscombined.cz/';
        currency = "CZK";
        break;
      case "Dansk":
        _temp = Locale('da', 'DK');
        local = 'da';
        lang = 'DA';
        country = 'DK';
        language1 = 'da';
        regionUrl = 'https://www.hotelscombined.dk/';
        currency = "DKK";
        break;
      case "Deutsch":
        _temp = Locale('de', 'DE');
        local = 'de';
        lang = 'DE';
        country = 'DE';
        language1 = 'de';
        regionUrl = 'https://www.hotelscombined.de/';
        currency = "EUR";
        break;
      case "Ελληνικά":
        _temp = Locale('el', 'GR');
        local = 'el';
        lang = 'EL';
        country = 'GR';
        language1 = 'el';
        regionUrl = 'https://www.hotelscombined.gr/';
        currency = "EUR";
        break;
      case "English":
        _temp = Locale('en', 'US');
        local = 'en_US';
        lang = 'EN';
        country = 'US';
        language1 = 'en';
        regionUrl = 'https://www.hotelscombined.com/';
        currency = "USD";
        break;
      case "Español":
        _temp = Locale('es', 'ES');
        local = 'es';
        lang = 'ES';
        country = 'ES';
        language1 = 'es';
        regionUrl = 'https://www.hotelscombined.es/';
        currency = "EUR";
        break;
      case "Eesti":
        _temp = Locale('et', 'EE');
        local = 'et';
        lang = 'ET';
        country = 'EE';
        language1 = 'et';
        regionUrl = 'https://www.hotelscombined.ee/';
        currency = "EUR";
        break;
      // case "Brazil":
      //   _temp = Locale('fa', 'BR');
      //   local = 'pt_BR';
      //   lang = 'PB';
      //   country = 'PT';
      //   language1 = 'pt';
      //   break;
      case "Suomi":
        _temp = Locale('fi', 'FI');
        local = 'fi';
        lang = 'FI';
        country = 'FI';
        language1 = 'fi';
        regionUrl = 'https://www.hotelscombined.fi/';
        currency = "EUR";
        break;
      case "Français":
        _temp = Locale('fr', 'FR');
        local = 'fr';
        lang = 'FR';
        country = 'FR';
        language1 = 'fr';
        regionUrl = 'https://www.hotelscombined.fr/';
        currency = "EUR";
        break;
      case "Hrvatski":
        _temp = Locale('hr', 'HR');
        local = 'hr';
        lang = 'HR';
        country = 'HR';
        language1 = 'hr';
        currency = "HRK";
        break;
      case "Magyar":
        _temp = Locale('hu', 'HU');
        local = 'hu';
        lang = 'HU';
        country = 'HU';
        language1 = 'hu';
        currency = "HUF";
        break;
      case "Bahasa Indonesia":
        _temp = Locale('id', 'ID');
        local = 'id';
        lang = 'ID';
        country = 'ID';
        language1 = 'id';
        regionUrl = 'https://www.hotelscombined.co.id/';
        currency = "IDR";
        break;
      case "עברית":
        _temp = Locale('he', 'IL');
        local = 'he';
        lang = 'HE';
        country = 'IL';
        language1 = 'he';
        regionUrl = 'https://www.hotelscombined.co.il/';
        currency = "ILS";
        break;
      case "Íslenska":
        _temp = Locale('is', 'IS');
        local = 'is';
        lang = 'IS';
        country = 'IS';
        language1 = 'is';
        currency = "ISK";
        break;
      case "Italiano":
        _temp = Locale('it', 'IT');
        local = 'it';
        lang = 'IT';
        country = 'IT';
        language1 = 'it';
        regionUrl = 'https://www.hotelscombined.it/';
        currency = "EUR";
        break;
      case "日本語":
        _temp = Locale('ja', 'JP');
        local = 'ja';
        lang = 'JA';
        country = 'JP';
        language1 = 'ja';
        currency = "JPY";
        break;
      case "한국어":
        _temp = Locale('ko', 'KO');
        local = 'ko';
        lang = 'KO';
        country = 'KO';
        language1 = 'ko';
        regionUrl = 'https://www.hotelscombined.co.kr/';
        currency = "KRW";
        break;
      case "Lietuvių":
        _temp = Locale('lt', 'LT');
        local = 'lt';
        lang = 'LT';
        country = 'LT';
        language1 = 'lt';
        currency = "EUR";
        break;
      case "Latviski":
        _temp = Locale('lv', 'LV');
        local = 'lv';
        lang = 'LV';
        country = 'LV';
        language1 = 'lv';
        currency = "EUR";
        break;
      case "Bahasa Malaysia":
        _temp = Locale('ms', 'MY');
        local = 'ms';
        lang = 'MS';
        country = 'MY';
        language1 = 'ms';
        regionUrl = 'https://www.hotelscombined.my/';
        currency = "MYR";
        break;
      case "Nederlands":
        _temp = Locale('nl', 'NL');
        local = 'nl';
        lang = 'NL';
        country = 'NL';
        language1 = 'nl';
        regionUrl = 'https://www.hotelscombined.nl/';
        currency = "EUR";
        break;
      case "Norsk":
        _temp = Locale('no', 'NO');
        local = 'no';
        lang = 'NO';
        country = 'NO';
        language1 = 'no';
        regionUrl = 'https://www.hotelscombined.no/';
        currency = "NOK";
        break;
      case "Polski":
        _temp = Locale('pl', 'PL');
        local = 'pl';
        lang = 'PL';
        country = 'PL';
        language1 = 'pl';
        regionUrl = 'https://www.hotelscombined.pl/';
        currency = "PLN";
        break;
      case "Português":
        _temp = Locale('pt', 'PT');
        local = 'pt';
        lang = 'PT';
        country = 'PT';
        language1 = 'pt';
        regionUrl = 'https://www.hotelscombined.pt/';
        currency = "EUR";
        break;
      case "Română":
        _temp = Locale('ro', 'RO');
        local = 'ro';
        lang = 'RO';
        country = 'RO';
        language1 = 'ro';
        regionUrl = 'https://ro.hotelscombined.com/';
        currency = "RON";
        break;
      case "Русский":
        _temp = Locale('ru', 'RU');
        local = 'ru';
        lang = 'RU';
        country = 'RU';
        language1 = 'ru';
        regionUrl = 'https://www.roomguru.ru/';
        currency = "RUB";
        break;
      case "Slovenčina":
        _temp = Locale('sk', 'SK');
        local = 'sk';
        lang = 'SK';
        country = 'SK';
        language1 = 'sk';
        currency = "EUR";
        break;
      case "Slovenščina":
        _temp = Locale('sl', 'SL');
        local = 'sl';
        lang = 'SL';
        country = 'SL';
        language1 = 'sl';
        currency = "EUR";
        break;
      case "Srpski":
        _temp = Locale('sr', 'SP');
        local = 'sr';
        lang = 'SR';
        country = 'SP';
        language1 = 'sr';
        currency = "RSD";
        break;
      case "Svenska":
        _temp = Locale('sv', 'SE');
        local = 'sv';
        lang = 'SV';
        country = 'SE';
        language1 = 'sv';
        regionUrl = 'https://www.hotelscombined.se/';
        currency = "SEK";
        break;
      // case "China(T)":
      //   _temp = Locale('te', 'TW');
      //   local = 'zh_TW';
      //   lang = 'CN';
      //   country = 'CN';
      //   language1 = 'zh';
      //   break;
      case "ภาษาไทย":
        _temp = Locale('th', 'TH');
        local = 'th';
        lang = 'TH';
        country = 'TH';
        language1 = 'th';
        regionUrl = 'https://www.hotelscombined.co.th/';
        currency = "THB";
        break;
      case "Filipino":
        _temp = Locale('tl', 'PH');
        local = 'tl';
        lang = 'TL';
        country = 'PH';
        language1 = 'tl';
        regionUrl = 'https://www.hotelscombined.com.ph/';
        currency = "PHP";
        break;
      case "Türkçe":
        _temp = Locale('tr', 'TR');
        local = 'tr';
        lang = 'TR';
        country = 'TR';
        language1 = 'tr';
        regionUrl = 'https://www.hotelscombined.com.tr/';
        currency = "TRY";
        break;
      case "Українська":
        _temp = Locale('uk', 'UA');
        local = 'uk';
        lang = 'UK';
        country = 'UA';
        language1 = 'uk';
        currency = "UAH";
        break;
      // case "Hongkong":
      //   _temp = Locale('ur', 'HK');
      //   local = 'zh_HK';
      //   lang = 'CS';
      //   country = 'CN';
      //   language1 = 'zh';
      //   break;
      case "Tiếng Việt":
        _temp = Locale('vi', 'VN');
        local = 'vi';
        lang = 'VI';
        country = 'VN';
        language1 = 'vi';
        regionUrl = 'https://www.hotelscombined.vn/';
        currency = "VND";
        break;
      case "简体中文":
        _temp = Locale('zh', 'CN');
        local = 'zh_CN';
        lang = 'CS';
        country = 'CN';
        language1 = 'zh';
        regionUrl = 'https://hotels.biyi.cn/';
        currency = "CNY";
        break;
      // case "India":
      //   _temp = Locale('en', 'US');
      //   local = 'en_US';
      //   lang = 'EN';
      //   country = 'US';
      //   language1 = 'en';
      //   regionUrl = 'https://www.hotelscombined.com/';
      //   currency ="INR";
      //   break;
      default:
        _temp = Locale('en', 'US');
        local = 'en_US';
        lang = 'EN';
        country = 'US';
        language1 = 'en';
        regionUrl = 'https://www.hotelscombined.com/';
        currency = "USD";
    }
    Hive.box('locale').put(1, country);
    Hive.box('code').put(10, local);
    Hive.box('code').put(1, lang);
    Hive.box('locale').put(2, language1);
    Hive.box('region').put(1, regionUrl);
    // Hive.box('code').put(2, currency);
    MyApp.setLocale(context, _temp);
  }

  // void search(String value) {
  //   for (int i = 0; i <= countries.length; i++) {
  //     String data = countries[i];
  //     if (data.substring(0, value.length) == value) {
  //       print("gvh");
  //       if (region.contains(data) == false)
  //         setState(() {
  //           region.add(countries[i]);
  //         });
  //     }
  //   }
  // }

  // Widget projectWidget() {
  //   return FutureBuilder(
  //     future: country(),
  //     builder: (context, projectSnap) {
  //       if (projectSnap.connectionState == ConnectionState.none &&
  //           projectSnap.hasData == null) {
  //         //print('project snapshot data is: ${projectSnap.data}');
  //         return Container();
  //       }
  //       return Column(
  //         children: [
  //           TextField(
  //             controller: countryController,
  //             onChanged: (value) async {
  //               // String value = countryController.text.toString();
  //               projectSnap.data.clear();
  //               search(value);
  //             },
  //           ),
  //           Expanded(
  //             child: ListView.builder(
  //                 itemCount: projectSnap.data.length,
  //                 itemBuilder: (BuildContext context, int index) {
  //                   return Padding(
  //                     padding: const EdgeInsets.only(left: 10.0, right: 10.0),
  //                     child: ListTile(
  //                       title: Text(
  //                         projectSnap.data[index],
  //                         style: TextStyle(
  //                           color: Colors.black54,
  //                           fontSize: screenWidth(context, dividedBy: 25),
  //                         ),
  //                       ),
  //                       onTap: () {},
  //                     ),
  //                   );
  //                 }),
  //           ),
  //         ],
  //       );
  //     },
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[3],
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 15)),
          child: AppBar(
            title: Text(
              'Region',
              style: TextStyle(
                  color: Constants.kitGradients[17],
                  fontWeight: FontWeight.w300,
                  fontSize: 16),
            ),
            leading: new IconButton(
              onPressed: () {
                FirebaseAnalytics().logEvent(name: 'clicked_back_button',parameters: null);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                    (route) => false);
              },
              icon: new Icon(
                Icons.close,
                color: Colors.black,
              ),
            ),
            backgroundColor: Colors.white,
            elevation: 0.5,
          )),
      body: ListView.builder(
          itemCount: region.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: ListTile(
                tileColor: Colors.white,
                title: Text(
                  region[index],
                  style: TextStyle(
                    color: Constants.kitGradients[2],
                    fontSize: screenWidth(context, dividedBy: 25),
                  ),
                ),
                onTap: () {
                  FirebaseAnalytics().logEvent(name: 'region_selected',parameters: {
                    'region':region[index]
                  });
                  String language = region[index];
                  _changeLanguage(language);
                  Hive.box('lang').put(2, language);
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => HomePage()),
                      (route) => false);
                },
              ),
            );
          }),
    );
  }
}
