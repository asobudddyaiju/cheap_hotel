

import 'dart:convert';
import 'dart:io';

import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/localization/locale.dart';
// import 'dart:convert' as convert;
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/object_factory.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';
import 'home_screen.dart';
import 'package:http/http.dart' as http;

class DestinationPage extends StatefulWidget {
  @override
  _DestinationPageState createState() => _DestinationPageState();
}

class _DestinationPageState extends State<DestinationPage> {
  final List<String> items = <String>[
    'London, England, United Kingdom',
    'London, Heathrow Airport, London',
    'Londoner Hotel Gwangan, Vusan',
    'Long island, New York State'
  ];
  String defaultLang = Platform.localeName;
  List<String> destinationSuggestionList = [];
  List<String> destinationSuggestionListIcon = [];
  List<String> destinationSuggestionCountryList = [];
  List<String> destinationSuggestionHotelList = [];
  List<String> destinationSuggestionHotelLists = [];
  int i;
  Function myFunction(response){
    print("ssssssjjjjj"+response.body.toString().split("(")[2]);
    String p1=response.body.toString().split("(")[2];
    print("p1111"+p1.split(")")[0]);
    String p2=p1.split(")")[0];
    final jsonResponse = json.decode(p2);
    print("ZZZZ");
    List<String>.from(jsonResponse["Destinations"].map((x) {
      // print(x["n"]);
      // print(x["p"]);
      print(x["n"]);
      setState(() {
        destinationSuggestionList.add(x["k"].toString().substring(6,x["k"].toString().length));
        destinationSuggestionHotelLists.add(x["n"]);
        destinationSuggestionListIcon.add(x["t"]);
        destinationSuggestionHotelList.add(x["h"]);
        destinationSuggestionCountryList.add(x["p"].toString().substring(1,x["p"].toString().length-1));
      });
    }));
  }

  bool isLoading = false;
  TextEditingController destinationTextEditingController =
      new TextEditingController();
  IconData _iconData;
  final List<IconData> items2 = <IconData>[
    Icons.location_city,
    (Icons.flight_takeoff),
    (Icons.hotel),
    (Icons.flag),
    (Icons.language),
    (Icons.account_balance_outlined),
    (Icons.apartment),
    (Icons.place)
  ];

  IconData selectIcon(String iconName) {
    if (iconName == "airports")
      _iconData = items2[1];
    else if (iconName == "hotels")
      _iconData = items2[2];
     else if (iconName == "cities")
      _iconData = items2[0];
    else if(iconName == "touristregions")
      _iconData = items2[3];
    else if(iconName == "towns")
      _iconData = items2[6];
    else if(iconName == "districts")
      _iconData = items2[5];
    else if(iconName == "countries")
      _iconData = items2[4];
    else if(iconName == "touristregions")
      _iconData = items2[3];
    else
      _iconData = items2[7];
    return _iconData;
  }

  // void getDestination() {
  //   // userBlocSingle.destinationList(searchKey: destinationTextEditingController.text);
  //   setState(() {
  //     isLoading = true;
  //   });
  //   final response = ObjectFactory()
  //       .apiClient
  //       .destinationList(searchKey: destinationTextEditingController.text,lang: Hive.box('lang').get(1)!=null?Hive.box('lang').get(1):"EN",myFunction: myFunction);
  //   response.then((value) {
  //     setState(() {
  //       isLoading = false;
  //     });
  //     var jsonResponse = convert.jsonDecode(value.data);
  //     myFunction(jsonResponse);
  //
  //
  //     // print("ffff"+jsonResponse["myFunction"]);
  //
  //   });
  // }



  Future<void> getDestination() async {
    String searchKey=destinationTextEditingController.text;
    String lang = Hive.box('code').get(1) !=null ? Hive.box('code').get(1) :ObjectFactory().getLanguage.getCountryLanguage(defaultLang)[2] ;
    final response = await http
        .get("http://book.hotelmost.com/AutoUniversal.ashx?search=$searchKey&languageCode=$lang&callback=$myFunction",
      headers: {
        'Content-type' : 'application/json',
        'Accept': 'application/json'
      },
    );

    if(response == null || response.statusCode != 200) {
      myFunction(null);
    } else {
      print(response.body.runtimeType);
      String res=response.body;
      print("aaaa"+res);
      myFunction(response);
    }
  }
  @override
  void initState() {
    FirebaseAnalytics().logEvent(name: 'desination_page',parameters: null);
    // getDestination();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[3],
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 6)),
          // here the desired height
          child: AppBar(
            title: Text(
              getTranslated(context, 'Destination'),
              style: TextStyle(color: Constants.kitGradients[17],
                fontWeight: FontWeight.w300,
                fontSize: 16
              ),
            ),
            leading: new IconButton(
              onPressed: () {
                FirebaseAnalytics().logEvent(name: 'clicked_back_button',parameters: null);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                    (route) => false);
              },
              icon: new Icon(
                Icons.arrow_back_ios,
                color: Constants.kitGradients[15],
              ),
            ),
            bottom:PreferredSize(
              preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 12)),
              child:Container(
                color: Colors.white,
                height:screenHeight(context,dividedBy: 10),
                child: Padding(
                  padding:  EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 20), vertical: screenHeight(context,dividedBy: 60)),
                  child: Container(
                    height: screenHeight(context, dividedBy: 15),
                    // decoration: BoxDecoration(
                    //   color: Colors.white,
                      // borderRadius: new BorderRadius.circular(50.0),
                      // boxShadow: [
                      //   BoxShadow(
                      //       color: Colors.grey[300],
                      //       blurRadius: 1.0,
                      //       spreadRadius: 0.2)
                      // ]
                    // ),
                    child: TextField(
                      autofocus: true,
                      controller: destinationTextEditingController,
                      style: TextStyle(
                        color: Constants.kitGradients[9],
                      ),
                      onSubmitted: (_) {
                        if (destinationTextEditingController.text.length > 2) {
                          if (true) getDestination();
                        }
                      },
                      onChanged: (_) {
                        if (destinationTextEditingController.text.length > 2 ||
                            true) {
                          if (true) getDestination();
                        }
                        if (destinationTextEditingController.text.length == 0 ||
                            true) {
                          destinationSuggestionList.clear();
                          destinationSuggestionListIcon.clear();
                          destinationSuggestionCountryList.clear();
                          destinationSuggestionHotelLists.clear();
                          destinationSuggestionHotelList.clear();
                        }
                      },
                      decoration: InputDecoration(
                        //   prefixIcon: SvgPicture.asset(
                        //     "assets/images/location3_icon.svg",
                        //     height: screenHeight(context, dividedBy: 70),
                        //     width: screenWidth(context, dividedBy: 70),
                        //     fit: BoxFit.none,
                        //   ),
                        hintText: getTranslated(context, 'select_destination'),
                        hintStyle: TextStyle(fontSize: 14),
                        // suffixIcon: GestureDetector(
                        //   child: Icon(Icons.close),
                        //   onTap: () {
                        //     destinationTextEditingController.text = "";
                        //   },
                        // ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(150),
                          borderSide: BorderSide(
                            width: 0,
                            style: BorderStyle.none,
                          ),
                        ),
                        filled: true,
                        fillColor: Constants.kitGradients[16],
                        contentPadding: EdgeInsets.all(16),
                      ),
                    ),
                  ),
                ),
              ),
            ),

            backgroundColor: Colors.white,
            elevation: 0.5,
          )),
      body: Column(
        children: <Widget>[
          // Container(
          //   color: Colors.white,
          //   height:screenHeight(context,dividedBy: 10),
          //   child: Padding(
          //     padding: const EdgeInsets.only(left: 30.0, right: 30.0, top: 10.0,bottom: 10.0),
          //     child: Container(
          //       height: screenHeight(context, dividedBy: 15),
          //       decoration: BoxDecoration(
          //           color: Colors.grey,
          //           borderRadius: new BorderRadius.circular(50.0),
          //           // boxShadow: [
          //           //   BoxShadow(
          //           //       color: Colors.grey[300],
          //           //       blurRadius: 1.0,
          //           //       spreadRadius: 0.2)
          //           // ]
          //       ),
          //       child: TextField(
          //         autofocus: true,
          //         controller: destinationTextEditingController,
          //         style: TextStyle(
          //           color: Constants.kitGradients[9],
          //         ),
          //         onSubmitted: (_) {
          //           if (destinationTextEditingController.text.length > 2) {
          //             if (true) getDestination();
          //           }
          //         },
          //         onChanged: (_) {
          //           if (destinationTextEditingController.text.length > 2 ||
          //               true) {
          //             if (true) getDestination();
          //           }
          //           if (destinationTextEditingController.text.length == 0 ||
          //               true) {
          //             destinationSuggestionList.clear();
          //             destinationSuggestionListIcon.clear();
          //             destinationSuggestionCountryList.clear();
          //             destinationSuggestionHotelLists.clear();
          //           }
          //         },
          //         decoration: InputDecoration(
          //         //   prefixIcon: SvgPicture.asset(
          //         //     "assets/images/location3_icon.svg",
          //         //     height: screenHeight(context, dividedBy: 70),
          //         //     width: screenWidth(context, dividedBy: 70),
          //         //     fit: BoxFit.none,
          //         //   ),
          //           hintText: getTranslated(context, 'select_destination'),
          //           hintStyle: TextStyle(fontSize: 14),
          //           suffixIcon: GestureDetector(
          //             child: Icon(Icons.close),
          //             onTap: () {
          //               destinationTextEditingController.text = "";
          //             },
          //           ),
          //           border: OutlineInputBorder(
          //             borderRadius: BorderRadius.circular(150),
          //             borderSide: BorderSide(
          //               width: 0,
          //               style: BorderStyle.none,
          //             ),
          //           ),
          //           filled: true,
          //           fillColor: Constants.kitGradients[16],
          //           contentPadding: EdgeInsets.all(16),
          //         ),
          //       ),
          //     ),
          //   ),
          // ),
          isLoading == false
              ? destinationSuggestionList.length != 0
                  ? Expanded(
                      child: ListView.builder(
                          itemCount: destinationSuggestionList.length,
                          itemBuilder: (BuildContext context, int index) {
                            return Padding(
                              padding: const EdgeInsets.only(
                                  left: 10.0, right: 1.0),
                              child: ListTile(
                                leading: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Icon(selectIcon(
                                        destinationSuggestionListIcon[index]),color: Colors.blue,),
                                  ],
                                ),
                                title: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Flexible(
                                      flex:1,
                                      child: Text(
                                        destinationSuggestionHotelLists[index],
                                        // overflow: TextOverflow.ellipsis,
                                        style: TextStyle(
                                          color: Colors.black,
                                          fontSize:
                                              screenWidth(context, dividedBy: 30),
                                        ),
                                      ),
                                    ),
                                    SizedBox(width: 3,),
                                    destinationSuggestionHotelList[index] !=null ? Row(
                                      children: [
                                        Text(destinationSuggestionHotelList[index].split(" ").first,
                                          style: TextStyle(
                                              color:Colors.black,fontSize: screenWidth(context,dividedBy: 30),fontWeight: FontWeight.w600
                                          ),),
                                        Text(" "+destinationSuggestionHotelList[index].split(" ").last,
                                          style: TextStyle(
                                              color:Colors.black,fontSize: screenWidth(context,dividedBy: 30)
                                          ),),
                                      ],
                                    ): Container(),
                                  ],
                                ),
                                subtitle: Text(destinationSuggestionCountryList[index],
                                style: TextStyle(
                                  color: Colors.black54,
                                  fontSize: screenWidth(context,dividedBy: 30)
                                ),),
                                // trailing: Row(
                                //   children: [
                                //     Text(destinationSuggestionHotelList[index].split(" ").first,
                                //     style: TextStyle(
                                //       color:Colors.black,fontSize: screenWidth(context,dividedBy: 28),fontWeight: FontWeight.w600
                                //     ),),
                                //     Text(" "+destinationSuggestionHotelList[index].split(" ").last,
                                //       style: TextStyle(
                                //           color:Colors.black,fontSize: screenWidth(context,dividedBy: 28)
                                //       ),),
                                //   ],
                                // ),
                                onTap: () {
                                  FirebaseAnalytics().logEvent(name: 'selected_destination',parameters: {
                                    'destination':destinationSuggestionList[index]
                                  });
                                  destinationTextEditingController.text =
                                      destinationSuggestionList[index];
                                  Hive.box('lang').put(
                                      10, destinationSuggestionList[index]);
                                  Hive.box('lang').put('country', destinationSuggestionCountryList[index]);
                                  Hive.box('lang').put('place', destinationSuggestionHotelLists[index]);
                                  Navigator.pushAndRemoveUntil(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => HomePage()),
                                      (route) => false);
                                },
                              ),
                            );
                          }),
                    )
                  : Container()
              : Container(
                  height: 50,
                  child: Center(child: CircularProgressIndicator()),
                ),
          // Padding(
          //   padding: const EdgeInsets.only(left: 10.0, right: 10.0),
          //   child: Row(
          //     children: [
          //       Text(
          //         'Recent Location',
          //         style: TextStyle(
          //           fontWeight: FontWeight.w500,
          //           color: Colors.black,
          //         ),
          //       ),
          //       Spacer(),
          //       MaterialButton(
          //         color: Colors.transparent,
          //         child: Text(
          //           'Clear',
          //           style: TextStyle(
          //             color: Constants.kitGradients[0],
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
          // ),
          // Expanded(
          //   child: ListView.builder(
          //       itemCount: items.length,
          //       itemBuilder: (BuildContext context, int index) {
          //         return Padding(
          //           padding: const EdgeInsets.only(left: 10.0, right: 10.0),
          //           child: ListTile(
          //             leading: Icon(items2[index]),
          //             title: Text(
          //               items[index],
          //               style: TextStyle(
          //                 color: Colors.black54,
          //                 fontSize: screenWidth(context, dividedBy: 25),
          //               ),
          //             ),
          //           ),
          //         );
          //       }),
          // ),
          // SizedBox(
          //   height: screenHeight(context, dividedBy: 10),
          // ),
          // SizedBox(
          //   height: screenHeight(context, dividedBy: 10),
          //   child:
          //       Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          //     Text(
          //       'Sign in to access recent searches anywhere',
          //       style: TextStyle(color: Colors.black, fontSize: 10.0),
          //     ),
          //     SignButton()
          //   ]),
          // )
        ],
      ),
    );
  }
}
