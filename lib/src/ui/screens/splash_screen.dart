import 'dart:async';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:flutter_svg/svg.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';
import 'package:shimmer/shimmer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hotel_booking_1/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';

class SplashScreen extends StatefulWidget {
  @override
  _SplashScreenState createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen>
    with SingleTickerProviderStateMixin {
  Timer _timerControl;
  AnimationController animationController;
  Animation animIconHeight;
  Animation animIconWidth;
  Animation animIconColor;

  void startTimer() {
    _timerControl = Timer.periodic(const Duration(seconds: 3), (timer) {
      _timerControl.cancel();
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomePage()),
              (route) => false);
    });
  }
@override
  void initState() {
  FirebaseAnalytics().logEvent(name: 'splash_screen_page',parameters: null);
    super.initState();
  }
  @override
  void didChangeDependencies() {
    animationController =
        AnimationController(vsync: this, duration: Duration(seconds: 2));
    animIconColor = ColorTween(
        begin: Constants.kitGradients[0], end: Constants.kitGradients[3])
        .animate(CurvedAnimation(
        parent: animationController, curve: Interval(0.5, 0.6)));
    animIconHeight = Tween<double>(
      end: 40.0,
      begin: 80.0,
    ).animate(CurvedAnimation(
        parent: animationController, curve: Interval(0.5, 0.6)));
    animIconWidth = Tween<double>(end: 40.0, begin: 100.0).animate(
        CurvedAnimation(
            parent: animationController, curve: Interval(0.5, 0.6)));
    animationController.addListener(() {
      setState(() {});
    });
    animationController.forward();
    startTimer();
    statusBarColor();
    super.didChangeDependencies();
  }

  @override
  void dispose() {
    animationController.dispose();
    _timerControl.cancel();
    super.dispose();
  }
  statusBarColor() async {
    await FlutterStatusbarManager.setHidden(true);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[3],
        appBar: PreferredSize(
            preferredSize: Size.fromHeight(0.0), // here the desired height
            child: AppBar(
              backgroundColor: Constants.kitGradients[0],
              elevation: 0.0,
            )),
        body: Builder(
            builder: (context) => SafeArea(
                top: true,
                left: true,
                bottom: true,
                child: Stack(children: [
                  Container(
                    height: screenHeight(context, dividedBy: 1),
                    width: screenWidth(context, dividedBy: 1),
                    child: Column(
                      children: [
                        SizedBox(
                          height: screenHeight(context, dividedBy: 3.5),
                        ),
                        Container(
                          height: 140,
                          width: 180,
                          child: Padding(
                            padding: EdgeInsets.only(
                              top: animIconHeight.value,
                              bottom: animIconHeight.value,
                              left: animIconWidth.value,
                              right: animIconWidth.value,
                            ),
                            child: SvgPicture.asset(
                              "assets/images/app_icon_final_1.svg",
                              fit: BoxFit.fill,
                            ),
                          ),
                        ),
                        SizedBox(
                          height: screenHeight(context, dividedBy: 40),
                        ),
                        Shimmer.fromColors(
                            child: Text(
                              'cheap hotels',
                              style: TextStyle(
                                  fontFamily: 'Josefin',
                                  fontSize: 28,
                                  fontWeight: FontWeight.w300,
                                  fontStyle: FontStyle.normal),
                            ),
                            baseColor: Constants.kitGradients[2],
                            highlightColor: Constants.kitGradients[4]),
                      ],
                    ),
                  ),
                ]))));
  }
}
