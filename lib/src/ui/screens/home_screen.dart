import 'dart:io';
import 'dart:io' show Platform;
import 'package:data_connection_checker/data_connection_checker.dart';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_statusbar_manager/flutter_statusbar_manager.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/localization/locale.dart';
import 'package:hotel_booking_1/src/ui/screens/destination_page.dart';
import 'package:hotel_booking_1/src/ui/screens/search_destination.dart';
import 'package:hotel_booking_1/src/ui/screens/web_view2.dart';
import 'package:hotel_booking_1/src/ui/screens/webview.dart';
import 'package:hotel_booking_1/src/ui/widgets/calender.dart';
import 'package:hotel_booking_1/src/ui/widgets/date_field.dart';
import 'package:hotel_booking_1/src/ui/widgets/guest_bottom_sheet.dart';
import 'package:hotel_booking_1/src/ui/widgets/home_drawer.dart';
import 'package:hotel_booking_1/src/ui/widgets/home_field_destination.dart';
import 'package:hotel_booking_1/src/ui/widgets/home_page_field_box.dart';
import 'package:hotel_booking_1/src/ui/widgets/home_page_field_date.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/object_factory.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:intl/intl.dart';
import 'package:intl/date_symbol_data_local.dart';

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends State<HomePage>
    with SingleTickerProviderStateMixin {
  GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  var destination = "Select destination";
  String dates;
  String checkInDate;
  String checkOutDate;
  String checkInYear;
  String checkOutYear;
  String nights;
  String day1;
  String day2;
  int rooms = 0;
  int adults = 0;
  int children = 0;
  int counterValueAdults1=2;
  int counterValueAdults2=0;
  int counterValueAdults3=0;
  int counterValueAdults4=0;
  int counterValueAdults5=0;
  int counterValueAdults6=0;
  int counterValueAdults7=0;
  int counterValueAdults8=0;
  int counterValueAdults9=0;
  int counterValueAdultsSum=0;

  int counterValueChildren1=0;
  int counterValueChildren2=0;
  int counterValueChildren3=0;
  int counterValueChildren4=0;
  int counterValueChildren5=0;
  int counterValueChildren6=0;
  int counterValueChildren7=0;
  int counterValueChildren8=0;
  int counterValueChildren9=0;
  int counterValueChildrenSum=0;
  List<int> adultArray;
  List<int> childrenArray;
  SharedPreferences _sharedPreferences;
  final FirebaseAuth _auth = FirebaseAuth.instance;
  final FirebaseAuth _authfb = FirebaseAuth.instance;
  String monthInLetterCheckIn;
  String monthInLetterCheckOut;
  String dayInLetterCheckIn;
  String dayInLetterCheckOut;
  String checkInDay;
  String checkOutDay;
  AnimationController animationController;
  Animation animationSize;
  String adultLabel;
  String roomLabel;
  String childLabel;
  String defaultLang = Platform.localeName;
  void getSharedPref() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }
  @override
  void didChangeDependencies() {
    statusBarColor();
  }
  @override
  void initState() {
    // print("aaa"+Hive.box('code').get(6));
    // print("aaa"+Hive.box('code').get(5));
    // print("aaa"+Hive.box('lang').get('country'));
    print("aaa"+monthInLetterCheckOut.toString());

    FirebaseAnalytics().logEvent(name: 'home_page',parameters: null);
    if(Hive.box('adult').get(1)==null)
      Hive.box('adult').put(1, 1);
    counterValueAdults1=Hive.box('adult').get(21) != null?Hive.box('adult').get(21):counterValueAdults1;
    counterValueAdults2=Hive.box('adult').get(21) != null?Hive.box('adult').get(22):counterValueAdults2;
    counterValueAdults3=Hive.box('adult').get(21) != null?Hive.box('adult').get(23):counterValueAdults3;
    counterValueAdults4=Hive.box('adult').get(21) != null?Hive.box('adult').get(24):counterValueAdults4;
    counterValueAdults5=Hive.box('adult').get(21) != null?Hive.box('adult').get(25):counterValueAdults5;
    counterValueAdults6=Hive.box('adult').get(21) != null?Hive.box('adult').get(26):counterValueAdults6;
    counterValueAdults7=Hive.box('adult').get(21) != null?Hive.box('adult').get(27):counterValueAdults7;
    counterValueAdults8=Hive.box('adult').get(21) != null?Hive.box('adult').get(28):counterValueAdults8;
    counterValueAdults9=Hive.box('adult').get(21) != null?Hive.box('adult').get(29):counterValueAdults9;

    counterValueAdultsSum=counterValueAdults1+counterValueAdults2+counterValueAdults3+counterValueAdults4+counterValueAdults5+counterValueAdults6+counterValueAdults7+counterValueAdults8+counterValueAdults9;
    adultLabel=counterValueAdultsSum>1?"Adults":"Adult";

    counterValueChildren1=Hive.box('adult').get(31) != null?Hive.box('adult').get(31):counterValueChildren1;
    counterValueChildren2=Hive.box('adult').get(32) != null?Hive.box('adult').get(32):counterValueChildren2;
    counterValueChildren3=Hive.box('adult').get(33) != null?Hive.box('adult').get(33):counterValueChildren3;
    counterValueChildren4=Hive.box('adult').get(34) != null?Hive.box('adult').get(34):counterValueChildren4;
    counterValueChildren5=Hive.box('adult').get(35) != null?Hive.box('adult').get(35):counterValueChildren5;
    counterValueChildren6=Hive.box('adult').get(36) != null?Hive.box('adult').get(36):counterValueChildren6;
    counterValueChildren7=Hive.box('adult').get(37) != null?Hive.box('adult').get(37):counterValueChildren7;
    counterValueChildren8=Hive.box('adult').get(38) != null?Hive.box('adult').get(38):counterValueChildren8;
    counterValueChildren9=Hive.box('adult').get(39) != null?Hive.box('adult').get(39):counterValueChildren9;

    counterValueChildrenSum=counterValueChildren1+counterValueChildren2+counterValueChildren3+counterValueChildren4+counterValueChildren5+counterValueChildren6+counterValueChildren7+counterValueChildren8+counterValueChildren9;
    childLabel=counterValueChildrenSum>1?"Children":"child";
    Intl.defaultLocale = Hive.box('locale').get(1) !=null ? Locale(Hive.box('locale').get(2),Hive.box('locale').get(1)).toString() : defaultLang;
    var now = new DateTime.now();
    var formatter = new DateFormat('EEE, dd MMM yyyy');
    day2 = formatter.format(now.add(Duration(days: 1)));
    print(day2);
    day1 = formatter.format(now);
    print(day1);

    setState(() {
      monthInLetterCheckIn = ObjectFactory()
          .getMonthAlpha
          .getCheckInMonth(
          Hive.box('code').get(5)
      );
    });
    setState(() {
      dayInLetterCheckIn = ObjectFactory()
          .getDay
          .getWeekDay(Hive.box('date').get('weekday1'))[1];
    });
    setState(() {
      dayInLetterCheckOut = ObjectFactory()
          .getDay
          .getWeekDay(Hive.box('date').get('weekday2'))[1];
    });

    setState(() {
      monthInLetterCheckOut = ObjectFactory()
          .getMonthAlpha
          .getCheckInMonth(
          Hive.box('code').get(6)
      );
    });
      setState(() {
        checkInDate =Hive.box('code').get(3).toString() +
            " ";
      });
    setState(() {
      checkInYear =Hive.box('code').get(22).toString();
    });
      setState(() {
        checkOutDate =Hive.box('code').get(4).toString() +
            " ";
      });
    setState(() {
      checkOutYear =Hive.box('code').get(23).toString();
    });
    if (Hive.box('adult').get(1) !=null) {
      setState(() {
        rooms = Hive.box('adult').get(1);
      });
    } else {
      setState(() {
        rooms = 1;
      });
    }
    if(rooms == 1){
      setState(() {
        roomLabel = "Room";
      });
    } else{
      setState(() {
        roomLabel = "Rooms";
      });
    }
    if (Hive.box('adult').get(2) != null) {
      setState(() {
        adultArray = Hive.box('adult').get(2);
        for(int i=0;i<adultArray.length;i++){
          adults=adults + adultArray[i];
        }
      });
    } else {
      setState(() {
        adults = 0;
      });
    }
    if (Hive.box('adult').get(3) != null) {
      setState(() {
        childrenArray = Hive.box('adult').get(3);
        for(int i=0;i<childrenArray.length;i++){
          children = children + childrenArray[i];
        }
      });
    } else {
      setState(() {
        children = 0;
      });
    }
    super.initState();
  }
  checkInternetStatus(BuildContext context) async {
    bool result = await DataConnectionChecker().hasConnection;
    if(result == true) {
      if (Hive.box('lang').get(10) != null) {
        Platform.isIOS? Navigator.push(context,
            MaterialPageRoute(builder: (context) => WebViewClass())):Navigator.push(context,
            MaterialPageRoute(builder: (context) => Webview()));
      } else {
        final snackbar = SnackBar(
          content: Text(
            getTranslated(context, Constants.CHOOSE_DESTINATION),
            style: TextStyle(
                fontStyle: FontStyle.normal,
                fontFamily: 'Poppins',
                fontSize: 14.0),
          ),
          duration: Duration(seconds: 1),
          backgroundColor: Constants.kitGradients[0],
        );
        _globalKey.currentState.showSnackBar(snackbar);
      }
    } else {
      final snackbar = SnackBar(
        content: Text(
          Constants.INTERNET_CONNECTION_STATUS,
          style: TextStyle(
              fontStyle: FontStyle.normal,
              fontFamily: 'Poppins',
              fontSize: 14.0),
        ),
        duration: Duration(seconds: 1),
        backgroundColor: Constants.kitGradients[0],
      );
      _globalKey.currentState.showSnackBar(snackbar);
      print(DataConnectionChecker().lastTryResults);
    }
  }

  statusBarColor() async {
    await FlutterStatusbarManager.setHidden(false);
    await FlutterStatusbarManager.setColor(Constants.kitGradients[3],animated: true);
    await FlutterStatusbarManager.setStyle(StatusBarStyle.DARK_CONTENT);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[3],
      key: _globalKey,
      resizeToAvoidBottomPadding: false,
      drawer: HomeDrawer(auth: _auth, authfb: _authfb),
      body: Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          Padding(
            padding: EdgeInsets.only(
                top: screenHeight(context, dividedBy: 12), left: screenWidth(context,dividedBy: 13),right: screenWidth(context,dividedBy: 13),bottom: screenWidth(context,dividedBy: 12)),
            child: GestureDetector(
              child: Container(
                height: 30,
                width: 30,
                child:SvgPicture.asset("assets/images/Group.svg"),
              ),
              onTap: () {
                FirebaseAnalytics().logEvent(name: 'clicked_drawer_button',parameters: null);
                _globalKey.currentState.openDrawer();
              },
            ),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 12)),
            child: Row(
              children: [
                Text(
                  getTranslated(context, 'Search')+" ",
                  style: TextStyle(
                      color: Colors.black,
                      fontWeight: FontWeight.w300,
                      fontFamily: 'Gilroy',
                      fontSize: 30.0),
                ),
                Text(
                  getTranslated(context, 'Hotels'),
                  style: TextStyle(
                      color: Constants.kitGradients[0],
                      fontWeight: FontWeight.bold,
                      fontSize: 30.0,
                      fontFamily: 'Gilroy'),
                ),
              ],
            ),
          ),
          SizedBox(
            height: screenHeight(context,dividedBy: 130),
          ),
          Padding(
            padding: EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 12)),
            child: Container(
              height: 5,
              width: screenWidth(context, dividedBy: 9),
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                color: Constants.kitGradients[0],
              ),
            ),
          ),
          HomeFieldDestination(
            label: getTranslated(context, 'Destination'),
            svgPath: "assets/images/map-pin.svg",
            hintText: Hive.box('lang').get(10) != null
                ? Hive.box('lang').get("place")+ (Hive.box('lang').get('country').toString().trim()!=''? ", "+Hive.box('lang').get('country'):"")
                : getTranslated(context, 'select_destination'),
            onTap: () {
              FirebaseAnalytics().logEvent(name: 'clicked_destination_button',parameters: null);
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        DestinationPage()),
              );
            },
          ),
          HomePageDate(
            label: getTranslated(context, 'Check_in'),
            icon: SvgPicture.asset("assets/images/calendar.svg",color: Colors.black,),
            hintText: (Hive.box('code').get(3) !=null ? getTranslated(context,dayInLetterCheckIn)+", "+checkInDate+" "
                 +
                 getTranslated(context, monthInLetterCheckIn)
                +' '+checkInYear
                :day1
            ),
            onTap: () {
              FirebaseAnalytics().logEvent(name: 'clicked_destination_button',parameters: null);
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        DestinationPage()),
              );
            },
          ),

          Padding(
            padding: EdgeInsets.symmetric(horizontal: screenWidth(context,dividedBy: 7.5)),
            child: Container(
              height: screenHeight(context, dividedBy: 17),
              child: Column(children: [
                Container(
                  height: screenHeight(context, dividedBy: 125),
                  width: screenWidth(context, dividedBy: 75),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(25),
                    color: Color(0xFFE3E4E8),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 250)),
                  child: Container(
                    height: screenHeight(context, dividedBy: 125),
                    width: screenWidth(context, dividedBy: 75),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Color(0xFFE3E4E8),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 250)),
                  child: Container(
                    height: screenHeight(context, dividedBy: 125),
                    width: screenWidth(context, dividedBy: 75),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Color(0xFFE3E4E8),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 250)),
                  child: Container(
                    height: screenHeight(context, dividedBy: 125),
                    width: screenWidth(context, dividedBy: 75),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Color(0xFFE3E4E8),
                    ),
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(
                      top: screenHeight(context, dividedBy: 250)),
                  child: Container(
                    height: screenHeight(context, dividedBy: 125),
                    width: screenWidth(context, dividedBy: 75),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(25),
                      color: Color(0xFFE3E4E8),
                    ),
                  ),
                ),
              ]),
            ),
          ),
          HomePageDate(
            label: getTranslated(context, 'Check_out'),
            icon: SvgPicture.asset("assets/images/calendar.svg",color: Colors.black,),
            hintText: ( Hive.box('code').get(4) !=null ? getTranslated(context, dayInLetterCheckOut)+", "+checkOutDate+" "
                +getTranslated(context, monthInLetterCheckOut)
                +" "+checkOutYear
                :day2),
          ),

          HomeFieldDestination(
          label: getTranslated(context, 'Guests'),
          svgPath: "assets/images/user1_1.svg",
          hintText:rooms.toString()+" "+getTranslated(context, roomLabel)+", "+ counterValueAdultsSum.toString()+" " +getTranslated(context, adultLabel)+", " +
              counterValueChildrenSum.toString()+" "+ getTranslated(context, childLabel),
            onTap: () {
              FirebaseAnalytics().logEvent(name: 'clicked_destination_button',parameters: null);
              Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        ModalBottomSheet()),
              );
            },
        ),
          SizedBox(
            height: screenHeight(context,dividedBy: 11),
          ),
          Padding(
            padding: EdgeInsets.only(
                left: screenWidth(context,dividedBy: 13),
                right: screenWidth(context,dividedBy: 13),
               ),
            child: GestureDetector(
              onTap: () {
                FirebaseAnalytics().logEvent(name: 'clicked_destination_button',parameters: null);
                if (Hive.box('lang').get(10) != null) {
                  checkInternetStatus(context);
                } else {
                  final snackbar = SnackBar(
                    content: Text(
                      getTranslated(context, Constants.CHOOSE_DESTINATION),
                      style: TextStyle(
                          fontStyle: FontStyle.normal,
                          fontFamily: 'Poppins',
                          fontSize: 14.0),
                    ),
                    duration: Duration(seconds: 3),
                    backgroundColor: Constants.kitGradients[0],
                  );
                  _globalKey.currentState.showSnackBar(snackbar);
                }
              },
              child: Center(
                child: Container(
                    height: screenHeight(context,dividedBy: 14),
                    width: screenWidth(context,dividedBy: 1.2),
                    decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(45),
                        color: Constants.kitGradients[0]),
                    child: Center(
                        child: Text(getTranslated(context, 'Search'),
                            style: TextStyle(
                                fontSize: 22,
                                fontFamily: 'NostoSans',
                                color: Colors.white)))),
              ),
            ),
          ),
        ]),
      ),
    );
  }
}
