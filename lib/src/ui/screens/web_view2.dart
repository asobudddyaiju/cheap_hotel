import 'dart:async';
import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'dart:io';
import 'dart:io' show Platform;
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/localization/locale.dart';
import 'package:hotel_booking_1/src/ui/widgets/map_shrimmer.dart';
import 'package:hotel_booking_1/src/ui/widgets/sort_shrimmer.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/object_factory.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';
import 'package:shimmer/shimmer.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:percent_indicator/percent_indicator.dart';
import 'package:flutter_colored_progress_indicators/flutter_colored_progress_indicators.dart';
import 'package:intl/intl.dart';
import 'home_screen.dart';


const kAndroidUserAgent =
    'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36';
// ignore: prefer_collection_literals
final Set<JavascriptChannel> jsChannels = [
  JavascriptChannel(
      name: 'Print',
      onMessageReceived: (JavascriptMessage message) {
        print(message.message);
      }),
].toSet();

class WebViewClass extends StatefulWidget {
  final flutterWebViewPlugin = FlutterWebviewPlugin();

  @override
  _WebViewClassState createState() => _WebViewClassState();
}

class _WebViewClassState extends State<WebViewClass> {

  String urlWeb = "";
  String urlDummy = "";
  String changedUrl="";
  String urlRedirect="https://redirect.datahc.com/ProviderRedirect/";
  String urlRedirect2=" http://book.hotelmost.com/ProviderRedirect.ashx";
  String baseUrl = "http://book.hotelmost.com/SearchTermTypeRedirection.ashx?";
  String destinationDummy = "";
  String destination = "";
  String langCode = "";
  String currencyCode = "";
  String rooms = "";
  String checkIn = "";
  String checkOut = "";
  String day1;
  String day2;
  List<int> adultArrayDummy = [];
  String adultDummy = "";
  String adult = "adults_";
  List<String> agelist1 = [];
  List<String> agelist2 = [];
  List<String> agelist3 = [];
  List<String> agelist4 = [];
  List<String> agelist5 = [];
  List<String> agelist6 = [];
  List<String> agelist7 = [];
  List<String> agelist8 = [];
  List<String> agelist9 = [];
  String childAgeDummy = "";
  String childAgeDummy1 = "";
  String childAgeDummy2 = "";
  String childAgeDummy3 = "";
  String childAge = "childAges_";
  List<int> childrenCount = [];
  double progress = 0;
  int counterValueAdults1=2;
  int counterValueAdults2=0;
  int counterValueAdults3=0;
  int counterValueAdults4=0;
  int counterValueAdults5=0;
  int counterValueAdults6=0;
  int counterValueAdults7=0;
  int counterValueAdults8=0;
  int counterValueAdults9=0;
  int counterValueChildren1=0;
  int counterValueChildren2=0;
  int counterValueChildren3=0;
  int counterValueChildren4=0;
  int counterValueChildren5=0;
  int counterValueChildren6=0;
  int counterValueChildren7=0;
  int counterValueChildren8=0;
  int counterValueChildren9=0;
  List<int> counterValueAdultsTotal=[];
  List<int> counterValueChildrenTotal=[];
  String defaultLang = Platform.localeName;
  StreamSubscription<double> _onProgressChanged;
  StreamSubscription<String> _onUrlChanged;
  final _history = [];
  final key = UniqueKey();
  int position;



  @override
  void initState() {
    FirebaseAnalytics().logEvent(name: 'webview_page',parameters: null);
    Intl.defaultLocale = 'en_US';
    var now = new DateTime.now();
    var formatter = new DateFormat('yyyy-MM-dd');
    day2 = formatter.format(now.add(Duration(days: 1)));
    print(day2);
    day1 = formatter.format(now);
    print(day1);
    print("aaaa" + Hive.box('adult').get(11).toString());
    setState(() {
      destinationDummy = Hive.box('lang').get(10) != null
          ? Hive.box('lang').get(10).toString().trim()
          : "";
      destination = destinationDummy.replaceAll(' ', '_');
      langCode = Hive.box('code').get(1) != null
          ? Hive.box('code').get(1).toString().trim()
          : ObjectFactory().getLanguage.getCountryLanguage(defaultLang)[2].toUpperCase();
      currencyCode = Hive.box('code').get(2) != null
          ? Hive.box('code').get(2).toString().substring(0,3)
          : Hive.box('room').get('country')!= null?ObjectFactory().getCurrency.getCurrency(Hive.box('room').get('currency')):"USD";
      rooms = Hive.box('adult').get(1) != null
          ? Hive.box('adult').get(1).toString().trim()
          : "2";
      checkIn = Hive.box('lang').get(5) as String != null ? Hive.box('lang').get(5) as String : day1;
      checkOut = Hive.box('lang').get(6) as String != null ? Hive.box('lang').get(6) as String : day2;
      //adultArrayDummy = Hive.box('adult').get(2) != null
      //   ? Hive.box('adult').get(2)
      //   : [2, 0, 0, 0, 0, 0, 0, 0, 0];

      counterValueAdults1=Hive.box('adult').get(21) != null?Hive.box('adult').get(21):counterValueAdults1;
      counterValueAdults2=Hive.box('adult').get(22) != null?Hive.box('adult').get(22):counterValueAdults2;
      counterValueAdults3=Hive.box('adult').get(23) != null?Hive.box('adult').get(23):counterValueAdults3;
      counterValueAdults4=Hive.box('adult').get(24) != null?Hive.box('adult').get(24):counterValueAdults4;
      counterValueAdults5=Hive.box('adult').get(25) != null?Hive.box('adult').get(25):counterValueAdults5;
      counterValueAdults6=Hive.box('adult').get(26) != null?Hive.box('adult').get(26):counterValueAdults6;
      counterValueAdults7=Hive.box('adult').get(27) != null?Hive.box('adult').get(27):counterValueAdults7;
      counterValueAdults8=Hive.box('adult').get(28) != null?Hive.box('adult').get(28):counterValueAdults8;
      counterValueAdults9=Hive.box('adult').get(29) != null?Hive.box('adult').get(29):counterValueAdults9;

      adultArrayDummy=[counterValueAdults1,counterValueAdults2,counterValueAdults3,counterValueAdults4,counterValueAdults5,counterValueAdults6,counterValueAdults7,counterValueAdults8,counterValueAdults9];

      agelist1 =
      Hive.box('adult').get(11) != null ? Hive.box('adult').get(11) : [];
      agelist2 =
      Hive.box('adult').get(12) != null ? Hive.box('adult').get(12) : [];
      agelist3 =
      Hive.box('adult').get(13) != null ? Hive.box('adult').get(13) : [];
      agelist4 =
      Hive.box('adult').get(14) != null ? Hive.box('adult').get(14) : [];
      agelist5 =
      Hive.box('adult').get(15) != null ? Hive.box('adult').get(15) : [];
      agelist6 =
      Hive.box('adult').get(16) != null ? Hive.box('adult').get(16) : [];
      agelist7 =
      Hive.box('adult').get(17) != null ? Hive.box('adult').get(17) : [];
      agelist8 =
      Hive.box('adult').get(18) != null ? Hive.box('adult').get(18) : [];
      agelist9 =
      Hive.box('adult').get(19) != null ? Hive.box('adult').get(19) : [];

      // childrenCount = Hive.box('adult').get(3);

      counterValueChildren1=Hive.box('adult').get(31) != null?Hive.box('adult').get(31):counterValueChildren1;
      counterValueChildren2=Hive.box('adult').get(32) != null?Hive.box('adult').get(32):counterValueChildren2;
      counterValueChildren3=Hive.box('adult').get(33) != null?Hive.box('adult').get(33):counterValueChildren3;
      counterValueChildren4=Hive.box('adult').get(34) != null?Hive.box('adult').get(34):counterValueChildren4;
      counterValueChildren5=Hive.box('adult').get(35) != null?Hive.box('adult').get(35):counterValueChildren5;
      counterValueChildren6=Hive.box('adult').get(36) != null?Hive.box('adult').get(36):counterValueChildren6;
      counterValueChildren7=Hive.box('adult').get(37) != null?Hive.box('adult').get(37):counterValueChildren7;
      counterValueChildren8=Hive.box('adult').get(38) != null?Hive.box('adult').get(38):counterValueChildren8;
      counterValueChildren9=Hive.box('adult').get(39) != null?Hive.box('adult').get(39):counterValueChildren9;

      childrenCount=[counterValueChildren1,counterValueChildren2,counterValueChildren3,counterValueChildren4,counterValueChildren5,counterValueChildren6,counterValueChildren7,counterValueChildren8,counterValueChildren9];

      for (int i = 0; i < Hive.box('adult').get(1); i++) {
        // print(adult + (i + 1).toString() + "=" + adultArrayDummy[i].toString());
        adultDummy = adultDummy.trim() +
            adult +
            (i + 1).toString() +
            "=" +
            adultArrayDummy[i].toString().trim() +
            "&";
      }
      if (agelist1 != null) {
        for (int x = 0; x < agelist1.length; x++) {
          childAgeDummy1 = childAgeDummy1.trim() +
              childAge +
              "1=" +
              agelist1[x].replaceAll(' Years old', '&');
        }
      } else {
        childAgeDummy1 = "";
      }

      if (agelist2 != null) {
        for (int y = 0; y < agelist2.length; y++) {
          childAgeDummy2 = childAgeDummy2.trim() +
              childAge +
              "2=" +
              agelist2[y].replaceAll(' Years old', '&');
        }
      } else {
        childAgeDummy2 = "";
      }

      if (agelist3 != null) {
        for (int z = 0; z < agelist3.length; z++) {
          childAgeDummy3 = childAgeDummy3.trim() +
              childAge +
              "3=" +
              agelist3[z].replaceAll(' Years old', '&');
        }
      } else {
        childAgeDummy3 = "";
      }

      childAgeDummy = childAgeDummy1 + childAgeDummy2 + childAgeDummy3;
      urlWeb = baseUrl +
          "destination=place%3A$destination" +
          "&radius=0km" +
          "&checkin=$checkIn" +
          "&checkout=$checkOut" +
          "&$adultDummy" +
          "$childAgeDummy" +
          "Rooms=$rooms" +
          "&pageSize=15&pageIndex=0&sort=Popularity-desc&showSoldOut=false&HotelID=&mapState=expanded%3D0" +
          "&languageCode=$langCode" +
          "&currencyCode=$currencyCode"
      ;

      urlDummy="http://book.hotelmost.com/Hotels/Search?"+
          "destination=place%3A$destination" +
          "&checkin=$checkIn" +
          "&checkout=$checkOut" +
          "&$adultDummy" +
          "$childAgeDummy" +
          "Rooms=$rooms" +
          "&pageSize=15&pageIndex=0&sort=Popularity-desc&showSoldOut=false&HotelID=&mapState=expanded%3D0" +
          "&languageCode=$langCode" +
          "&currencyCode=$currencyCode";
      print("url is $urlWeb");
    });

    _onProgressChanged =
        widget.flutterWebViewPlugin.onProgressChanged.listen((double progress) {

          if (mounted) {
            setState(() {
              _history.add('onProgressChanged: $progress');
            });
          }

          print("progress percent is "+progress.toString());
        });

    _onUrlChanged = widget.flutterWebViewPlugin.onUrlChanged.listen((String url) {
      setState(() {
        changedUrl=url;
      });
      FirebaseAnalytics().logEvent(name: 'webview_url',parameters: {
        'webview_url':url.substring(0,25)
      });
      if (mounted) {
        setState(() {
          _history.add('onUrlChanged: $url');
        });
      }
      print("current url is "+url);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[3],
      body: WebviewScaffold(
        url:urlWeb,
        javascriptChannels: jsChannels,
        mediaPlaybackRequiresUserGesture: false,
        withZoom: true,
        withLocalStorage: true,
        hidden: true,
        initialChild: Padding(
          padding: EdgeInsets.only(top: screenHeight(context, dividedBy: 900)),
          child: Container(
              height: screenHeight(context, dividedBy: 1),
              width: screenWidth(context, dividedBy: 1),
              child: Column(
                children: [
                  Container(
                    height: screenHeight(context, dividedBy: 10),
                    width: screenWidth(context, dividedBy: 1),
                    color: Colors.grey[200],
                    child: Padding(
                      padding: EdgeInsets.only(
                          top: screenHeight(context, dividedBy: 20)),
                      child: Column(
                        children: [
                          Container(
                            height: screenHeight(context, dividedBy: 25),
                            width: screenWidth(context, dividedBy: 1.8),
                            color: Colors.grey[300],
                          ),
                        ],
                      ),
                    ),
                  ),
                  Column(
                    children: [
                      Padding(
                        padding: EdgeInsets.only(
                            top: screenHeight(context, dividedBy: 100)),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            MapShrimmer(title: getTranslated(context, 'map'),),
                            MapShrimmer(title: getTranslated(context, 'filter'),),
                          ],
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(
                            top: screenHeight(context, dividedBy: 100)),
                        child: Divider(
                          thickness: 3,
                          color: Colors.grey[100],
                        ),
                      ),

                      Padding(
                        padding: EdgeInsets.only(
                            top: screenHeight(context, dividedBy: 80,),
                            left: screenWidth(context, dividedBy: 2.05)),
                        child: SortShrimmer(
                            title: getTranslated(context, 'Sort_by_Popularity'),
                            SortIcon: Icons.keyboard_arrow_down_rounded),
                      )


                    ],
                  ),

                  Expanded(
                    child: Shimmer.fromColors(
                      baseColor: Colors.grey[300],
                      highlightColor: Colors.grey[100],
                      enabled: true,
                      child: Padding(
                        padding: EdgeInsets.only(
                          top: screenHeight(context, dividedBy: 20),),
                        child: ListView.builder(
                          itemBuilder: (_, __) =>
                              Padding(
                                padding: const EdgeInsets.only(bottom: 8.0),
                                child: Padding(
                                  padding: EdgeInsets.only(
                                      left: screenWidth(context, dividedBy: 40)),
                                  child: Row(
                                    crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                        width: screenWidth(context,
                                            dividedBy: 3.5),
                                        height: screenHeight(context,
                                            dividedBy: 6),
                                        color: Colors.white,
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.symmetric(
                                            horizontal: 8.0),
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                          children: <Widget>[
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  top: screenHeight(
                                                      context, dividedBy: 60)),
                                              child: Container(
                                                width: screenWidth(
                                                    context, dividedBy: 1.8),
                                                height: screenHeight(
                                                    context, dividedBy: 30),
                                                color: Colors.white,
                                              ),
                                            ),
                                            const Padding(
                                              padding: EdgeInsets.symmetric(
                                                  vertical: 2.0),
                                            ),
                                            Container(
                                              width: screenWidth(
                                                  context, dividedBy: 1.8),
                                              height: screenHeight(
                                                  context, dividedBy: 30),
                                              color: Colors.white,
                                            ),
                                            Padding(
                                              padding: EdgeInsets.only(
                                                  top: screenHeight(
                                                      context, dividedBy: 40),
                                                  left: screenWidth(
                                                      context, dividedBy: 3.8)),

                                              child: Container(
                                                width: screenWidth(
                                                    context, dividedBy: 3.5),
                                                height: screenHeight(
                                                    context, dividedBy: 30),
                                                color: Colors.white,
                                              ),
                                            ),
                                          ],
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              ),
                          itemCount: 8,
                        ),
                      ),
                    ),
                  ),
                ],
              )),
        ),

        //appBar: AppBar(backgroundColor: Constants.kitGradients[0],toolbarHeight: 35,),
        bottomNavigationBar: BottomAppBar(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              IconButton(
                icon: const Icon(Icons.arrow_back_ios),
                onPressed: () async{
                  FirebaseAnalytics().logEvent(name: 'clicked_webview_back',parameters: null);
                  var status=await widget.flutterWebViewPlugin.canGoBack();
                  if(status){
                    widget.flutterWebViewPlugin.goBack();
                  }else{
                    Navigator.pushAndRemoveUntil(context, MaterialPageRoute(builder: (context)=>HomePage()), (route) => false);
                  }
                },
              ),
              IconButton(
                icon: const Icon(Icons.autorenew),
                onPressed: () {
                  FirebaseAnalytics().logEvent(name: 'clicked_webview_refresh',parameters: null);
                  widget.flutterWebViewPlugin.reload();
                },
              ),
              IconButton(
                icon: const Icon(Icons.arrow_forward_ios),
                onPressed: () {
                  FirebaseAnalytics().logEvent(name: 'clicked_webview_forward',parameters: null);
                  widget.flutterWebViewPlugin.goForward();
                },
              ),
            ],
          ),
        ),

      ),
    );

  }
}


