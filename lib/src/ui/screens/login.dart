import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:hotel_booking_1/localization/locale.dart';
import 'package:hotel_booking_1/src/auth_service/firebase_auth.dart';
import 'package:hotel_booking_1/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';

class Login extends StatefulWidget {
  final FirebaseAuth auth, authFb;

  const Login({this.auth, this.authFb});

  @override
  State<StatefulWidget> createState() {
    return _LoginState();
  }
}

class _LoginState extends State<Login> {
  bool _isLoading;
  final GlobalKey<ScaffoldState> _globalKey = GlobalKey<ScaffoldState>();
  @override
  void initState() {
    FirebaseAnalytics().logEvent(name: 'login_page',parameters: null);
    _isLoading = false;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(statusBarColor: Constants.kitGradients[0]),
      child: Scaffold(
        backgroundColor: Constants.kitGradients[3],
        resizeToAvoidBottomPadding: false,
        appBar: PreferredSize(
            preferredSize:
                Size.fromHeight(screenHeight(context, dividedBy: 15)),
            // here the desired height
            child: AppBar(
              leading: new IconButton(
                onPressed: () {
                  FirebaseAnalytics().logEvent(name: 'clicked_back_button',parameters: null);
                  Navigator.pop(context);
                },
                icon: new Icon(
                  Icons.close,
                  color: Colors.black,
                ),
              ),
              backgroundColor: Colors.white,
              elevation: 0.5,
            )),
        key: _globalKey,
        body: SafeArea(
          child: Center(
            child: Stack(
              children: [
                Column(
                  children: [
                    Center(
                      child: Container(
                        width: screenWidth(context, dividedBy: 1.1),
                        height: screenHeight(context, dividedBy: 2.3),
                        // decoration: BoxDecoration(
                        //   gradient: LinearGradient(
                        //       colors: [Color(0xFF15548F), Colors.white],
                        //       begin: Alignment.topCenter,
                        //       end: Alignment.bottomCenter),
                        // ),
                        child: SvgPicture.asset(
                          "assets/images/hotellocation.svg",
                          fit: BoxFit.fill,
                        ),
                      ),
                    ),
                    SizedBox(height: screenHeight(context,dividedBy:15),),
                    Center(
                      child: Text(
                        getTranslated(context, 'Sign_In_and_Save'),
                        style: TextStyle(
                            fontFamily: 'NexaBold',
                            fontWeight: FontWeight.w700,
                            color: Color(0xFF484848),
                            fontSize: 18.0,
                            fontStyle: FontStyle.normal),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(
                          top: 10.0, bottom: 10.0, left: 20.0, right: 20.0),
                      child: Center(
                        child: Text(
                          getTranslated(context, 'Track_prices,_organize_travel_plans_and_access_member-only_with_your_HotelBooking_account'),
                          style: TextStyle(
                              fontFamily: 'NexaLight',
                              fontWeight: FontWeight.w300,
                              color: Color(0xFF7E7E7E),
                              fontSize: 10.0,
                              fontStyle: FontStyle.normal),
                        ),
                      ),
                    ),
                    Padding(
                      padding:
                          EdgeInsets.only(left: 35.0, right: 35.0, top: 20.0),
                      child: Center(
                        child: GestureDetector(
                          onTap: () async {
                            FirebaseAnalytics().logEvent(name: 'clicked_google_signin',parameters: null);
                            googleSignin();
                          },
                          child: Container(
                            width: MediaQuery.of(context).size.width,
                            height: 60.0,
                            decoration: BoxDecoration(
                                border: Border.all(style: BorderStyle.none),
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(30.0),
                                boxShadow: [
                                  BoxShadow(
                                      color: Color(0xFFDBE7EB).withOpacity(0.2),
                                      offset: Offset(0, 5),
                                      blurRadius: 1.0,
                                      spreadRadius: 1.0)
                                ]),
                            child: Center(
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: [
                                  Image.asset('assets/images/google.png'),
                                  Padding(
                                    padding: EdgeInsets.only(left: 10.0),
                                    child: Text(
                                      getTranslated(context, 'Continue_with_Google'),
                                      style: TextStyle(
                                          fontFamily: 'NexaLight',
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.w300,
                                          fontStyle: FontStyle.normal,
                                          color: Color(0xFF484848)),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                    // Padding(
                    //   padding:
                    //       EdgeInsets.only(left: 35.0, right: 35.0, top: 20.0),
                    //   child: Center(
                    //     child: GestureDetector(
                    //       onTap: () async {
                    //   FirebaseAnalytics().logEvent(name: 'clicked_facebook_signin',parameters: null);
                    //         facebookSignin();
                    //       },
                    //       child: Container(
                    //         width: MediaQuery.of(context).size.width,
                    //         height: 60.0,
                    //         decoration: BoxDecoration(
                    //             border: Border.all(style: BorderStyle.none),
                    //             color: Colors.white,
                    //             borderRadius: BorderRadius.circular(30.0),
                    //             boxShadow: [
                    //               BoxShadow(
                    //                   color: Color(0xFFDBE7EB).withOpacity(0.2),
                    //                   offset: Offset(0, 5),
                    //                   spreadRadius: 1.0,
                    //                   blurRadius: 1.0)
                    //             ]),
                    //         child: Center(
                    //           child: Row(
                    //             mainAxisAlignment: MainAxisAlignment.center,
                    //             children: [
                    //               Image.asset('assets/images/Facebook.png',
                    //                   width: 20.0, height: 20.0),
                    //               Padding(
                    //                 padding: EdgeInsets.only(left: 10.0),
                    //                 child: Text(
                    //                   getTranslated(context, 'Continue_with_Facebook'),
                    //                   style: TextStyle(
                    //                       fontFamily: 'NexaLight',
                    //                       fontSize: 16.0,
                    //                       fontWeight: FontWeight.w300,
                    //                       fontStyle: FontStyle.normal,
                    //                       color: Color(0xFF484848)),
                    //                 ),
                    //               ),
                    //             ],
                    //           ),
                    //         ),
                    //       ),
                    //     ),
                    //   ),
                    // ),
                  ],
                ),
                Positioned(
                  bottom: 10,
                  child: SafeArea(
                    child: Center(
                      child: Padding(
                        padding: EdgeInsets.only(
                            bottom: 15, left: 15.0, right: 15.0),
                        child: RichText(
                          text: TextSpan(
                              text: getTranslated(context, 'By_signing_up_you_agree_to_our'),
                              style: TextStyle(
                                fontFamily: 'NexaLight',
                                fontWeight: FontWeight.w300,
                                color: Color(0xFF404040),
                                fontSize: 10.0,
                              ),
                              children: <TextSpan>[
                                TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () => print(
                                          'you just clicked terms and conditions'),
                                    text: ' Terms & Conditions',
                                    style: TextStyle(
                                        fontFamily: 'NexaLight',
                                        fontWeight: FontWeight.w300,
                                        color: Colors.blue,
                                        fontSize: 10.0)),
                                TextSpan(
                                    text: ' '+ getTranslated(context,'And'),
                                    style: TextStyle(
                                        fontFamily: 'NexaLight',
                                        fontWeight: FontWeight.w300,
                                        fontSize: 10.0,
                                        color: Color(0xFF404040))),
                                TextSpan(
                                    recognizer: TapGestureRecognizer()
                                      ..onTap = () => print(
                                          'you just clicked Privacy Policy'),
                                    text: ' '+getTranslated(context, 'Privacy_policy'),
                                    style: TextStyle(
                                        fontFamily: 'NexaLight',
                                        fontWeight: FontWeight.w300,
                                        color: Colors.blue,
                                        fontSize: 10.0))
                              ]),
                        ),
                      ),
                    ),
                  ),
                ),
                _isLoading == true
                    ? Center(
                        child: Container(
                            height: 40.0,
                            width: 40.0,
                            child: Center(child: CircularProgressIndicator())),
                      )
                    : Stack()
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _showSnackbar(String snackdata) {
    final snack = SnackBar(
      content: Text(snackdata,style: TextStyle(fontFamily: "Poppins",fontStyle: FontStyle.normal,fontSize: 12.0),),
      duration: Duration(seconds: 2),
      backgroundColor: Constants.kitGradients[0],
    );
    _globalKey.currentState.showSnackBar(snack);
  }

  void googleSignin() async {
    setState(() {
      _isLoading = true;
    });
    String googlesigninstatus =
        await Auth(auth: widget.auth, authfb: widget.authFb).signInWithGoogle();
    if (googlesigninstatus == "Success") {
      FirebaseAnalytics().logEvent(name: 'googlesignin_success',parameters: null);
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomePage()),
          (route) => false);
    } else {
      FirebaseAnalytics().logEvent(name: 'google_signin_error',parameters: {
        'error_status':googlesigninstatus.substring(0,25)
      });
      setState(() {
        _isLoading = false;
      });
      _showSnackbar(googlesigninstatus.trim());
    }
  }

  void facebookSignin() async {
    setState(() {
      _isLoading = true;
    });
    String status = await Auth(auth: widget.auth, authfb: widget.authFb)
        .signInWithFacebook();
    if (status == "Success") {
      FirebaseAnalytics().logEvent(name: 'facebook_signin_success',parameters: null);
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomePage()),
          (route) => false);
    } else {
      if(
      status.trim()=='An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address.')
      {
        FirebaseAnalytics().logEvent(name: 'link_fb_google_auth',parameters: null);
        LinkGoogleAuthAndFacebookAuth();
      }else{
        FirebaseAnalytics().logEvent(name: 'link_fb_google_error',parameters: {
          'error_status':status.substring(0,25)
        });
        _showSnackbar(status.trim());
        print(status);
        setState(() {
          _isLoading=false;
        });
      }
    }
  }

  void LinkGoogleAuthAndFacebookAuth() async {
    setState(() {
      _isLoading = true;
    });
    String status = await Auth(auth: widget.auth, authfb: widget.authFb)
        .linkGoogleAndFacebook();
    if (status == "Success") {
      FirebaseAnalytics().logEvent(name: 'link_fb_google_success',parameters: null);
      Navigator.pushAndRemoveUntil(
          context,
          MaterialPageRoute(builder: (context) => HomePage()),
          (route) => false);
    } else {
      FirebaseAnalytics().logEvent(name: 'link_fb_google_error',parameters: {
        'error_status':status.substring(0,25)
      });
      _showSnackbar(status.trim());
      print(status);
      setState(() {
        _isLoading = false;
      });
    }
  }
}
