import 'package:firebase_analytics/firebase_analytics.dart';
import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/localization/locale.dart';
import 'package:hotel_booking_1/src/ui/screens/home_screen.dart';
import 'package:hotel_booking_1/src/utils/constants.dart';
import 'package:hotel_booking_1/src/utils/utils.dart';


class CurrencyPage extends StatefulWidget {
  @override
  _CurrencyPageState createState() => _CurrencyPageState();
}

class _CurrencyPageState extends State<CurrencyPage> {
  List<String> currency = [];
  String value;
  @override
  void initState() {
    FirebaseAnalytics().logEvent(name: 'currency_page',parameters: null);
    setState(() {
      currency = currencies;
    });
    setState(() {
      value = Hive.box('code').get(2);
    });
    super.initState();
  }

  List<String> currencies = [
    "AED United Arab Emirates Dirham (AED)",
    "ALL Albanian Lek (ALL)",
    "AMD Armenian Dram (AMD)",
    "ARS Argentine Peso (ARS)",
    "AUD Australian Dollar (A\u0024)",
    "AZN Azerbaijani Manat (AZN)",
    "BAM Bosnia-Herzegovina Convertible Mark (BAM)",
    "BDT Bangladeshi Taka (BDT)",
    "BHD Bahraini Dinar (BHD)",
    "BGN Bulgarian Lev (BGN)",
    "BRL Brazil Real (R\u0024)",
    "CAD Canadian Dollar (C\u0024)",
    "CLP Chilean Peso (CLP)",
    "COP Colombian Peso (COP)",
    "CHF Swiss Francs (CHF)",
    "CNY Chinese yuan (CN\u00a5)",
    "CZK Czech koruna (CZK)",
    "HRK Croatian Kuna (HRK)",
    "DKK Danish Krone (DKK)",
    "DZD Algerian Dinar (DZD)",
    "EGP Egyptian Pound (EGP)",
    "GBP British Pound (£)",
    "GEL Georgian Lari (GEL)",
    "GHS Ghanaian Cedi (GHS)",
    "HTG Haitian Gourde (HTG)",
    "EUR Euro (\u20AC)",
    "HUF Hungarian Forint (HUF)",
    "HKD Hong Kong Dollar (HK\u0024)",
    "IQD Iraqi Dinar (IQD)",
    "IRR Iranian Rial (IRR)",
    "JOD Jordanian Dinar (JOD)",
    "ISK Iceland Krona (ISK)",
    "INR Indian Rupee (\u20B9)",
    "IDR Indonesian Rupiah (IDR)",
    "ILS Israeli New Shekel (₪)",
    "JPY Japanese Yen (\u00a5)",
    "KES Kenyan Shilling (KES)",
    "KGS Kyrgystani Som (KGS)",
    "KWD Kuwaiti Dinar (KWD)",
    "KZT Kazakhstani Tenge (KZT)",
    "LKR Sri Lankan Rupee (LKR)",
    "LYD Libyan Dinar (LYD)",
    "MNT Mongolian Tugrik (MNT)",
    "MUR Mauritian Rupee (MUR)",
    "MXN Mexican Peso (MX\u0024)",
    "KRW South Korean Won (\u20a9)",
    "LTL Lita (LTL)",
    "MYR Malaysian Ringgit (MYR)",
    "MZN Mozambican Metical (MZN)",
    "NGN Nigerian Naira (NGN)",
    "NOK Norwegian Krone (NOK)",
    "NPR Nepalese Rupee (NPR)",
    "NZD New Zealand Dollar (NZ\u0024)",
    "OMR Omani Rial (OMR)",
    "PEN Peruvian Sol (PEN)",
    "PHP Philippine Piso (PHP)",
    "PKR Pakistani Rupee (PKR)",
    "PLN Polish Zloty (PLN)",
    "QAR Qatari Rial (QAR)",
    "RON Romanian New Lei (RON)",
    "RUB Russian Rouble (RUB)",
    "RSD Serbian Dinar (RSD)",
    "SAR Saudi riyal (SAR)",
    "SGD Singapore Dollar (SGD)",
    "SEK Swedish Krona (SEK)",
    "TJS Tajikistani Somoni (TJS)",
    "TND Tunisian Dinar (TND)",
    "THB Thai Baht (THB)",
    "TRY Turkish Lira (TL)",
    "TWD Taiwan dollar (NT\u0024)",
    "UAH Ukrainian Hryvnia (UAH)",
    "USD U.S. Dollar (\u0024)",
    "UZS Uzbekistani Som (UZS)",
    "VND Vietnamese Dong (₫)",
    "XOF West African CFA Franc (CFA)",
    "ZAR South African Rand (ZAR)",
    "ZMW Zambian Kwacha (ZMW)"
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Constants.kitGradients[3],
      appBar: PreferredSize(
          preferredSize: Size.fromHeight(screenHeight(context, dividedBy: 15)),
          child: AppBar(
            title: Text(
              getTranslated(context, 'Currency'),
              style: TextStyle(
                  color: Constants.kitGradients[17],
                  fontWeight: FontWeight.w300,
                  fontSize: 16),
            ),
            leading: new IconButton(
              onPressed: () {
                FirebaseAnalytics().logEvent(name: 'clicked_back_button',parameters: null);
                Navigator.pushAndRemoveUntil(
                    context,
                    MaterialPageRoute(builder: (context) => HomePage()),
                    (route) => false);
              },
              icon: new Icon(
                Icons.close,
                color: Colors.black,
              ),
            ),
            backgroundColor: Colors.white,
            elevation: 0.5,
          )),
      body: ListView.builder(
          itemCount: currency.length,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
              padding: const EdgeInsets.only(left: 10.0, right: 10.0),
              child: ListTile(
                tileColor: Colors.white,
                title: Row(
                  children: [
                    Container(
                      width: screenWidth(context,dividedBy: 11),
                      margin: const EdgeInsets.all(5),
                      padding: const EdgeInsets.all(0),
                      decoration: BoxDecoration(
                          border: Border.all(color: Constants.kitGradients[0])),
                      child: Center(
                        child: Text(
                          currency[index].split(' ').first,
                          style: TextStyle(
                            color: Constants.kitGradients[0],
                            fontSize: screenWidth(context, dividedBy: 30),
                          ),
                        ),
                      ),
                    ),
                    Container(

                      child: Flexible(
                        flex: 1,
                        child: Text(
                          currency[index].substring(4,currency[index].length),
                          overflow: TextOverflow.visible,
                          style: TextStyle(
                            color: Constants.kitGradients[2],
                            fontSize: screenWidth(context, dividedBy: 30),
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
                onTap: () {
                  FirebaseAnalytics().logEvent(name: 'currency_selected',parameters: {
                    'currency':currency[index]
                  });
                  value = currency[index];
                  Hive.box('code').put(2, value);
                  Navigator.pushAndRemoveUntil(
                      context,
                      MaterialPageRoute(builder: (context) => HomePage()),
                      (route) => false);
                },
              ),
            );
          }),
    );
  }
}
