import 'dart:async';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:flutter_facebook_auth/flutter_facebook_auth.dart';
import 'package:hive/hive.dart';
import 'package:hotel_booking_1/src/ui/screens/login.dart';
import 'package:hotel_booking_1/src/utils/object_factory.dart';

class Auth {
  final FirebaseAuth auth,authfb;
  FacebookAuth facebookAuth=FacebookAuth.instance;
  Auth({this.auth,this.authfb});

  Stream<User> get user => auth.authStateChanges();
  Stream<User> get userfb => authfb.authStateChanges();

  Future<String> signInWithGoogle() async {
    try {
      // Trigger the authentication flow
      final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();

      // Obtain the auth details from the request
      final GoogleSignInAuthentication googleAuth =
      await googleUser.authentication;

      // Create a new credential
      final GoogleAuthCredential credential = GoogleAuthProvider.credential(
        accessToken: googleAuth.accessToken,
        idToken: googleAuth.idToken,
      );
      await auth.signInWithCredential(credential);
      if(googleUser!=null) {
        Hive.box('lang').put(12, googleUser.photoUrl);
        Hive.box('lang').put(13, googleUser.displayName);
        Hive.box('lang').put(14, googleUser.email);
        Hive.box('lang').put(15, googleAuth.idToken);
        Hive.box('lang').put(16, auth.currentUser.uid);
      }
      String status="Success";
      return status.trim();
    }on FirebaseAuthException catch(e){
      return e.message;
    }
    catch(e){
      rethrow;
    }
  }

  Future<String> Signout() async {
    try {
      await auth.signOut();
      await GoogleSignIn().signOut();
      await facebookAuth.logOut();
      await authfb.signOut();
      String status = "Success";
      return status.trim();
    } on FirebaseAuthException catch (e) {
      return e.message;
    } catch (e) {
      rethrow;
    }
  }

  Future<String> signInWithFacebook() async {
    try{
      // Trigger the sign-in flow
      final LoginResult result = await facebookAuth.login();
      // Create a credential from the access token
      final FacebookAuthCredential facebookAuthCredential =
      FacebookAuthProvider.credential(result.accessToken.token);
      UserCredential fbcredential=await authfb.signInWithCredential(facebookAuthCredential);
      if (fbcredential != null) {
        getTokenid()async{
          dynamic tokenid=await fbcredential.user.getIdToken(true);
          Hive.box('lang').put(15, tokenid);
          print(tokenid);
        }
        getTokenid();
        Hive.box('lang').put(12, fbcredential.user.photoURL);
        Hive.box('lang').put(13, fbcredential.user.displayName);
        Hive.box('lang').put(14, fbcredential.user.email);
        Hive.box('lang').put(16, authfb.currentUser.uid);
      }
      return "Success";
    } on FirebaseAuthException catch (e) {
      return e.message;
    } catch (e) {
      rethrow;
    }
  }
// To merge Facebook
  Future<String> linkGoogleAndFacebook() async {
    // Trigger the Google Authentication flow.
    final GoogleSignInAccount googleUser = await GoogleSignIn().signIn();
    // Obtain the auth details from the request.
    final GoogleSignInAuthentication googleAuth = await googleUser.authentication;
    // Create a new credential.
    final GoogleAuthCredential googleCredential = GoogleAuthProvider.credential(
      accessToken: googleAuth.accessToken,
      idToken: googleAuth.idToken,
    );
    // Sign in to Firebase with the Google [UserCredential].
    final UserCredential googleUserCredential =
    await auth.signInWithCredential(googleCredential);
    if(googleUser!=null) {
      Hive.box('lang').put(12, googleUser.photoUrl);
      Hive.box('lang').put(13, googleUser.displayName);
      Hive.box('lang').put(14, googleUser.email);
      Hive.box('lang').put(15, googleAuth.idToken);
      Hive.box('lang').put(16, auth.currentUser.uid);
    }
    // Trigger the sign-in flow of facebook
    final LoginResult result = await facebookAuth.login();
    // Create a credential from the access token
    final FacebookAuthCredential facebookAuthCredential =
    FacebookAuthProvider.credential(result.accessToken.token);

    // Link the Facebook account to the Google account.
    await googleUserCredential.user.linkWithCredential(facebookAuthCredential);
    return "Success";
  }
}
