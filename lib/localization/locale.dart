import 'package:flutter/cupertino.dart';
import 'package:hotel_booking_1/localization/demo_local.dart';

String getTranslated(BuildContext context, String key) {
  return DemoLocalizations.of(context).getTranslatedValues(key);
}
